package com.spot.smartcr.fetch;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

import org.junit.Ignore;
import org.junit.Test;

import com.smartcr.core.index.Fetcher;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.IProgressMonitor;
import com.smartcr.core.util.RepositoryConfig;
import com.smartcr.fetch.FetcherNotFoundException;
import com.smartcr.fetch.FetchersLoader;

public class FetchersTest {

	FetchersLoader loader = FetchersLoader.getInstance(new File("fetchers.xml"));
	IProgressMonitor monitor = DefaultProgressMonitor.getInstance();

	@Test
	public void testGetFetchers() {
		Assert.assertNotNull(loader.getFetchers(monitor));
	}

	@Test
	public void testFetcherNotFound() {
		try {
			Assert.assertNull(loader.getFetcherByName("non existing", monitor));
		} catch (Exception e) {
			Assert.assertTrue(e instanceof FetcherNotFoundException);
		}
	}

	@Test
	public void testFetchCRsFromBugzilla() throws FetcherNotFoundException {
		assertLimitedCRsFinding("bugzilla", 2L);
	}

	@Ignore
	@Test
	public void testFetchCRsFromMantiBT() throws FetcherNotFoundException {
		assertLimitedCRsFinding("mantisbt", 2L);
	}
	
	@Ignore
	@Test
	public void testFetchCRsFromMantiSerpro() throws FetcherNotFoundException {
		assertLimitedCRsFinding("mantis-serpro", 2L);
	}

	@Ignore
	@Test
	public void testFetchCRsFromJazzSerpro() throws FetcherNotFoundException {
		assertLimitedCRsFinding("alm-serpro", 2L);
	}

	@Test
	public void testIDsInAscendiOrder() throws FetcherNotFoundException {
		RepositoryConfig config = loader.getFetcherByName("mantis-serpro", monitor).getRepositoryConfig();
		if (!config.getIds().isEmpty()) {
			Iterator<String> iter = config.getIds().iterator();
			String firstNextId = iter.next();
			while (iter.hasNext()) {
				String secondNextId = iter.next();
				Assert.assertTrue(new Long(firstNextId) <= new Long(secondNextId));
			}
		}
	}
	
	private void assertLimitedCRsFinding(String repository, long limit)
			throws FetcherNotFoundException {
		Fetcher fetcher = loader.getFetcherByName(repository, monitor);
		fetcher.getRepositoryConfig().setLimit(limit);
		List<ChangeRequest> crs = fetcher.findAllChangeRequests();
		for (ChangeRequest cr : crs) {
			System.out.println("Get CR from " + repository + ": " + cr.getOriginalId());
		}
		Assert.assertEquals(limit, crs.size());
	}
}
