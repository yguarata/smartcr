package com.smartcr.fetch.ibmjazz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.RejectedExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.runtime.IProgressMonitor;

import com.ibm.team.process.client.IProcessItemService;
import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.process.common.IProjectAreaHandle;
import com.ibm.team.repository.client.ILoginHandler2;
import com.ibm.team.repository.client.ILoginInfo2;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.client.TeamPlatform;
import com.ibm.team.repository.client.login.UsernameAndPasswordLoginInfo;
import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.IContributorHandle;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.workitem.client.IWorkItemClient;
import com.ibm.team.workitem.common.IAuditableCommon;
import com.ibm.team.workitem.common.IQueryCommon;
import com.ibm.team.workitem.common.expression.AttributeExpression;
import com.ibm.team.workitem.common.expression.Expression;
import com.ibm.team.workitem.common.expression.IQueryableAttribute;
import com.ibm.team.workitem.common.expression.QueryableAttributes;
import com.ibm.team.workitem.common.expression.Term;
import com.ibm.team.workitem.common.model.AttributeOperation;
import com.ibm.team.workitem.common.model.ICategory;
import com.ibm.team.workitem.common.model.IComment;
import com.ibm.team.workitem.common.model.IWorkItem;
import com.ibm.team.workitem.common.model.ItemProfile;
import com.ibm.team.workitem.common.query.IQueryDescriptor;
import com.ibm.team.workitem.common.query.IQueryResult;
import com.ibm.team.workitem.common.query.IResolvedResult;
import com.smartcr.core.index.Fetcher;
import com.smartcr.core.index.IChangeRequestFetcher;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.util.ConsolePrint;
import com.smartcr.core.util.RepositoryConfig;

public class IBMJazzChangeRequestFetcher implements IChangeRequestFetcher {

	static Logger logger = Logger.getLogger(IBMJazzChangeRequestFetcher.class
			.getName());
	IProgressMonitor myProgressMonitor = new SysoutProgressMonitor();
	IAuditableCommon auditableCommon;
	IQueryCommon queryCommon;
	ITeamRepository teamRepository;
	IWorkItemClient workItemClient;
	IProcessItemService processItemService;
	private RepositoryConfig repositoryConfig;

	@Override
	public void configure(RepositoryConfig config) {
		repositoryConfig = config;
	}

	public void initRepository() throws TeamRepositoryException {
		// Login to the repository using the provided credentials
		assert repositoryConfig != null;
		teamRepository = TeamPlatform.getTeamRepositoryService()
				.getTeamRepository(repositoryConfig.getUrl());
		teamRepository.registerLoginHandler(new ILoginHandler2() {
			@Override
			public ILoginInfo2 challenge(ITeamRepository repo) {
				return new UsernameAndPasswordLoginInfo(repositoryConfig
						.getUser(), repositoryConfig.getPassword());
			}
		});

		int retryCount = 1;
		while (true) {
			try {
				if (!teamRepository.loggedIn()) {
					logger.info("Trying to login. " + retryCount
							+ " tentative.");
					teamRepository.login(myProgressMonitor);
				}
				break;
			} catch (RejectedExecutionException exception) {
				if (retryCount++ == 6) {
					throw exception;
				}
			}
		}

		workItemClient = (IWorkItemClient) teamRepository
				.getClientLibrary(IWorkItemClient.class);

		processItemService = (IProcessItemService) teamRepository
				.getClientLibrary(IProcessItemService.class);

		auditableCommon = (IAuditableCommon) teamRepository
				.getClientLibrary(IAuditableCommon.class);

		queryCommon = (IQueryCommon) teamRepository
				.getClientLibrary(IQueryCommon.class);
	}

	@SuppressWarnings("unchecked")
	public List<ChangeRequest> findAllChangeRequests() {
		List<ChangeRequest> result = new ArrayList<ChangeRequest>();
		try {
			List<IProjectArea> projectAreas = processItemService
					.findAllProjectAreas(null, myProgressMonitor);

			HashMap<String, List<ChangeRequest>> crs = new HashMap<String, List<ChangeRequest>>();

			boolean breakFor = false;

			for (IProjectArea currentProjectArea : projectAreas) {
				if (breakFor) {
					break;
				}
				logger.info("Loading CRs from " + currentProjectArea.getName());
				IQueryResult<IResolvedResult<IWorkItem>> results = getDefectWorkItems(currentProjectArea);

				while (results.hasNext(myProgressMonitor)) {
					IWorkItem item = results.next(myProgressMonitor).getItem();
					ConsolePrint
							.getCR(item.getId(), repositoryConfig.getName());
					ChangeRequest cr = createChangeRequest(item);
					if (crs.get(cr.getProject()) == null) {
						crs.put(cr.getProject(), new ArrayList<ChangeRequest>());
					}
					crs.get(cr.getProject()).add(cr);
					result.add(cr);
					if (repositoryConfig.getLimit() != null
							&& result.size() >= repositoryConfig.getLimit()) {
						breakFor = true;
						break;
					}
				}
			}

			logger.info("\n=== TOTALS ===\n");
			for (String key : crs.keySet()) {
				logger.info(key + ": " + crs.get(key).size());
			}

		} catch (TeamRepositoryException e) {
			logger.log(Level.SEVERE,
					"It was not possible to retireve CRs from "
							+ repositoryConfig.getName(), e);
		}
		return result;
	}

	private IQueryResult<IResolvedResult<IWorkItem>> getDefectWorkItems(
			IProjectAreaHandle currentProjectArea)
			throws TeamRepositoryException {
		IQueryableAttribute projectAreaProp = QueryableAttributes.getFactory(
				IWorkItem.ITEM_TYPE).findAttribute(currentProjectArea,
				IWorkItem.PROJECT_AREA_PROPERTY, auditableCommon, null);

		IQueryableAttribute itemTypeProp = QueryableAttributes.getFactory(
				IWorkItem.ITEM_TYPE).findAttribute(currentProjectArea,
				IWorkItem.TYPE_PROPERTY, auditableCommon, myProgressMonitor);

		Expression inProjectArea = new AttributeExpression(projectAreaProp,
				AttributeOperation.EQUALS, currentProjectArea);

		Expression isDefectType = new AttributeExpression(itemTypeProp,
				AttributeOperation.EQUALS, "defect");

		Term typeinProjectArea = new Term(Term.Operator.AND);
		typeinProjectArea.add(inProjectArea);
		typeinProjectArea.add(isDefectType);

		IQueryDescriptor descriptor = queryCommon.createQuery(
				currentProjectArea, "MyQuery", "MyQuery", typeinProjectArea);

		IQueryResult<IResolvedResult<IWorkItem>> results = queryCommon
				.getResolvedExpressionResults(currentProjectArea,
						descriptor.getExpression(), IWorkItem.FULL_PROFILE);
		return results;
	}

	private ChangeRequest createChangeRequest(IWorkItem item)
			throws TeamRepositoryException {
		IProjectArea projectArea = auditableCommon.resolveAuditable(
				item.getProjectArea(), ItemProfile.PROJECT_AREA_DEFAULT,
				myProgressMonitor);

		ICategory category = auditableCommon.resolveAuditable(
				item.getCategory(), ICategory.SMALL_PROFILE, myProgressMonitor);

		Author resolver = getAuthorFromContributor(item.getResolver());
		Author creator = getAuthorFromContributor(item.getCreator());
		Author owner = getAuthorFromContributor(item.getOwner());

		ChangeRequest cr = new ChangeRequest();
		cr.setProject(projectArea.getName());
		cr.setOriginalId(String.valueOf(item.getId()));
		cr.setTitle(item.getHTMLSummary().getPlainText());
		cr.setDescription(item.getHTMLDescription().getPlainText());

		cr.setDateCreated(item.getCreationDate());
		cr.setDateDue(item.getDueDate());

		cr.setCreatedBy(creator);
		cr.setFixedBy(owner);
		cr.setClosedBy(resolver);
		cr.setOrigin(category.getName());
		cr.setSeverity(item.getSeverity().getStringIdentifier());
		cr.setResolution(item.getResolution2() == null ? null : item
				.getResolution2().getStringIdentifier());
		cr.setStatus(item.getState2().getStringIdentifier());

		for (IComment comment : item.getComments().getContents()) {
			Author author = getAuthorFromContributor(comment.getCreator());
			Comment comment_ = new Comment();
			comment_.setAuthor(author);
			comment_.setComment(comment.getHTMLContent().getPlainText());
			cr.getComments().add(comment_);
		}
		return cr;
	}

	private Author getAuthorFromContributor(IContributorHandle contributorHandle)
			throws TeamRepositoryException {
		IContributor contributor = auditableCommon.resolveAuditable(
				contributorHandle, ItemProfile.CONTRIBUTOR_DEFAULT,
				myProgressMonitor);

		logger.info("Getting author from contributor name="
				+ contributor.getName() + " originalId="
				+ contributor.getUserId());

		if (contributor.getName().toLowerCase().equals("unassigned")) {
			return null;
		}

		Author author = new Author(contributor.getName(),
				contributor.getEmailAddress());
		author.setOriginalId(new String(contributor.getUserId()));
		return author;
	}

	@Override
	public ChangeRequest getChangeRequest(String id) {
		ChangeRequest cr = null;
		try {
			ConsolePrint.getCR(Long.valueOf(id), repositoryConfig.getName());
			IWorkItem item = workItemClient.findWorkItemById(new Integer(id),
					IWorkItem.FULL_PROFILE, myProgressMonitor);

			if (item != null) {
				cr = createChangeRequest(item);
			} else {
				ConsolePrint.crDontExist(Long.valueOf(id),
						repositoryConfig.getName());
			}

		} catch (TeamRepositoryException e) {
			e.printStackTrace();
		}

		return cr;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<String> findAvailableChangeRequestIDs() {
		List<String> result = new ArrayList<String>();
		try {
			List<IProjectArea> projectAreas = processItemService
					.findAllProjectAreas(null, myProgressMonitor);
			for (IProjectArea currentProjectArea : projectAreas) {
				logger.info("Loading CR IDs from "
						+ currentProjectArea.getName());
				IQueryResult<IResolvedResult<IWorkItem>> results = getDefectWorkItems(currentProjectArea);

				while (results.hasNext(myProgressMonitor)) {
					IWorkItem item = results.next(myProgressMonitor).getItem();
					result.add(String.valueOf(item.getId()));
				}
			}
		} catch (TeamRepositoryException e) {
			logger.log(Level.SEVERE,
					"It was not possible to retireve CR IDs from "
							+ repositoryConfig.getName(), e);
		}
		return new HashSet<String>(result);
	}

	@Override
	public void startWorkSession() {
		try {
			if (!TeamPlatform.isStarted()) {
				TeamPlatform.startup();
				initRepository();
			}
		} catch (TeamRepositoryException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stopWorkSession() {
		// if (TeamPlatform.isStarted()) {
		// TeamPlatform.shutdown();
		// }
		// teamRepository.logout();
	}

	public static void main(String[] args) {
		RepositoryConfig repository = new RepositoryConfig();
		repository.setUser("yguarata");
		repository.setPassword("25deiz83");
		repository.setUrl("https://jazz.net/sandbox02-ccm");
		Fetcher f = new Fetcher(new IBMJazzChangeRequestFetcher(), repository,
				com.smartcr.core.util.DefaultProgressMonitor.getInstance());
		f.getFetcherImpl().startWorkSession();
		f.getFetcherImpl().findAllChangeRequests();
	}
}