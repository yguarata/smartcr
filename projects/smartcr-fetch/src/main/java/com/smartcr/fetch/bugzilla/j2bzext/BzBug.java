package com.smartcr.fetch.bugzilla.j2bzext;

import com.j2bugzilla.base.Bug;

/**
 * This class is a wrapper around Bug class in order to provide more information
 * about the bug, using the xml-rpc api.
 * 
 * @author yguarata
 * 
 */
public class BzBug {

	private Bug bug;

	public BzBug(Bug bug) {
		this.bug = bug;
	}

	public Bug getBug() {
		return bug;
	}

	public String getAssignedTo() {
		return (String) this.bug.getParameterMap().get("assigned_to");
	}

	public String getCreator() {
		return (String) this.bug.getParameterMap().get("creator");
	}

}
