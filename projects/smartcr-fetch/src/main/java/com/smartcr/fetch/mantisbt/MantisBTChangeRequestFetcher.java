package com.smartcr.fetch.mantisbt;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

import com.smartcr.core.index.Fetcher;
import com.smartcr.core.index.IChangeRequestFetcher;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.util.ConsolePrint;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.RepositoryConfig;
import com.smartcr.fetch.mantisbt.webservice.mantisbt.IssueData;
import com.smartcr.fetch.mantisbt.webservice.mantisbt.IssueNoteData;
import com.smartcr.fetch.mantisbt.webservice.mantisbt.MantisConnectPortType;
import com.smartcr.fetch.mantisbt.webservice.mantisbt.MantisConnectPortTypeProxy;

public class MantisBTChangeRequestFetcher implements IChangeRequestFetcher {

	static Logger logger = Logger.getLogger(MantisBTChangeRequestFetcher.class
			.getName());
	public MantisConnectPortType service;
	private RepositoryConfig repositoryConfig;

	public MantisBTChangeRequestFetcher() {

	}

	@Override
	public void configure(RepositoryConfig config) {
		repositoryConfig = config;
		service = new MantisConnectPortTypeProxy(repositoryConfig.getUrl());
	}

	@Override
	public List<ChangeRequest> findAllChangeRequests() {
		try {
			List<ChangeRequest> changeRequets = new ArrayList<ChangeRequest>();
			List<Long> deniedCRs = new ArrayList<Long>();
			Set<String> ids = repositoryConfig.getIds();

			if (repositoryConfig.getLimit() != null) {
				List<String> idsLIst = new ArrayList<String>(ids).subList(0,
						repositoryConfig.getLimit().intValue());
				ids.clear();
				ids.addAll(idsLIst);
			}

			for (String id : ids) {
				BigInteger idb = BigInteger.valueOf(Long.valueOf(id));
				Long idl = idb.longValue();

				if (service.mc_issue_exists(repositoryConfig.getUser(),
						repositoryConfig.getPassword(), idb)) {
					try {
						IssueData issue = service.mc_issue_get(
								repositoryConfig.getUser(),
								repositoryConfig.getPassword(), idb);
						changeRequets.add(createChangeRequest(issue));
					} catch (Exception e) {
						logger.log(Level.SEVERE, "Could not get CR " + idb, e);
						deniedCRs.add(idb.longValue());
					}
				} else {
					logger.log(Level.WARNING, "CR do not exist: id=" + idl
							+ " repository=" + repositoryConfig.getName());
					repositoryConfig.setLimit(repositoryConfig.getLimit() + 1);
				}
			}

			if (!deniedCRs.isEmpty()) {
				logger.log(Level.WARNING, "Some CRs could not be retrieved: "
						+ deniedCRs);
			}

			return changeRequets;
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	private ChangeRequest createChangeRequest(IssueData issue) {
		ChangeRequest cr = new ChangeRequest();
		cr.setOriginalId(String.valueOf(issue.getId()));
		cr.setTitle(issue.getSummary());
		cr.setDescription(issue.getDescription() + " "
				+ issue.getAdditional_information());
		cr.setStepsToReproduce(issue.getSteps_to_reproduce());

		if (issue.getProject() != null) {
			cr.setProject(issue.getProject().getName());
		}

		if (issue.getDate_submitted() != null) {
			cr.setDateCreated(issue.getDate_submitted().getTime());
		}
		if (issue.getLast_updated() != null) {
			cr.setDateUpdated(issue.getLast_updated().getTime());
		}
		if (issue.getDue_date() != null) {
			cr.setDateDue(issue.getDue_date().getTime());
		}

		if (issue.getReporter() != null) {
			Author createdBy = new Author(issue.getReporter().getName(), issue
					.getReporter().getEmail());
			createdBy.setOriginalId(String.valueOf(issue.getReporter().getId()));
			cr.setCreatedBy(createdBy);
		}

		if (issue.getHandler() != null) {
			Author fixedBy = new Author(issue.getHandler().getName(), issue
					.getHandler().getEmail());
			fixedBy.setOriginalId(String.valueOf(issue.getHandler().getId()));
			cr.setFixedBy(fixedBy);
		}

		if (cr.getCreatedBy() != null
				&& StringUtils.isEmpty(cr.getCreatedBy().getEmail())) {
			cr.getCreatedBy().setEmail(cr.getCreatedBy().getName());
		}

		if (cr.getFixedBy() != null
				&& StringUtils.isEmpty(cr.getFixedBy().getEmail())) {
			cr.getFixedBy().setEmail(cr.getFixedBy().getName());
		}

		if (issue.getSeverity() != null) {
			cr.setSeverity(issue.getSeverity().getName());
		}
		if (issue.getResolution() != null) {
			cr.setResolution(issue.getResolution().getName());
		}
		if (issue.getStatus() != null) {
			cr.setStatus(issue.getStatus().getName());
		}

		if (issue.getNotes() != null) {
			for (IssueNoteData note : issue.getNotes()) {
				Comment comment = new Comment();
				if (note.getReporter() != null) {
					Author author = new Author(note.getReporter().getName(),
							note.getReporter().getEmail());
					author.setOriginalId(String.valueOf(note.getReporter().getId()));
					comment.setAuthor(author);
				}
				comment.setChangeRequest(cr);
				comment.setComment(note.getText());
				comment.setOriginalId(String.valueOf(note.getId()));
				cr.getComments().add(comment);
			}
		}

		return cr;
	}

	@Override
	public Set<String> findAvailableChangeRequestIDs() {
		return repositoryConfig.getIds();
	}

	@Override
	public ChangeRequest getChangeRequest(String id) {
		try {
			BigInteger idBI = new BigInteger(id);
			if (service.mc_issue_exists(repositoryConfig.getUser(),
					repositoryConfig.getPassword(), idBI)) {
				IssueData issue = service.mc_issue_get(
						repositoryConfig.getUser(),
						repositoryConfig.getPassword(), idBI);
				return createChangeRequest(issue);
			} else {
				ConsolePrint.crDontExist(Long.valueOf(id),
						repositoryConfig.getName());
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void startWorkSession() {
		// TODO Auto-generated method stub

	}

	@Override
	public void stopWorkSession() {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) {
		RepositoryConfig repository = new RepositoryConfig();
		repository.setUser("yguarata");
		repository.setPassword("25dez83");

		Set<String> ids = new LinkedHashSet<String>();
		ids.addAll(Arrays.asList(new String[] { "1", "2", "3" }));
		repository.setIds(ids);
		repository
				.setUrl("http://www.mantisbt.org/bugs/api/soap/mantisconnect.php");

		Fetcher f = new Fetcher(new MantisBTChangeRequestFetcher(), repository,
				DefaultProgressMonitor.getInstance());

		List<ChangeRequest> crs = f.getFetcherImpl().findAllChangeRequests();

		for (ChangeRequest changeRequest : crs) {
			System.out.println(changeRequest);
		}

		f.getFetcherImpl().getChangeRequest("1");
	}

}
