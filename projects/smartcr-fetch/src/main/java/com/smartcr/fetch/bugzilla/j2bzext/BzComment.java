package com.smartcr.fetch.bugzilla.j2bzext;

import com.j2bugzilla.base.Comment;

/**
 * This class extends the original Comment class, from j2bugzilla, to complement
 * it with creator information.
 * 
 * @author yguarata
 * 
 */
public class BzComment extends Comment {

	private String creator;

	public BzComment(int id, String text, String creator) {
		super(id, text);
		this.creator = creator;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

}
