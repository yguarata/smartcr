package com.smartcr.fetch;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.smartcr.core.index.Fetcher;
import com.smartcr.core.index.IChangeRequestFetcher;
import com.smartcr.core.index.LuceneIndexer;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.IProgressMonitor;
import com.smartcr.core.util.RepositoryConfig;
import com.smartcr.core.util.RepositoryType;
import com.smartcr.core.util.SameRepositoryNameException;

public class FetchersLoader {

	static Logger logger = Logger.getLogger(FetchersLoader.class.getName());
	private static FetchersLoader instance;
	private Document doc;
	private boolean testMode = false;

	private FetchersLoader(File configurationFile) {
		try {
			doc = parseXMLFile(configurationFile);
		} catch (Exception e) {
			System.out
					.println("It was not possible to read the configuration file.");
			e.printStackTrace();
		}
	}

	public static FetchersLoader getInstance(File configurationFile) {
		if (instance == null) {
			instance = new FetchersLoader(configurationFile);
		}
		return instance;
	}

	public List<RepositoryConfig> load() {
		List<RepositoryConfig> repositories = new ArrayList<RepositoryConfig>();
		NodeList nList = doc.getElementsByTagName("repository");

		for (int i = 0; i < nList.getLength(); i++) {
			Node nNode = nList.item(i);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element repositoryNode = (Element) nNode;
				RepositoryConfig repository = new RepositoryConfig();
				repository.setName(repositoryNode.getElementsByTagName("name")
						.item(0).getTextContent());
				repository.setType(RepositoryType.valueOf(repositoryNode
						.getElementsByTagName("type").item(0).getTextContent()
						.replace("-", "_").toUpperCase()));
				repository.setUser(repositoryNode.getElementsByTagName("user")
						.item(0).getTextContent());
				repository.setPassword(repositoryNode
						.getElementsByTagName("password").item(0)
						.getTextContent());
				repository.setUrl(repositoryNode.getElementsByTagName("url")
						.item(0).getTextContent());
				repository.setFetcherClass(repositoryNode
						.getElementsByTagName("fetcher-class").item(0)
						.getTextContent());

				if (repositoryNode.getElementsByTagName("limit").getLength() > 0) {
					repository.setLimit(Long.valueOf(repositoryNode
							.getElementsByTagName("limit").item(0)
							.getTextContent()));
				}

				if (repositoryNode.getElementsByTagName("ids").getLength() > 0) {
					String[] idsString = repositoryNode
							.getElementsByTagName("ids").item(0)
							.getTextContent().split(",");
					List<Long> idsLong = new ArrayList<Long>(idsString.length);

					for (int j = 0; j < idsString.length; j++) {
						idsLong.add(new Long(idsString[j].trim()));
					}

					Collections.sort(idsLong);

					for (Long id : idsLong) {
						repository.getIds().add(id.toString());
					}
				}

				repositories.add(repository);
			}
		}

		try {
			validateRepositories(repositories);
		} catch (SameRepositoryNameException e) {
			e.printStackTrace();
			System.exit(1);
		}

		return repositories;
	}

	public Fetcher getFetcherByName(String name, IProgressMonitor monitor)
			throws FetcherNotFoundException {
		Fetcher foundFetcher = null;
		for (Fetcher fetcher : getFetchers(monitor)) {
			if (fetcher.getName().equalsIgnoreCase(name)) {
				foundFetcher = fetcher;
			}
		}

		if (foundFetcher == null) {
			throw new FetcherNotFoundException("Fetcher '" + name
					+ "' was not found.");
		}

		return foundFetcher;
	}

	public List<Fetcher> getFetchers(IProgressMonitor monitor) {
		List<Fetcher> list = new ArrayList<Fetcher>();
		try {
			for (RepositoryConfig repositoryConfig : this.load()) {
				Class<?> clazz;
				clazz = Class.forName(repositoryConfig.getFetcherClass());
				IChangeRequestFetcher f = (IChangeRequestFetcher) clazz
						.getConstructor().newInstance();
				Fetcher ff = new Fetcher(f, repositoryConfig, monitor);
				list.add(ff);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return list;
	}

	private void validateRepositories(List<RepositoryConfig> repositories)
			throws SameRepositoryNameException {
		List<String> names = new ArrayList<String>();

		for (RepositoryConfig repo : repositories) {
			if (names.contains(repo.getName())) {
				throw new SameRepositoryNameException(repo.getName());
			} else {
				names.add(repo.getName());
			}
		}
	}

	private Document parseXMLFile(File configurationFile)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(configurationFile);
		return doc;
	}

	public static void main(String[] args) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException,
			NoSuchMethodException, SecurityException, IllegalArgumentException,
			InvocationTargetException, FetcherNotFoundException {
		for (Fetcher r : FetchersLoader.getInstance(new File("fetchers.xml"))
				.getFetchers(DefaultProgressMonitor.getInstance())) {
			System.out.println(r.getRepositoryConfig().getName());
			System.out.println(r.getRepositoryConfig().getType());
			System.out.println(r.getRepositoryConfig().getUrl());
			System.out.println(r.getRepositoryConfig().getUser());
			System.out.println(r.getRepositoryConfig().getPassword());
		}

		Fetcher fetcher = FetchersLoader.getInstance(new File("fetchers.xml"))
				.getFetcherByName("almibm",
						DefaultProgressMonitor.getInstance());

		fetcher.getRepositoryConfig().setUser("yguarata");
		fetcher.getRepositoryConfig().setPassword("25deiz83");

		LuceneIndexer.getInstance().storeAllChangeRequests(fetcher);

		for (ChangeRequest cr : fetcher.findAllChangeRequests()) {
			System.out.println(cr.getOriginalId());
		}

	}

	public boolean isTestMode() {
		return testMode;
	}

	public void setTestMode(boolean testMode) {
		this.testMode = testMode;
	}

}
