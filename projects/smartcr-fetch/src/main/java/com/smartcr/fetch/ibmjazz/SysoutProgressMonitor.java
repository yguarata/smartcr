package com.smartcr.fetch.ibmjazz;
/*******************************************************************************
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2006, 2012. All Rights Reserved. 
 * 
 * Note to U.S. Government Users Restricted Rights:  Use, 
 * duplication or disclosure restricted by GSA ADP Schedule 
 * Contract with IBM Corp.
 *******************************************************************************/
import org.eclipse.core.runtime.IProgressMonitor;

public class SysoutProgressMonitor implements IProgressMonitor {

    @Override
	public void beginTask(String name, int totalWork) {
        print(name);
    }

    @Override
	public void done() {
    }

    @Override
	public void internalWorked(double work) {
    }

    @Override
	public boolean isCanceled() {
        return false;
    }

    @Override
	public void setCanceled(boolean value) {
    }

    @Override
	public void setTaskName(String name) {
        print(name);
    }

    @Override
	public void subTask(String name) {
        print(name);
    }

    @Override
	public void worked(int work) {
    }
    
    private void print(String name) {
        if(name != null && ! "".equals(name))
            System.out.println(name);
    }
}