package com.smartcr.fetch.mantisbt;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.smartcr.core.index.Fetcher;
import com.smartcr.core.index.IChangeRequestFetcher;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.model.History;
import com.smartcr.core.util.ConsolePrint;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.RepositoryConfig;
import com.smartcr.fetch.mantisbt.webservice.serpro.IssueData;
import com.smartcr.fetch.mantisbt.webservice.serpro.IssueNoteData;
import com.smartcr.fetch.mantisbt.webservice.serpro.MantisConnectPortType;
import com.smartcr.fetch.mantisbt.webservice.serpro.MantisConnectPortTypeProxy;

public class MantisSerproChangeRequestFetcher implements IChangeRequestFetcher {

	static Logger logger = Logger
			.getLogger(MantisSerproChangeRequestFetcher.class.getName());
	private MantisConnectPortType service;
	private RepositoryConfig repositoryConfig;
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
			"dd-MM-yyyy HH:mm");
	
	@Override
	public void configure(RepositoryConfig config) {
		repositoryConfig = config;
		service = new MantisConnectPortTypeProxy(repositoryConfig.getUrl());
	}

	@Override
	public Set<String> findAvailableChangeRequestIDs() {
		return repositoryConfig.getIds();
	}

	@Override
	public List<ChangeRequest> findAllChangeRequests() {
		try {
			List<ChangeRequest> changeRequets = new ArrayList<ChangeRequest>();
			List<Long> deniedCRs = new ArrayList<Long>();
			Set<String> ids = repositoryConfig.getIds();

			if (repositoryConfig.getLimit() != null) {
				List<String> idsList = new ArrayList<String>(ids).subList(0,
						repositoryConfig.getLimit().intValue());
				ids.clear();
				ids.addAll(idsList);
			}

			for (String id : ids) {
				BigInteger idb = BigInteger.valueOf(Long.valueOf(id));
				Long idl = idb.longValue();

				if (service.mc_issue_exists(repositoryConfig.getUser(),
						repositoryConfig.getPassword(), idb)) {
					try {
						IssueData issue = service.mc_issue_get(
								repositoryConfig.getUser(),
								repositoryConfig.getPassword(), idb);
						changeRequets.add(createChangeRequest(issue));
					} catch (Exception e) {
						logger.log(Level.SEVERE, "Could not get CR " + idb, e);
						deniedCRs.add(idb.longValue());
					}
				} else {
					ConsolePrint.crDontExist(idl, repositoryConfig.getName());
					repositoryConfig.setLimit(repositoryConfig.getLimit() + 1);
				}
			}

			if (!deniedCRs.isEmpty()) {
				logger.log(Level.WARNING, "Some CRs could not be retrieved: "
						+ deniedCRs);
			}

			return changeRequets;
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	private ChangeRequest createChangeRequest(IssueData issue) {
		ChangeRequest cr = new ChangeRequest();
		cr.setOriginalId(String.valueOf(issue.getId()));
		cr.setTitle(issue.getSummary());
		cr.setDescription(issue.getDescription());
		cr.setStepsToReproduce(issue.getSteps_to_reproduce());

		if (issue.getProject() != null) {
			cr.setProject(issue.getProject().getName());
		}

		if (issue.getDate_submitted() != null) {
			cr.setDateCreated(issue.getDate_submitted().getTime());
		}
		if (issue.getLast_updated() != null) {
			cr.setDateUpdated(issue.getLast_updated().getTime());
		}
		if (issue.getDue_date() != null) {
			cr.setDateDue(issue.getDue_date().getTime());
		}

		if (issue.getReporter() != null) {
			Author createdBy = new Author(issue.getReporter().getName(), issue
					.getReporter().getEmail());
			createdBy.setOriginalId(String.valueOf(issue.getReporter().getId()));
			cr.setCreatedBy(createdBy);
		}

		if (issue.getHandler() != null) {
			Author fixedBy = new Author(issue.getHandler().getName(), issue
					.getHandler().getEmail());
			fixedBy.setOriginalId(String.valueOf(issue.getHandler().getId()));
			cr.setFixedBy(fixedBy);
		}

		if (cr.getCreatedBy() != null
				&& StringUtils.isEmpty(cr.getCreatedBy().getEmail())) {
			cr.getCreatedBy().setEmail(cr.getCreatedBy().getName());
		}

		if (cr.getFixedBy() != null
				&& StringUtils.isEmpty(cr.getFixedBy().getEmail())) {
			cr.getFixedBy().setEmail(cr.getFixedBy().getName());
		}

		if (issue.getSeverity() != null) {
			cr.setSeverity(issue.getSeverity().getName());
		}
		if (issue.getResolution() != null) {
			cr.setResolution(issue.getResolution().getName());
		}
		if (issue.getStatus() != null) {
			cr.setStatus(issue.getStatus().getName());
		}

		if (issue.getNotes() != null) {
			for (IssueNoteData note : issue.getNotes()) {
				Comment comment = new Comment();
				if (note.getReporter() != null) {
					Author author = new Author(note.getReporter().getName(),
							note.getReporter().getEmail());
					author.setOriginalId(String.valueOf(note.getReporter().getId()));
					comment.setAuthor(author);
				}
				comment.setChangeRequest(cr);
				comment.setComment(note.getText());
				comment.setOriginalId(String.valueOf(note.getId()));
				cr.getComments().add(comment);
			}
		}

		extractCRHistoryFromHTML(cr);
		return cr;
	}

	public void extractCRHistoryFromHTML(ChangeRequest cr) {
		try {
			String id = cr.getOriginalId().toString();
			while (7 - id.length() > 0) { // Complete with zeros on the left.
				id = 0 + id;
			}
			File htmlFile = new File("/home/04966162424/Desktop/bugs-siafi/"+id+".php.html");
			logger.info("Extracting hitory from " + htmlFile.getAbsolutePath());
			Document doc;
			doc = Jsoup.parse(htmlFile, "UTF-8", "http://10.31.48.21/mantis/");
			Elements historyDiv = doc.select("div#history_open");
			Elements historyTable = historyDiv.select("table");
			extractInformationFromRow(historyTable.select(".row-1").iterator(), cr);
			extractInformationFromRow(historyTable.select(".row-2").iterator(), cr);
			logger.info("Extracted " + cr.getHistory().size() + 
					" history entries from " + htmlFile.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void extractInformationFromRow(Iterator<Element> rows1Iter,
			ChangeRequest cr) {
		while (rows1Iter.hasNext()) {
			History history = new History();
			history.setChangeRequest(cr);
			cr.getHistory().add(history);

			Elements TDs = rows1Iter.next().select("td");

			// Get the date
			String dateStr = TDs.get(0).text().trim();
			Date date;
			try {
				date = simpleDateFormat.parse(dateStr);
				history.setDate(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			// Get reporter
			String name = TDs.get(1).text().trim();
			history.setAuthor(new Author(name, null));
			// Get field
			String field = TDs.get(2).text().trim();
			history.setField(field);
			// Get change
			String change = TDs.get(3).text().trim();
			history.setChange(change);
		}
	}
	
	@Override
	public ChangeRequest getChangeRequest(String id) {
		try {
			BigInteger idBI = new BigInteger(id);
			if (service.mc_issue_exists(repositoryConfig.getUser(),
					repositoryConfig.getPassword(), idBI)) {
				IssueData issue = service.mc_issue_get(
						repositoryConfig.getUser(),
						repositoryConfig.getPassword(), idBI);
				return createChangeRequest(issue);
			} else {
				ConsolePrint.crDontExist(Long.valueOf(id),
						repositoryConfig.getName());
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		RepositoryConfig repository = new RepositoryConfig();
		repository.setUser("04966162424");
		repository.setPassword("25deiz1983s2");
		repository.setUrl("http://10.31.48.21/mantis/api/soap/mantisconnect.php");

		Fetcher f = new Fetcher(
				new MantisSerproChangeRequestFetcher(), repository,
				DefaultProgressMonitor.getInstance());

		ChangeRequest cr = f.getFetcherImpl().getChangeRequest("4588");
		
		for (History history: cr.getHistory()) {
			System.out.println(history.getField());
			System.out.println(history.getChange());
			System.out.println(" ");
		}
		
	}

	@Override
	public void startWorkSession() {
		// TODO Auto-generated method stub

	}

	@Override
	public void stopWorkSession() {
		// TODO Auto-generated method stub

	}

}
