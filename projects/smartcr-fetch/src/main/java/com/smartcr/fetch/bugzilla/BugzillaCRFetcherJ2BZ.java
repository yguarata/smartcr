package com.smartcr.fetch.bugzilla;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

import com.j2bugzilla.base.BugzillaConnector;
import com.j2bugzilla.base.BugzillaException;
import com.j2bugzilla.base.ConnectionException;
import com.j2bugzilla.rpc.BugComments;
import com.j2bugzilla.rpc.GetBug;
import com.smartcr.core.index.Fetcher;
import com.smartcr.core.index.IChangeRequestFetcher;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.RepositoryConfig;
import com.smartcr.fetch.bugzilla.j2bzext.BzBug;
import com.smartcr.fetch.bugzilla.j2bzext.BzBugComments;
import com.smartcr.fetch.bugzilla.j2bzext.BzComment;

public class BugzillaCRFetcherJ2BZ implements IChangeRequestFetcher {

	static Logger logger = Logger.getLogger(BugzillaCRFetcherJ2BZ.class
			.getName());
	private BugzillaConnector bzConnector;
	private RepositoryConfig repositoryConfig;

	@Override
	public void configure(RepositoryConfig config) {
		repositoryConfig = config;
		bzConnector = new BugzillaConnector();
		connect();
	}

	private void connect() {
		try {
			if (StringUtils.isNotEmpty(repositoryConfig.getUser())
					&& StringUtils.isNotEmpty(repositoryConfig.getPassword())) {
				bzConnector.connectTo(repositoryConfig.getUrl(),
						repositoryConfig.getUser(),
						repositoryConfig.getPassword());
			} else {
				bzConnector.connectTo(repositoryConfig.getUrl());
			}
		} catch (ConnectionException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
	}

	@Override
	public List<ChangeRequest> findAllChangeRequests() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ChangeRequest getChangeRequest(String id) {
		return getChangeRequestJ2Bugzilla(id);
	}

	private ChangeRequest getChangeRequestJ2Bugzilla(String id) {
		ChangeRequest cr = new ChangeRequest();
		GetBug getBug = new GetBug(id);
		BzBug bzBug;
		BugComments getComments;

		try {
			bzConnector.executeMethod(getBug);
			bzBug = new BzBug(getBug.getBug());
			getComments = new BzBugComments(bzBug.getBug());
			bzConnector.executeMethod(getComments);
		} catch (BugzillaException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return null;
		}
		cr.setOriginalId(String.valueOf(bzBug.getBug().getID()));
		cr.setProject(bzBug.getBug().getProduct());
		cr.setStatus(bzBug.getBug().getStatus());
		cr.setSeverity(bzBug.getBug().getSeverity());
		cr.setResolution(bzBug.getBug().getResolution());
		cr.setTitle(bzBug.getBug().getSummary());
		cr.setPriority(bzBug.getBug().getPriority());
		cr.setTarget(bzBug.getBug().getComponent());
		cr.setPlatform(bzBug.getBug().getPlatform());
		cr.setVersion(bzBug.getBug().getVersion());
		cr.setOperatingSystem(bzBug.getBug().getOperatingSystem());

		if (!bzBug.getAssignedTo().startsWith("noboby")) {
			cr.setFixedBy(new Author(bzBug.getAssignedTo(), bzBug
					.getAssignedTo()));
		}

		cr.setCreatedBy(new Author(bzBug.getCreator(), bzBug.getCreator()));

		BzComment[] bzComments = getComments.getComments().toArray(
				new BzComment[0]);
		cr.setDescription(bzComments[0].getText());

		for (int i = 1; i < bzComments.length; i++) {
			BzComment bzComment = bzComments[i];
			com.smartcr.core.model.Comment comment = new com.smartcr.core.model.Comment();
			comment.setComment(bzComment.getText());
			comment.setOriginalId(String.valueOf(bzComment.getID()));
			String creatorLogin = bzComment.getCreator();
			comment.setAuthor(new Author(creatorLogin, creatorLogin));
			cr.getComments().add(comment);
		}

		return cr;
	}

	@Override
	public Set<String> findAvailableChangeRequestIDs() {
		return repositoryConfig.getIds();
	}

	public static void main(String[] args) {
		RepositoryConfig config = new RepositoryConfig();
		config.setUrl("https://bugzilla.mozilla.org/");
		Fetcher f = new Fetcher(new BugzillaCRFetcherJ2BZ(), config, DefaultProgressMonitor.getInstance());
		System.out.println(f.getFetcherImpl().getChangeRequest("328231"));
		// System.out.println(f.getChangeRequest("428232"));
		// System.out.println(f.getChangeRequest("883253"));
		//
		// RepositoryConfig config2 = new RepositoryConfig();
		// config2.setUrl("https://bugs.eclipse.org/bugs/");
		// BugzillaCRFetcher f2 = new BugzillaCRFetcher(config2,
		// SysoutProgressMonitor.getInstance());
		// System.out.println(f2.getChangeRequest("328231"));
	}

	@Override
	public void startWorkSession() {
		// TODO Auto-generated method stub

	}

	@Override
	public void stopWorkSession() {
		// TODO Auto-generated method stub

	}

}
