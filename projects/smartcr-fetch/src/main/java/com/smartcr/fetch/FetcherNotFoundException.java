package com.smartcr.fetch;

public class FetcherNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8688815101015108161L;

	public FetcherNotFoundException() {
		super();
	}
	
	public FetcherNotFoundException(String msg) {
		super(msg);
	}
	
	public FetcherNotFoundException(String msg, Exception e) {
		super(msg, e);
	}
}
