package com.smartcr.fetch.bugzilla.j2bzext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.j2bugzilla.base.Bug;
import com.j2bugzilla.base.Comment;
import com.j2bugzilla.rpc.BugComments;

/**
 * This class extends the original class BugComments in order to access the
 * creator information from the xml-rcp api.
 * 
 * @author yguarata
 * 
 */
public class BzBugComments extends BugComments {

	private Map<Object, Object> hash;
	private int id;

	public BzBugComments(Bug bug) {
		super(bug);
		this.id = bug.getID();
	}

	@Override
	public void setResultMap(Map<Object, Object> hash) {
		this.setHash(hash);
		super.setResultMap(hash);
	}

	public Map<Object, Object> getHash() {
		return hash;
	}

	private void setHash(Map<Object, Object> hash) {
		this.hash = hash;
	}

	@Override
	public List<Comment> getComments() {
		List<Comment> commentList = new ArrayList<Comment>();

		if (hash.containsKey("bugs")) {
			/*
			 * Hideous, but it's the structure of the XML that Bugzilla returns.
			 * Since it's designed to return comments for multiple bugs at a
			 * time, there's extra nesting we don't need. TODO Allow requests
			 * for lists of Bugs
			 */
			@SuppressWarnings("unchecked")
			Map<String, Map<String, Map<Object, Object>[]>> m = (Map<String, Map<String, Map<Object, Object>[]>>) hash
					.get("bugs");

			Map<String, Map<Object, Object>[]> weird = m
					.get(String.valueOf(id));
			Object[] comments = weird.get("comments");

			if (comments.length == 0) {
				return commentList;
			} // Early return to prevent ClassCast

			for (Object o : comments) {
				@SuppressWarnings("unchecked")
				Map<Object, Object> comment = (Map<Object, Object>) o;
				Comment c = new BzComment((Integer) comment.get("id"),
						(String) comment.get("text"),
						(String) comment.get("creator"));
				commentList.add(c);
			}
		}

		return commentList;
	}

}
