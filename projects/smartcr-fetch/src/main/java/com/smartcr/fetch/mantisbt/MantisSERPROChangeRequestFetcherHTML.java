package com.smartcr.fetch.mantisbt;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.model.History;

/**
 * Extract CR from the HTML downloaded from the Mantis web site.
 * @author yguarata
 *
 */
public class MantisSERPROChangeRequestFetcherHTML extends
		MantisSerproChangeRequestFetcher {

	private Logger logger = Logger.getLogger(this.getClass().getName());
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
			"dd-MM-yyyy HH:mm");
	private static final String HTML_LOCATION = new String(
			"/home/yguarata/Downloads/bugs-siafi/");

	@Override
	public List<ChangeRequest> findAllChangeRequests() {
		File[] crFiles = new File(HTML_LOCATION).listFiles();
		logger.info("Loading " + crFiles.length + " CR files.");
		long count = 0;
		List<ChangeRequest> crs = new ArrayList<ChangeRequest>(crFiles.length);
		for (File file : crFiles) {
			String crID = file.getName().split("\\.")[0];
			try {
				crs.add(getChangeRequest(crID));
				logger.info("Loaded " + ++count + " files.");
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		return null;
	}

	@Override
	public ChangeRequest getChangeRequest(String id) {
		ChangeRequest cr = new ChangeRequest();
		try {
			File file = new File(HTML_LOCATION + id + ".php.html");
			logger.info("Loading file " + file.getAbsolutePath());
			Document doc = Jsoup.parse(file, "UTF-8",
					"http://10.31.48.21/mantis/");
			cr.setOriginalId(id);
			extractCRDetailsFromHTML(doc, cr);
			extractCRCommentsFromHTML(doc, cr);
			extractCRHistoryFromHTML(doc, cr);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cr;
	}

	private void extractCRDetailsFromHTML(Document doc, ChangeRequest cr) {
		Element tableDetails = doc.select("table").get(2);
		Elements rowsDetails = tableDetails.select("tr");		
		
		// Line 3
		String project = rowsDetails.get(2).select("td").get(1).text();
		String category = rowsDetails.get(2).select("td").get(2).text();
		String creationDate = rowsDetails.get(2).select("td").get(4).text();
		String lastUpdateDate = rowsDetails.get(2).select("td").get(5).text();
		// Line 5
		String createdBy = rowsDetails.get(4).select("td").get(1).text();
		// Line 6
		String fixedBy = rowsDetails.get(5).select("td").get(1).text();
		// Line 7
		String priority = rowsDetails.get(6).select("td").get(1).text();
		String severity = rowsDetails.get(6).select("td").get(3).text();
		// Line 8
		String status = rowsDetails.get(7).select("td").get(1).text();
		String resolution = rowsDetails.get(7).select("td").get(3).text();
		// Line 9
		String platform = rowsDetails.get(8).select("td").get(1).text();
		String operatingSystem = rowsDetails.get(8).select("td").get(3).text();
		// Line 10
		String version = null;
		if (rowsDetails.get(9).select("td").get(0).text().contains("Versão do Produto")) {
			version = rowsDetails.get(9).select("td").get(1).text();
		}
		// Line 13
		String summary = rowsDetails.get(12).select("td").get(1).text();
		// Line 14
		String description = rowsDetails.get(13).select("td").get(1).text();
		// Line 15
		String steps = null;
		if (rowsDetails.get(14).select("td").get(0).text().contains("Passos para Reproduzir")) {
			steps = rowsDetails.get(14).select("td").get(1).text();
		}
		// Line 20
		String component = null;
		if (rowsDetails.get(19).select("td").get(0).text().contains("Módulo")) {
			component = rowsDetails.get(19).select("td").get(1).text();
		}
		
		// Set attributes to the CR
		cr.setTitle(summary);
		cr.setDescription(description);
		cr.setStepsToReproduce(steps);
		cr.setOrigin(component);
		cr.setProject(project);
		cr.setPriority(priority);
		cr.setSeverity(severity);
		cr.setStatus(status);
		cr.setResolution(resolution);
		cr.setPlatform(platform);
		cr.setVersion(version);
		cr.setOperatingSystem(operatingSystem);
		cr.setCreatedBy(new Author(createdBy, null));
		cr.setFixedBy(new Author(fixedBy, null));
		// cr.setCategory(category);
		try {
			cr.setDateCreated(simpleDateFormat.parse(creationDate));
			cr.setDateUpdated(simpleDateFormat.parse(lastUpdateDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private void extractCRCommentsFromHTML(Document doc, ChangeRequest cr) {
		Elements commentRows = doc.select(".bugnote");
		for (Element commentRow : commentRows) {
			Elements bugnote = commentRow.select(".bugnote-public");
			boolean privated = false;
			if (!bugnote.hasText()) {
				bugnote = commentRow.select(".bugnote-private");
				privated = true;
			}
			
			String authorName = null;
			
			if (bugnote.select("a").size() > 1) {
				authorName = bugnote.select("a").get(1).text();
			} else {
				authorName = bugnote.select("font").get(0).text();
			}
			
			String dateStr;
			if (privated) {
				dateStr = bugnote.select("span").get(3).text();
			} else {
				dateStr = bugnote.select("span").get(2).text();	
			}
			
			String commentStr = null;
			if (privated) {
				commentStr = commentRow.select(".bugnote-note-private").get(0).text();
			} else {
				commentStr = commentRow.select(".bugnote-note-public").get(0).text();
			}

			Comment comment = new Comment();
			comment.setAuthor(new Author(authorName, null));
			comment.setComment(commentStr);
			try {
				comment.setDateCreate(simpleDateFormat.parse(dateStr));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			cr.getComments().add(comment);
		}
	}

	public void extractCRHistoryFromHTML(Document doc, ChangeRequest cr) {
		Elements historyDiv = doc.select("div#history_open");
		Elements historyTable = historyDiv.select("table");
		extractInformationFromRow(historyTable.select(".row-1").iterator(), cr);
		extractInformationFromRow(historyTable.select(".row-2").iterator(), cr);
	}

	private void extractInformationFromRow(Iterator<Element> rows1Iter,
			ChangeRequest cr) {
		while (rows1Iter.hasNext()) {
			History history = new History();
			history.setChangeRequest(cr);
			cr.getHistory().add(history);

			Elements TDs = rows1Iter.next().select("td");

			// Get the date
			String dateStr = TDs.get(0).text().trim();
			Date date;
			try {
				date = simpleDateFormat.parse(dateStr);
				history.setDate(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			// Get reporter
			String name = TDs.get(1).text().trim();
			history.setAuthor(new Author(name, null));
			// Get field
			String field = TDs.get(2).text().trim();
			history.setField(field);
			// Get change
			String change = TDs.get(3).text().trim();
			history.setChange(change);
		}
	}

	public static void main(String[] args) {
		for (ChangeRequest cr : new MantisSERPROChangeRequestFetcherHTML()
				.findAllChangeRequests()) {
		}
	}

}
