package com.smartcr.fetch.bugzilla;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.smartcr.core.index.Fetcher;
import com.smartcr.core.index.IChangeRequestFetcher;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.RepositoryConfig;

public class BugzillaCRFetcherXML implements IChangeRequestFetcher {

	static Logger logger = Logger.getLogger(BugzillaCRFetcherXML.class
			.getName());
	private RepositoryConfig repositoryConfig;

	@Override
	public void configure(RepositoryConfig config) {
		repositoryConfig = config;
	}

	@Override
	public List<ChangeRequest> findAllChangeRequests() {
		List<ChangeRequest> crs = new ArrayList<ChangeRequest>();
		for (String id : findAvailableChangeRequestIDs()) {
			crs.add(getChangeRequest(id));
			if (repositoryConfig.getLimit() != null
					&& crs.size() >= repositoryConfig.getLimit()) {
				break;
			}
		}
		return crs;
	}

	public Element getXMLChangeRequest(String url)
			throws MalformedURLException, SAXException, IOException,
			ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new URL(url).openStream());
		return doc.getDocumentElement();
	}

	private String getTextValue(Element doc, String tag) {
		String value = null;
		NodeList nl;
		nl = doc.getElementsByTagName(tag);
		if (nl.getLength() > 0 && nl.item(0).hasChildNodes()) {
			value = nl.item(0).getFirstChild().getNodeValue();
		}
		return value;
	}

	private String getTextAttrValue(Element doc, String tag, String attr) {
		String value = null;
		NodeList nl;
		nl = doc.getElementsByTagName(tag);
		if (nl.getLength() > 0 && nl.item(0).hasAttributes()) {
			Node at = nl.item(0).getAttributes().getNamedItem(attr);
			value = at.getTextContent();
		}
		return value;
	}

	public ChangeRequest getChangeRequestFromXML(String id) {
		ChangeRequest cr = new ChangeRequest();
		String dateFormat = "yyyy-MM-dd HH:mm:ss Z";
		SimpleDateFormat dtFormat = new SimpleDateFormat(dateFormat);
		try {
			String getXML = repositoryConfig.getUrl()
					+ "/show_bug.cgi?ctype=xml&id=" + id;
			Element elBug = getXMLChangeRequest(getXML);
			cr.setOriginalId(getTextValue(elBug, "bug_id"));
			cr.setProject(getTextValue(elBug, "product"));
			cr.setStatus(getTextValue(elBug, "bug_status"));
			cr.setSeverity(getTextValue(elBug, "bug_severity"));
			cr.setResolution(getTextValue(elBug, "resolution"));
			cr.setTitle(getTextValue(elBug, "short_desc"));
			cr.setPriority(getTextValue(elBug, "priority"));
			cr.setTarget(getTextValue(elBug, "component"));
			cr.setPlatform(getTextValue(elBug, "rep_platform"));
			cr.setVersion(getTextValue(elBug, "version"));
			cr.setOperatingSystem(getTextValue(elBug, "op_sys"));
			cr.setDateCreated(dtFormat
					.parse(getTextValue(elBug, "creation_ts")));
			cr.setDateUpdated(dtFormat.parse(getTextValue(elBug, "delta_ts")));

			/*
			 * Bugzilla does not have an specific field to the date of closing,
			 * thus we try to infer it.
			 */
			if (StringUtils.isNotEmpty(cr.getResolution())) {
				cr.setDateClosed(cr.getDateUpdated());
			}

			extractFixedby(cr, elBug);
			extractCreatedby(cr, elBug);
			extractComments(cr, elBug);
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			return null;
		}
		return cr;
	}

	private void extractComments(ChangeRequest cr, Element elBug) {
		NodeList nodeComments = elBug.getElementsByTagName("long_desc");
		Element elDescription = (Element) nodeComments.item(0);
		cr.setDescription(getTextValue(elDescription, "thetext"));

		for (int i = 1; i < nodeComments.getLength(); i++) {
			Element elComment = (Element) nodeComments.item(i);
			com.smartcr.core.model.Comment comment = new com.smartcr.core.model.Comment();
			comment.setChangeRequest(cr);
			comment.setComment(getTextValue(elComment, "thetext"));
			comment.setOriginalId(getTextValue(elComment, "commentid"));
			String creatorLogin = getTextValue(elComment, "who");
			String creatorName = getTextAttrValue(elComment, "who", "name");
			comment.setAuthor(new Author(creatorName, creatorLogin));
			cr.getComments().add(comment);
		}
	}

	private void extractCreatedby(ChangeRequest cr, Element elBug) {
		String emailReporter = getTextValue(elBug, "reporter");
		String nameReporter = getTextAttrValue(elBug, "reporter", "name");
		cr.setCreatedBy(new Author(nameReporter, emailReporter));
	}

	private void extractFixedby(ChangeRequest cr, Element elBug) {
		String emailAssignedTo = getTextValue(elBug, "assigned_to");
		String nameAssignedTo = getTextAttrValue(elBug, "assigned_to", "name");
		boolean isNobodyEmail = emailAssignedTo != null
				&& emailAssignedTo.toLowerCase().startsWith("nobody");
		boolean isNobodyName = nameAssignedTo != null
				&& nameAssignedTo.toLowerCase().startsWith("nobody");
		if (isNobodyEmail || isNobodyName) {
			cr.setFixedBy(null);
			logger.info(String.format("AssignedTo not found: %s (%s)",
					nameAssignedTo, emailAssignedTo));
		} else {
			cr.setFixedBy(new Author(nameAssignedTo, emailAssignedTo));
		}
	}

	@Override
	public ChangeRequest getChangeRequest(String id) {
		return getChangeRequestFromXML(id);
	}

	@Override
	public Set<String> findAvailableChangeRequestIDs() {
		return repositoryConfig.getIds();
	}

	@Override
	public void startWorkSession() {
		// TODO Auto-generated method stub

	}

	@Override
	public void stopWorkSession() {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) {
		RepositoryConfig config = new RepositoryConfig();
		config.setUrl("https://bugzilla.mozilla.org/");
		Fetcher f = new Fetcher(new BugzillaCRFetcherXML(), config,
				DefaultProgressMonitor.getInstance());
		System.out.println(f.getFetcherImpl().getChangeRequest("328231"));
		// System.out.println(f.getChangeRequest("428232"));
		// System.out.println(f.getChangeRequest("883253"));
		//
		// RepositoryConfig config2 = new RepositoryConfig();
		// config2.setUrl("https://bugs.eclipse.org/bugs/");
		// BugzillaCRFetcher f2 = new BugzillaCRFetcher(config2,
		// SysoutProgressMonitor.getInstance());
		// System.out.println(f2.getChangeRequest("328231"));
	}

}
