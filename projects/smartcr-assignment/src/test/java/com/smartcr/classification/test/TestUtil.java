package com.smartcr.classification.test;

import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.model.History;

public class TestUtil {

	public static ChangeRequest createCR(Long id, String fixedbyName, String fixedbyEmail) {
		ChangeRequest cr = createCR();
		cr.setId(id);
		cr.getFixedBy().setName(fixedbyName);
		cr.getFixedBy().setEmail(fixedbyEmail);
		return cr;
	}
	
	public static ChangeRequest createCR(String fixedbyName, String fixedbyEmail) {
		ChangeRequest cr = createCR();
		cr.getFixedBy().setId(null);
		cr.getFixedBy().setName(fixedbyName);
		cr.getFixedBy().setEmail(fixedbyEmail);
		return cr;
	}
	
	public static ChangeRequest createCR(Long id) {
		ChangeRequest cr = createCR();
		cr.setId(id);
		return cr;
	}
	
	public static ChangeRequest createCR() {
		ChangeRequest cr = new ChangeRequest();
		cr.setProject("TestProject");
		cr.setTitle("CR Test");
		
		Author author = new Author("Yguarata", "yguarata@gmail.com");
		
		cr.setCreatedBy(author);
		cr.setFixedBy(author);
		
		Comment comment = new Comment();
		comment.setAuthor(author);
		comment.setChangeRequest(cr);
		comment.setComment("Some comment");
		cr.getComments().add(comment);
		
		History history = new History();
		history.setAuthor(author);
		history.setChangeRequest(cr);
		history.setChange("Added comment");
		history.setField("notes");
		cr.getHistory().add(history);
		
		return cr;
	}

}
