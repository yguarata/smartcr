package com.smartcr.classification.test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import weka.classifiers.Evaluation;

import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.classification.assignment.impl.SVMWekaCRAssigner;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.DefaultProgressMonitor;

public class SVMWekaTest {

	@Test
	public void testClassifier() throws LearningException {
		SVMWekaCRAssigner svm = SVMWekaCRAssigner.getInstance();
		List<ChangeRequest> crs = new ArrayList<ChangeRequest>();
		crs.add(TestUtil.createCR(1l, "John", "john@gmail.com"));
		crs.add(TestUtil.createCR(2l, "luck", "luck@gmail.com"));
		crs.add(TestUtil.createCR(3l, "jorge", "jorge@gmail.com"));
		crs.add(TestUtil.createCR(4l, "one", "one@gmail.com"));
		crs.add(TestUtil.createCR(5l, "two", "two@gmail.com"));
		svm.train(crs, DefaultProgressMonitor.getInstance());

		try {
			Evaluation eTest = new Evaluation(svm.getTrainingSet());
			eTest.evaluateModel(svm.getClassifier(), svm.getTrainingSet());
			String strSummary = eTest.toSummaryString();
			System.out.println(strSummary);

			ChangeRequest crTest = TestUtil.createCR(10l, "luck", "luck@gmail.com");
			Author dev = svm.classifyCR(crTest);

			// The SVM classes must contain the recommend dev.
			Assert.assertTrue(svm.getClasses().contains(dev.getEmail()));

			// The assigned dev must be the first of the recommendation list.
			Assert.assertEquals(svm.recommendDevelopers(crTest).firstKey(), dev);
			
			System.out.println("Assigned to: " + dev);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
