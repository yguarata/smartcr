package com.smartcr.classification.test;

import junit.framework.Assert;

import org.junit.Test;

import com.smartcr.classification.assignment.impl.DroolsRulesExecutor;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Rule;
import com.smartcr.core.model.Rule.RuleType;
import com.smartcr.core.model.persitence.RuleDAO;

public class DroolsRulesExecutorTest extends TestBase {

	@Test
	public void test() {
		Rule simpleRule = new Rule();
		simpleRule.setName("Assign componentA");
		String r = "import com.smartcr.classification.test.core.test.core.classification.model.*" +
				"rule \"Simple\" " +
				"when $cr: ChangeRequest(origin == \"componentA\") " +
				"then $cr.setStatus(\"newStatus\"); " +
				"end";
		simpleRule.setRuleContent(r);
		simpleRule.setRuleType(RuleType.SIMPLE);
		RuleDAO.getInstance().insert(simpleRule);
		
		Assert.assertNotNull(simpleRule.getId());

		ChangeRequest cr = TestUtil.createCR();
		cr.setOrigin("componentA");

		DroolsRulesExecutor.getInstance().fireSimpleRules(cr, null);

		Assert.assertEquals("newStatus", cr.getStatus());
	}

}
