package com.smartcr.classification.test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.smartcr.classification.assignment.IAssignmentStrategy.StrategyMode;
import com.smartcr.classification.assignment.impl.AssignmentStrateyImpl;
import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.classification.assignment.impl.SVMWekaCRAssigner;
import com.smartcr.classification.context.RandomWorkloadBalancing;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Rule;
import com.smartcr.core.model.Rule.RuleType;
import com.smartcr.core.model.persitence.BaseDAO;
import com.smartcr.core.model.persitence.ChangeRequestDAO;
import com.smartcr.core.model.persitence.DAORegister;
import com.smartcr.core.model.persitence.RuleDAO;
import com.smartcr.core.util.DefaultProgressMonitor;

public class AssignmentStrategyTest extends TestBase {

	BaseDAO<ChangeRequest> crDAO = ChangeRequestDAO.getInstance();
	RuleDAO ruleDAO = RuleDAO.getInstance();

	@Before
	@After
	public void clean() {
		DAORegister.getInstance().configureDatabase(databaseConfig);
	}

	@Test
	public void test() throws LearningException, FileNotFoundException {
		AssignmentStrateyImpl assImpl = AssignmentStrateyImpl.getInstance();
		SVMWekaCRAssigner svm = SVMWekaCRAssigner.getInstance();

		svm.train(createCRsToTrain(), DefaultProgressMonitor.getInstance());

		ChangeRequest crTest = createCRToassign();
		Author dev = svm.classifyCR(crTest);

		boolean assigned = assImpl.assignCR(crTest,
				new RandomWorkloadBalancing(), StrategyMode.IR_ONLY);

		assertTrue(assigned);
		assertEquals(dev, crTest.getFixedBy());

		crTest.setFixedBy(null);

		assigned = assImpl.assignCR(crTest, new RandomWorkloadBalancing(),
				StrategyMode.SIMPLE_RULES_ONLY);
		assertFalse(assigned);

		createSimpleRules();
		assigned = assImpl.assignCR(crTest, new RandomWorkloadBalancing(),
				StrategyMode.SIMPLE_RULES_ONLY);
		assertTrue(assigned);

		crTest.setFixedBy(null);
		
		createComplexRules();
		assigned = assImpl.assignCR(crTest, new RandomWorkloadBalancing(),
				StrategyMode.COMPLETE);
		assertTrue(assigned);
	}

	private void createSimpleRules() throws FileNotFoundException {
		Rule splRule = new Rule();
		splRule.setName("Simple rules");
		URL rulesURL = getClass().getResource("simpleRulesForTest.drl");
		File file = new File(rulesURL.getFile());
		Scanner scanner = new Scanner(file);
		splRule.setRuleContent(scanner.useDelimiter("\\A").next());
		scanner.close();
		splRule.setRuleType(RuleType.SIMPLE);

		ruleDAO.insert(splRule);
	}

	private void createComplexRules() throws FileNotFoundException {
		Rule splRule = new Rule();
		splRule.setName("Complex rules");
		URL rulesURL = getClass().getResource("complexRulesForTest.drl");
		File file = new File(rulesURL.getFile());
		Scanner scanner = new Scanner(file);
		splRule.setRuleContent(scanner.useDelimiter("\\A").next());
		scanner.close();
		splRule.setRuleType(RuleType.COMPLEX);

		ruleDAO.insert(splRule);
	}

	private List<ChangeRequest> createCRsToTrain() {
		List<ChangeRequest> crs = new ArrayList<ChangeRequest>();
		crs.add(TestUtil.createCR("John", "john@gmail.com"));
		crs.add(TestUtil.createCR("luck", "luck@gmail.com"));
		crs.add(TestUtil.createCR("jorge", "jorge@gmail.com"));
		crs.add(TestUtil.createCR("one", "one@gmail.com"));
		crs.add(TestUtil.createCR("two", "two@gmail.com"));

		for (ChangeRequest cr : crs) {
			crDAO.insert(cr);
		}
		return crs;
	}

	private ChangeRequest createCRToassign() {
		ChangeRequest crTest = new ChangeRequest();
		crTest.setOrigin("componentA");
		crDAO.insert(crTest);
		return crTest;
	}

}
