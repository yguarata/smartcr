package com.smartcr.classification.assignment.impl;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import weka.core.Instance;

import com.smartcr.classification.GenericWEKAClassifier;
import com.smartcr.classification.assignment.IChangeRequestAssigner;
import com.smartcr.classification.assignment.IDeveloperRecommender;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.DefaultProgressMonitor;

public class SVMWekaCRAssigner extends GenericWEKAClassifier implements 
		IChangeRequestAssigner, IDeveloperRecommender {
	
	static Logger logger = Logger.getLogger(SVMWekaCRAssigner.class.getName());
	private static SVMWekaCRAssigner singleton;

	public static SVMWekaCRAssigner getInstance() {
		if (singleton == null) {
			singleton = new SVMWekaCRAssigner();
		}
		return singleton;
	}

	private SVMWekaCRAssigner() {
	}

	protected String getClassificationClass(ChangeRequest cr) {
		return cr.getFixedBy().getEmail();
	}

	public Author classifyCR(ChangeRequest changeRequest) {
		logger.info("Choosing author for CR " + changeRequest.getId() + "...");
		String email;
		try {
			email = classify(createInstanceToClassify(changeRequest));
			return new Author(null, email);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private Map<Author, Double> recommendAuthorsForCR(ChangeRequest cr) {
		return distributions(createInstanceToClassify(cr));
	}

	/**
	 * Recommends a list of author for a CR using SVM.
	 * 
	 * @param cr
	 * @return
	 * @throws LearningException
	 */
	@Override
	public SortedMap<Author, Double> recommendDevelopers(ChangeRequest cr)
			throws LearningException {
		logger.info("Recommeding authors for CR " + cr.getId() + "...");

		if (!isTrained()) {
			String msg = "The classifier is not trained. Train it first!";
			logger.log(Level.SEVERE, msg);
			throw new LearningException(msg);
		}

		final Map<Author, Double> map = recommendAuthorsForCR(cr);

		SortedMap<Author, Double> sortedMap = new TreeMap<Author, Double>(
				new Comparator<Author>() {
					public int compare(Author o1, Author o2) {
						return Double.compare(map.get(o2), map.get(o1));
					};
				});

		sortedMap.putAll(map);

		logger.info(sortedMap.toString());

		return sortedMap;
	}

	private Map<Author, Double> distributions(Instance cr) {
		try {
			Map<Author, Double> dists = new HashMap<Author, Double>();
			double[] pred = getClassifier().distributionForInstance(cr);
			for (int i = 0; i < pred.length; i++) {
				String email = getTrainingSet().classAttribute().value(i);
				Double dist = pred[i];
				dists.put(new Author(null, email), dist);
			}
			return dists;
		} catch (Exception e) {
			logger.log(Level.WARNING, "Could not compute the distributions.", e);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean assignCR(ChangeRequest cr) {
		Author oldAuthor = cr.getFixedBy();
		cr.setFixedBy(classifyCR(cr));
		return cr.getFixedBy() != null && !cr.getFixedBy().equals(oldAuthor);
	}

	public static void main(String[] args) throws LearningException {
		new SVMWekaCRAssigner().train(null,
				DefaultProgressMonitor.getInstance());
	}
	
}
