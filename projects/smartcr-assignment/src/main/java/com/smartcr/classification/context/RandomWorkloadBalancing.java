package com.smartcr.classification.context;

import java.util.Set;

import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;

public class RandomWorkloadBalancing implements IWorkloadBalancing {

	@Override
	public Author getAvailableDeveloper(Set<Author> recommendedAuthors, ChangeRequest cr) {
		int random = 0 + (int)Math.random() * (recommendedAuthors.size() - 1);
		return (Author)recommendedAuthors.toArray()[random];
	}
}
