package com.smartcr.classification.assignment.impl;

public class LearningException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 33034302884994310L;

	public LearningException(String msg) {
		super(msg);
	}
	
	public LearningException(String msg, Exception e) {
		super(msg, e);
	}
	
}
