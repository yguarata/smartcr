package com.smartcr.classification.assignment;

import java.util.SortedMap;

import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;

public interface IDeveloperRecommender {

	public SortedMap<Author, Double> recommendDevelopers(ChangeRequest cr) throws LearningException;
	
}
