package com.smartcr.classification;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

import weka.classifiers.functions.LibSVM;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.stemmers.SnowballStemmer;
import weka.filters.unsupervised.attribute.StringToWordVector;

import com.smartcr.classification.assignment.ILearning;
import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.IProgressMonitor;
import com.smartcr.core.util.Task;

public abstract class GenericWEKAClassifier implements ILearning {

	private FilteredClassifier classifier;
	private Instances trainingSet;
	private FastVector attributes;
	private FastVector classes;
	private boolean training = false;
	private boolean trained = false;
	static Logger logger = Logger.getLogger(GenericWEKAClassifier.class.getName());
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ChangeRequest> train(List<ChangeRequest> trainingSetCRs,
			IProgressMonitor progressMonitor) throws LearningException {
		logger.info("Training the classifyer...");
		training = true;
		List<ChangeRequest> notUsedCRS = new ArrayList<ChangeRequest>();
		configureAttributes(trainingSetCRs);
		this.trainingSet = new Instances("crs", attributes,
				trainingSetCRs.size());
		this.trainingSet.setClassIndex(this.trainingSet.numAttributes() - 1);

		if (progressMonitor == null) {
			progressMonitor = DefaultProgressMonitor.getInstance();
		}

		Task task = progressMonitor.beginTask("Creating the traning set",
				trainingSetCRs.size());

		for (ChangeRequest cr : trainingSetCRs) {
			try {
				Instance example = createInstance(cr);
				this.trainingSet.add(example);
				task.unitWorked("Added cr " + cr.getId());
			} catch (Exception e) {
				notUsedCRS.add(cr);
				logger.log(Level.WARNING,
						"Could not use the CR id=" + cr.getId()
								+ " in the classification.", e);
			}
		}

		StringToWordVector filter = new StringToWordVector();
		filter.setUseStoplist(true);
		filter.setStopwords(new File("stopwords-pt-br.txt"));
		filter.setStemmer(new SnowballStemmer("portuguese"));
		filter.setAttributeIndices("2"); // Starts from 1
		classifier = new FilteredClassifier();
		LibSVM libSVM = new LibSVM();
		// WLSVM svm = new WLSVM();
		// svm.setProbability(1); // Necessary to get distributions
		classifier.setClassifier(libSVM);
		classifier.setFilter(filter);
		try {
			classifier.buildClassifier(this.trainingSet);
		} catch (Exception e) {
			String msg = "The classifier could not be built!";
			logger.log(Level.SEVERE, msg, e);
			throw new LearningException(msg, e);
		}

		training = false;
		trained = true;
		return notUsedCRS;
	}

	protected void configureAttributes(List<ChangeRequest> trainingSetCRs) {
		setAttributes(new FastVector(3));
		getAttributes().addElement(new Attribute("crID"));
		getAttributes().addElement(new Attribute("contents", (FastVector) null));

		setClasses(new FastVector());
		
		for (ChangeRequest cr : trainingSetCRs) {
			String class_ = getClassificationClass(cr);
			if (!getClasses().contains(class_) && StringUtils.isNotEmpty(class_)) {
				getClasses().addElement(class_);
			}
		}

		getAttributes().addElement(new Attribute("class", getClasses()));

		for (Object o : getClasses().toArray()) {
			logger.info("SVM Classe: " + o.toString());
		}
	}
	
	protected Instance createInstance(ChangeRequest cr) {
		String text = getIndexableText(cr);
		Instance example = new Instance(3);
		example.setValue((Attribute) getAttributes().elementAt(0), cr.getId());
		example.setValue((Attribute) getAttributes().elementAt(1), text);
		example.setValue((Attribute) getAttributes().elementAt(2), getClassificationClass(cr));
		example.setDataset(getTrainingSet());
		return example;
	}
	
	public Instance createInstanceToClassify(ChangeRequest cr) {
		String text = getIndexableText(cr);
		Instance example = new Instance(3);
		example.setValue((Attribute) getAttributes().elementAt(0), cr.getId());
		example.setValue((Attribute) getAttributes().elementAt(1), text);
		example.setMissing(2);
		example.setDataset(getTrainingSet());
		return example;
	}
	
	public void invalidate() {
		trained = false;
		training = false;
	}
	
	protected String getIndexableText(ChangeRequest cr) {
		String text = cr.getTitle() + " " + cr.getDescription() + " "
				+ cr.getStepsToReproduce();
		for (Comment c: cr.getComments()) {
			text += " " + c.getComment();
		}
		return text;
	}
	
	protected String classify(Instance cr) throws Exception {
		double pred = classifier.classifyInstance(cr);
		String class_ = this.trainingSet.classAttribute().value((int) pred);
		return class_;
	}
	
	protected abstract String getClassificationClass(ChangeRequest cr);
	
	public FastVector getAttributes() {
		return attributes;
	}

	public void setAttributes(FastVector attributes) {
		this.attributes = attributes;
	}

	public void setClasses(FastVector classes) {
		this.classes = classes;
	}

	public void setTraining(boolean training) {
		this.training = training;
	}

	public void setTrained(boolean trained) {
		this.trained = trained;
	}
	
	public boolean isTrained() {
		return this.trained;
	}
	
	public FilteredClassifier getClassifier() {
		return classifier;
	}

	public void setClassifier(FilteredClassifier classifier) {
		this.classifier = classifier;
	}

	public boolean isTraining() {
		return training;
	}
	
	public FastVector getClasses() {
		return classes;
	}

	public Instances getTrainingSet() {
		return trainingSet;
	}

	public void setTrainingSet(Instances training) {
		this.trainingSet = training;
	}
}
