package com.smartcr.classification.context;

import java.util.Set;

import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;

public interface IWorkloadBalancing {

	public Author getAvailableDeveloper(Set<Author> recommendedAuthors, ChangeRequest assigningCR);
	
}
