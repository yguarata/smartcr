package com.smartcr.classification.context;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;

import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.History;
import com.smartcr.core.model.persitence.AuthorDAO;
import com.smartcr.core.model.persitence.ChangeRequestDAO;

class DeveloperVacation {
	String name;
	Date dateStart;
	Date dateEnd;
}

class Vacation {
	List<DeveloperVacation> vacations = null;
	
	void loadFile() throws IOException, ParseException {
		if (vacations != null && !vacations.isEmpty()) {
			return;
		}
		vacations = new ArrayList<DeveloperVacation>();
		URL url = MantisWorkloadBalancing.class.getResource("FeriasSIAFI.csv");
		BufferedReader buff = new BufferedReader(new FileReader(url.getPath()));
		String line = null;
		
		while ((line = buff.readLine()) != null) {
			StringTokenizer st = new StringTokenizer(line, ",");
			DeveloperVacation dv = new DeveloperVacation();
			dv.name = st.nextToken().toLowerCase();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
			dv.dateStart = sdf.parse(st.nextToken());
			dv.dateStart.setHours(0);
			dv.dateStart.setMinutes(0);
			dv.dateStart.setSeconds(0);
			dv.dateEnd = sdf.parse(st.nextToken());
			dv.dateEnd.setHours(0);
			dv.dateEnd.setMinutes(0);
			dv.dateEnd.setSeconds(0);
			vacations.add(dv);
		}
		buff.close();
	}
	
	public boolean isInVacation(String devName, Date date) {
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);
		boolean inVacation = false;
		try {
			loadFile();
			for (DeveloperVacation dv: vacations) {
				if (dv.name.equals(devName.toLowerCase())) {
					boolean inclusiveBetween = (date.after(dv.dateStart) && date.before(dv.dateEnd)) || 
							(DateUtils.isSameDay(date, dv.dateStart) || DateUtils.isSameDay(date, dv.dateEnd));
					if (inclusiveBetween) {
						inVacation = true;
						break;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return inVacation;
	}
}

public class MantisWorkloadBalancing implements IWorkloadBalancing {

	private static int TIME_WINDOW_DAYS = 5;
	private Vacation vacation = new Vacation();

	public void testVacation() {
		Vacation v = new Vacation();
		Calendar cal = Calendar.getInstance();
		cal.set(2011, 11-1, 25);
		System.out.println(v.isInVacation("RODRIGO BITTENCOURT MOTTA", cal.getTime()));
		cal.set(2011, 11-1, 16);
		System.out.println(v.isInVacation("RODRIGO BITTENCOURT MOTTA", cal.getTime()));
		cal.set(2011, 11-1, 18);
		System.out.println(v.isInVacation("RODRIGO BITTENCOURT MOTTA", cal.getTime()));
		cal.set(2011, 11-1, 15);
		System.out.println(v.isInVacation("RODRIGO BITTENCOURT MOTTA", cal.getTime()));
		cal.set(2011, 11-1, 26);
		System.out.println(v.isInVacation("RODRIGO BITTENCOURT MOTTA", cal.getTime()));
	}
	
	@Override
	public Author getAvailableDeveloper(Set<Author> recommendedAuthors,
			ChangeRequest assigningCR) {
		Date start = assigningCR.getDateCreated();
		Calendar c = Calendar.getInstance();
		c.setTime(start);
		c.add(Calendar.DAY_OF_MONTH, TIME_WINDOW_DAYS);
		Date end = c.getTime();

		Map<Author, Integer> countCRs = new HashMap<Author, Integer>();
		ChangeRequest example = new ChangeRequest();
		example.setProject(assigningCR.getProject());

		// Count the assignments of each recommended developer according to the
		// TIME_WINDOW_DAYS.
		for (ChangeRequest cr : ChangeRequestDAO.getInstance().findByExample(
				example)) {
			for (History h : cr.getHistory()) {
				boolean between = h.getDate().after(start)
						&& h.getDate().before(end);
				if (between && h.getField().toLowerCase().contains("atribuído")
						&& !h.getChange().trim().endsWith("=>")) {
					String[] name = h.getChange().split("=>");
					Author a;
					if (name.length > 1) {
						a = AuthorDAO.getInstance().findByName(name[1].trim());
						if (recommendedAuthors.contains(a)) {
							if (!countCRs.containsKey(a)) {
								countCRs.put(a, 1);
							} else {
								countCRs.put(a, countCRs.get(a) + 1);
							}
						}
					}
				}
			}
		}

		// Get the developer with less assignments
		int i = 5000;
		Author selected = null;
		for (Entry<Author, Integer> entry : countCRs.entrySet()) {
			String devName = entry.getKey().getName();
			boolean inVacation = //vacation.isInVacation(devName, start);
				vacation.isInVacation(devName, end);
			if (entry.getValue() < i && !inVacation) {
				i = entry.getValue();
				selected = entry.getKey();
			}
		}

		if (selected == null) {
			selected = recommendedAuthors.iterator().next();
		}
		System.out.println("Selected: " + selected.getName() + ": " + i);

		return selected;
	}
	
	public static void main(String[] args) {
		new MantisWorkloadBalancing().testVacation();
	}
}
