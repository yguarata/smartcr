package com.smartcr.classification.context;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.smartcr.core.model.Author;
import com.smartcr.core.model.Project;

public interface IDeveloperAvailability {

	/**
	 * Returns a map indicating which developers are available (allocated) for
	 * each project.
	 * 
	 * @return
	 */
	public Map<Project, Set<Author>> getDevelopersAllocation();

	/**
	 * Returns the list of the programmers. Programmers are developers who
	 * actually changed the software in order to fix a given CR.
	 * 
	 * @return
	 */
	public List<Author> getProgrammers();

}
