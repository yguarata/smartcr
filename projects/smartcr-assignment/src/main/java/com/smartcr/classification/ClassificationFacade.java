package com.smartcr.classification;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections.CollectionUtils;

import com.smartcr.classification.assignment.IAssignmentStrategy.StrategyMode;
import com.smartcr.classification.assignment.impl.AssignmentStrateyImpl;
import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.classification.assignment.impl.SVMWekaCRAssigner;
import com.smartcr.classification.context.IWorkloadBalancing;
import com.smartcr.classification.context.RandomWorkloadBalancing;
import com.smartcr.classification.context.WorkloadBalancingType;
import com.smartcr.core.ModelFacade;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.IProgressMonitor;

public class ClassificationFacade {

	static Logger logger = Logger.getLogger(ClassificationFacade.class.getName());

	private static ClassificationFacade coreFacadeInstance = null;

	private ClassificationFacade() {

	}

	public static ClassificationFacade getInstance() {
		if (coreFacadeInstance == null) {
			coreFacadeInstance = new ClassificationFacade();
		}

		return coreFacadeInstance;
	}

	public boolean isClassifierTrained() {
		return SVMWekaCRAssigner.getInstance().isTrained();
	}

	public boolean isClassifierTraining() {
		return SVMWekaCRAssigner.getInstance().isTraining();
	}

	public void invalidateClassifierTraining() {
		SVMWekaCRAssigner.getInstance().invalidate();
	}

	public Author choseAuthorForCR(ChangeRequest changeRequest) {
		Author newAuthor = SVMWekaCRAssigner.getInstance().classifyCR(
				changeRequest);
		if (newAuthor != null) {
			newAuthor = ModelFacade.getInstance().getAuthorByEmail(
					newAuthor.getEmail());
		} else {
			logger.log(Level.WARNING, "Could not choose an author for the CR.");
		}
		return newAuthor;
	}

	public SortedMap<Author, Double> recommendAuthorsForCR(ChangeRequest cr)
			throws LearningException {
		return SVMWekaCRAssigner.getInstance().recommendDevelopers(cr);
	}

	/**
	 * Assigns the CR to the developer referred by the given email.
	 * 
	 * @param cr
	 * @param authorEmail
	 */
	public void assignCR(ChangeRequest cr, String authorEmail) {
		Author dev = ModelFacade.getInstance().getAuthorByEmail(authorEmail);
		if (dev != null) {
			cr.setFixedBy(dev);
		}
	}

	public void assignCR(ChangeRequest cr, String[] authorsEmail, WorkloadBalancingType type) {
		Set<Author> authors = new HashSet<Author>();
		for (String email: authorsEmail) {
			authors.add(ModelFacade.getInstance().getAuthorByEmail(email));
		}
		Author dev = type.getBalancing().getAvailableDeveloper(authors, cr);
		if (dev != null) {
			cr.setFixedBy(dev);
		}
	}
	
	public void assignCR(ChangeRequest cr, Set<Author> authors, WorkloadBalancingType type) {
		Set<Author> authorsFromDB = new HashSet<Author>();
		for (Author a: authors) {
			authorsFromDB.add(ModelFacade.getInstance().getAuthorByEmail(a.getEmail()));
		}
		Author dev = type.getBalancing().getAvailableDeveloper(authorsFromDB, cr);
		if (dev != null) {
			cr.setFixedBy(dev);
		}
	}
	
	public void assignCR(ChangeRequest cr, Author author) {
		this.assignCR(cr, author.getEmail());
	}

	/**
	 * Assigns a CR using the default workload balancing and strategy.
	 * 
	 * @param cr
	 * @return
	 * @throws LearningException
	 */
	public boolean assignCR_(ChangeRequest cr) throws LearningException {
		return assignCR_(cr, new RandomWorkloadBalancing(),
				StrategyMode.COMPLETE);
	}

	/**
	 * Assigns a CR using the default strategy with the given workload
	 * balancing.
	 * 
	 * @param cr
	 * @param workloadBalancing
	 * @return
	 * @throws LearningException
	 */
	public boolean assignCR_(ChangeRequest cr,
			IWorkloadBalancing workloadBalancing) throws LearningException {
		return assignCR_(cr, workloadBalancing, StrategyMode.COMPLETE);
	}

	public boolean assignCR(ChangeRequest cr,
			WorkloadBalancingType balancingType) throws LearningException {
		IWorkloadBalancing balancing = balancingType.getBalancing();
		Set<Author> authors = recommendAuthorsForCR(cr).keySet();
		if (authors != null && !authors.isEmpty()) {
			Author author = balancing.getAvailableDeveloper(authors, cr);
			this.assignCR(cr, author);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Assign the CR to the proper developer, given the workload balancing
	 * method and the strategy mode. Should not, ever, called in the drools
	 * rules.
	 * 
	 * TODO: remove it from the drools scope.
	 * 
	 * @param cr
	 * @param workloadBalancing
	 * @param strategyMode
	 * @throws LearningException
	 */
	public boolean assignCR_(ChangeRequest cr,
			IWorkloadBalancing workloadBalancing, StrategyMode strategyMode)
			throws LearningException {
		if (AssignmentStrateyImpl.getInstance().assignCR(cr, workloadBalancing,
				strategyMode)) {
			return true;
		}
		return false;
	}

	/**
	 * Train the CR assigned by considering the assigned CRs and returns the CRs
	 * that could not be using in the training.
	 * 
	 * @param assignedCRs
	 * @return
	 * @throws LearningException
	 */
	public List<ChangeRequest> trainCRAssigner(IProgressMonitor progressMonitor)
			throws LearningException {
		List<ChangeRequest> assignedCRs = ModelFacade.getInstance()
				.findAssignedCRs();
		return trainAssigner(progressMonitor, assignedCRs);
	}

	/**
	 * Train the CR assigned by considering the assigned CRs passed by arguments
	 * and returns the CRs that could not be using in the training.
	 * 
	 * @param assignedCRs
	 * @return
	 * @throws LearningException
	 */
	public List<ChangeRequest> trainCRAssigner(List<ChangeRequest> crs,
			IProgressMonitor progressMonitor) throws LearningException {
		return trainAssigner(progressMonitor, crs);
	}

	/**
	 * Train the CR assigned by considering the assigned CRs of the given
	 * projects and returns the CRs that could not be using in the training.
	 * 
	 * @param assignedCRs
	 * @return
	 * @throws LearningException
	 */
	public List<ChangeRequest> trainCRAssignerByProject(
			IProgressMonitor progressMonitor, String project)
			throws LearningException {
		List<ChangeRequest> assignedCRs = ModelFacade.getInstance()
				.findAssignedCRsByProject(project);
		return trainAssigner(progressMonitor, assignedCRs);
	}

	private List<ChangeRequest> trainAssigner(IProgressMonitor progressMonitor,
			List<ChangeRequest> assignedCRs) throws LearningException {
		if (CollectionUtils.isEmpty(assignedCRs)) {
			String msg = "There is no assigned CR to be used in the training.";
			throw new LearningException(msg);
		}
		return SVMWekaCRAssigner.getInstance().train(assignedCRs,
				progressMonitor);
	}

}
