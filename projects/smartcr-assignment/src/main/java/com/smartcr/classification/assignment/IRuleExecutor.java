package com.smartcr.classification.assignment;

import java.util.List;

import com.smartcr.classification.context.Context;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;

public interface IRuleExecutor {

	/**
	 * Applies the rules and returns a boolean indicating if it has been
	 * assigned.
	 * 
	 * @param cr
	 * @param context
	 * @return
	 */
	public boolean fireSimpleRules(ChangeRequest cr, Context context);

	/**
	 * Applies the rules and returns a boolean indicating if it has been
	 * assigned.
	 * 
	 * @param cr
	 * @param context
	 * @return
	 */
	public boolean fireComplexRules(ChangeRequest cr, List<Author> possibleAuthors, Context context);

	public void fireSimpleRules(List<ChangeRequest> cr, Context context);

	public void fireComplexRules(List<ChangeRequest> cr, Context context);

}
