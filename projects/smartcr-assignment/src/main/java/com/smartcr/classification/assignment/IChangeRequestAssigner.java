package com.smartcr.classification.assignment;

import com.smartcr.core.model.ChangeRequest;

public interface IChangeRequestAssigner {

	/**
	 * Tries to assign the CR to a developer. Returns a boolean indicating if
	 * the CR has been assigned and, if it already had a developer assigned, the
	 * boolean also indicates if the developer changed.
	 * 
	 * @param cr
	 * @return
	 */
	public boolean assignCR(ChangeRequest cr);

}
