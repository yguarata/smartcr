package com.smartcr.classification.assignment;

import java.util.List;

import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.IProgressMonitor;

public interface ILearning {

	/**
	 * Train the classifier with the given list of change requests.
	 * 
	 * @param trainingSet
	 *            The change requests to train the classifier.
	 * @return The change requests that could not be used in the training.
	 * @throws LearningException
	 */
	List<ChangeRequest> train(List<ChangeRequest> trainingSet,
			IProgressMonitor progressMonitor) throws LearningException;

}
