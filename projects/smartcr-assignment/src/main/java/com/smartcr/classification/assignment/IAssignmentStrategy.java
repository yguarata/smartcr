package com.smartcr.classification.assignment;

import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.classification.context.IWorkloadBalancing;
import com.smartcr.core.model.ChangeRequest;

public interface IAssignmentStrategy {

	public enum StrategyMode {
		SIMPLE_RULES_ONLY, COMPLETE, IR_ONLY, COMPLEX_RULES_ONLY;
	}

	/**
	 * Assign the CR using the StrategyMode. Returns a boolean indicating if the
	 * CR has been assigned and, if it already had a developer assigned, the
	 * boolean also indicates if the developer changed.
	 * 
	 * @param cr
	 * @param strategy
	 * @return
	 * @throws LearningException
	 */
	public boolean assignCR(ChangeRequest cr,
			IWorkloadBalancing workloadBalancing, StrategyMode strategy)
			throws LearningException;
}
