package com.smartcr.classification.assignment.impl;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections.CollectionUtils;
import org.drools.KnowledgeBase;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatelessKnowledgeSession;

import com.smartcr.classification.ClassificationFacade;
import com.smartcr.classification.assignment.IRuleExecutor;
import com.smartcr.classification.context.Context;
import com.smartcr.core.IndexFacade;
import com.smartcr.core.ModelFacade;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Rule;
import com.smartcr.core.model.persitence.RuleDAO;

public class DroolsRulesExecutor implements IRuleExecutor {

	Logger logger = Logger.getLogger(getClass().getName());

	private List<Rule> addedRules = new ArrayList<Rule>();

	private KnowledgeBase kbase;

	private StatelessKnowledgeSession ksession;

	private static final RuleDAO droolsRuleDAO = RuleDAO.getInstance();

	private static DroolsRulesExecutor instance;

	private static final KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

	private DroolsRulesExecutor() {

	}

	public static DroolsRulesExecutor getInstance() {
		if (instance == null) {
			instance = new DroolsRulesExecutor();
		}
		return instance;
	}

	@Override
	public boolean fireSimpleRules(ChangeRequest cr, Context context) {
		List<Rule> rules = droolsRuleDAO.findSimpleRules();
		Author oldAuthor = cr.getFixedBy();
		applyRules(cr, rules, null);
		return cr.getFixedBy() != null && !cr.getFixedBy().equals(oldAuthor);
	}

	@Override
	public boolean fireComplexRules(ChangeRequest cr,
			List<Author> possibleAuthors, Context context) {
		List<Rule> rules = droolsRuleDAO.findComplexRules();
		Author oldAuthor = cr.getFixedBy();
		applyRules(cr, rules, possibleAuthors);
		return cr.getFixedBy() != null && !cr.getFixedBy().equals(oldAuthor);
	}

	@Override
	public void fireSimpleRules(List<ChangeRequest> crs, Context context) {
		List<Rule> rules = droolsRuleDAO.findSimpleRules();
		applyRules(crs, rules);
	}

	@Override
	public void fireComplexRules(List<ChangeRequest> crs, Context context) {
		List<Rule> rules = droolsRuleDAO.findComplexRules();
		applyRules(crs, rules);
	}

	private Resource addResource(Rule rule) {
		String complement = "import com.smartcr.core.model.* ";
		complement += "import com.smartcr.classification.context.* ";
		complement += "import com.smartcr.core.util.StringUtil ";
		complement += "global com.smartcr.classification.ClassificationFacade assignmentService ";
		complement += "global com.smartcr.core.ModelFacade modelService ";
		complement += "global com.smartcr.core.IndexFacade indexService ";
		complement += "global java.util.List recommendedDevelopers ";
		Resource myResource = ResourceFactory
				.newReaderResource((Reader) new StringReader(complement
						+ rule.getRuleContent()));
		return myResource;
	}

	private void applyRules(ChangeRequest cr, List<Rule> rules,
			List<Author> possibleAuthors) {
		boolean addedNewRules = false;
		
		if (CollectionUtils.isEmpty(rules)) {
			return;
		}

		for (Rule rule : rules) {
			if (!addedRules.contains(rule)) {
				kbuilder.add(addResource(rule), ResourceType.DRL);
				addedRules.add(rule);
				addedNewRules = true;
			}
		}

		if (kbuilder.hasErrors()) {
			logger.log(Level.SEVERE,
					"There were erros when adding rules to Drools. See the errors below:");
			System.out.println(kbuilder.getErrors());
			return;
		}

		if (kbase == null || addedNewRules) {
			kbase = kbuilder.newKnowledgeBase();
			kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
			ksession = kbase.newStatelessKnowledgeSession();
		}
		
		Map<String, Object> others = new HashMap<String, Object>();
		others.put("recommendedDevelopers", possibleAuthors);
		setGlobalsValues(others);

		ksession.execute(cr);

	}

	private void applyRules(List<ChangeRequest> crs, List<Rule> rules) {
		for (ChangeRequest cr : crs) {
			applyRules(cr, rules, null);
		}
	}

	private void setGlobalsValues(Map<String, Object> others) {
		ksession.setGlobal("modelService", ModelFacade.getInstance());
		ksession.setGlobal("assignmentService", ClassificationFacade.getInstance());
		ksession.setGlobal("indexService", IndexFacade.getInstance());
		if (others != null) {
			for (Entry<String, Object> entry : others.entrySet()) {
				ksession.setGlobal(entry.getKey(), entry.getValue());
			}
		}
	}
}
