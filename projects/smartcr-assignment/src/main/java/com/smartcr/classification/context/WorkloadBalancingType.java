package com.smartcr.classification.context;

public enum WorkloadBalancingType {
	
	RANDOM(new RandomWorkloadBalancing()),
	MANTIS(new MantisWorkloadBalancing());
	
	private IWorkloadBalancing balancing;
	
	private WorkloadBalancingType(IWorkloadBalancing balancing) {
		this.balancing = balancing;
	}

	public IWorkloadBalancing getBalancing() {
		return balancing;
	}
	
}
