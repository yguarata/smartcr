package com.smartcr.classification.assignment.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.smartcr.classification.assignment.IAssignmentStrategy;
import com.smartcr.classification.context.IWorkloadBalancing;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;

/**
 * É necessário repensar esses métodos. Sugere-se separá-los utilizando o padrão
 * Strategy. E os métodos que fazem a chamada ao Drools não deveriam de forma
 * alguma serem chamados dentro das regras, o que causaria um loop infinito.
 * 
 * @author yguarata
 * 
 */
public class AssignmentStrateyImpl implements IAssignmentStrategy {

	Logger logger = Logger.getLogger(getClass().getName());

	private static AssignmentStrateyImpl instance;

	private AssignmentStrateyImpl() {
	}

	public static AssignmentStrateyImpl getInstance() {
		if (instance == null) {
			instance = new AssignmentStrateyImpl();
		}
		return instance;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param workloadBalancing
	 */
	@Override
	public boolean assignCR(ChangeRequest cr,
			IWorkloadBalancing workloadBalancing, StrategyMode strategy)
			throws LearningException {
		if (!SVMWekaCRAssigner.getInstance().isTrained()) {
			String msg = "The classifier is not trained. Train it first!";
			logger.log(Level.SEVERE, msg);
			throw new LearningException(msg);
		}

		boolean assigned = false;

		logger.info("Assigning CR " + cr.getId() + "...");

		if (strategy.equals(StrategyMode.IR_ONLY)) {
			assigned = SVMWekaCRAssigner.getInstance().assignCR(cr);
		} else {
			// Tries with simple rules
			if (strategy.equals(StrategyMode.SIMPLE_RULES_ONLY)
					|| strategy.equals(StrategyMode.COMPLETE)) {
				assigned = DroolsRulesExecutor.getInstance().fireSimpleRules(
						cr, null);
			}

			// Tries again using complex rules
			if ((strategy.equals(StrategyMode.COMPLEX_RULES_ONLY) || 
					strategy.equals(StrategyMode.COMPLETE)) && !assigned) {
				// TODO
//				Set<Author> authors = SVMWekaCRAssigner.getInstance()
//						.recommendDevelopers(cr).keySet();
				Set<Author> authors = new HashSet<Author>();
				assigned = DroolsRulesExecutor.getInstance().fireComplexRules(
						cr, new ArrayList<Author>(authors), null);
			}
		}

		if (assigned) {
			logger.info("Assigned CR " + cr.getId() + " to "
					+ cr.getFixedBy().getEmail());
		} else {
			logger.log(Level.WARNING, "Could not assign the CR " + cr.getId());
		}

		return assigned;
	}

}
