/**
 * DISCLAIMER
 * 
 * The quality of the code is such that you should not copy any of it as best
 * practice how to build Vaadin applications.
 * 
 * @author jouni@vaadin.com
 * 
 */

package com.smartcr.webapp.cr;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

public class CRView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;
	CRMainTable crMainTable;
	Object editableId = null;
	private TabSheet tabs;

	@Override
	public void enter(ViewChangeEvent event) {
		setSizeFull();
		addStyleName("transactions");
		tabs = new TabSheet();
		tabs.setSizeFull();
		tabs.addStyleName("borderless");
		addComponent(tabs);
		tabs.addComponent(new CRListTab());
		// tabs.addComponent(new CRStatisticsTab());
		// tabs.addComponent(new TimeStatisticsTab());
		// tabs.addComponent(new DevelopersStatisticasTab());
		tabs.addComponent(new CRSettingsTab());
		setExpandRatio(tabs, 1);
	}

}
