package com.smartcr.webapp.util;

import java.util.SortedMap;
import java.util.Map.Entry;

import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.Cursor;
import com.vaadin.addon.charts.model.DataSeries;
import com.vaadin.addon.charts.model.DataSeriesItem;
import com.vaadin.addon.charts.model.Labels;
import com.vaadin.addon.charts.model.ListSeries;
import com.vaadin.addon.charts.model.PlotOptionsBar;
import com.vaadin.addon.charts.model.PlotOptionsPie;
import com.vaadin.addon.charts.model.Tooltip;
import com.vaadin.addon.charts.model.XAxis;
import com.vaadin.addon.charts.model.YAxis;
import com.vaadin.addon.charts.model.style.SolidColor;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;

public class Charts {

	public static Component buildSingleCategoryBarChart(String title,
			SortedMap<String, Long> count, String category) {
		if (count.isEmpty()) {
			Label label = new Label("No data was found for this chart.");
			label.setCaption(title);
			return label;
		}

		Chart chart = new Chart(ChartType.BAR);
		chart.setSizeFull();
		chart.setCaption(title);
		Configuration conf = chart.getConfiguration();
		conf.setTitle("");
		String[] categories = count.keySet().toArray(new String[] {});

		XAxis x = new XAxis();
		x.setCategories(categories);
		conf.addxAxis(x);

		YAxis y = new YAxis();
		y.setMin(0);
		y.setTitle(title);
		conf.addyAxis(y);

		Tooltip tooltip = new Tooltip();
		tooltip.setFormatter("this.series.name +': '+ this.y");
		conf.setTooltip(tooltip);

		PlotOptionsBar plot = new PlotOptionsBar();
		plot.setDataLabels(new Labels(true));
		conf.setPlotOptions(plot);

		ListSeries series = new ListSeries(category);

		for (String cat : categories) {
			series.addData(count.get(cat));
			// conf.addSeries(new ListSeries(cat, count.get(cat)));
		}

		conf.addSeries(series);

		chart.drawChart(conf);
		return chart;
	}

	public static Component buidPieChart(String title, SortedMap<String, Integer> count) {
		if (count.isEmpty()) {
			Label label = new Label("No data was found for this chart.");
			label.setCaption(title);
			return label;
		}

		Chart pieChart = new Chart(ChartType.PIE);
		pieChart.setCaption(title);
		pieChart.getConfiguration().setTitle("");
		pieChart.getConfiguration().getChart().setType(ChartType.PIE);
		pieChart.setWidth("100%");
		pieChart.setHeight("90%");

		PlotOptionsPie plotOptions = new PlotOptionsPie();
		plotOptions.setCursor(Cursor.POINTER);
		Labels dataLabels = new Labels();
		dataLabels.setEnabled(true);
		dataLabels.setColor(new SolidColor(0, 0, 0));
		dataLabels.setConnectorColor(new SolidColor(0, 0, 0));
		dataLabels
				.setFormatter("this.point.name +': '+ Math.round(this.percentage*10)/10.0 +'%'");
		plotOptions.setDataLabels(dataLabels);
		plotOptions.setSize("60%");
		pieChart.getConfiguration().setPlotOptions(plotOptions);

		DataSeries series = new DataSeries();

		for (Entry<String, Integer> c : count.entrySet()) {
			series.add(new DataSeriesItem(c.getKey(), c.getValue()));
		}

		pieChart.getConfiguration().setSeries(series);

		return pieChart;
	}
	
}
