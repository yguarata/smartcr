package com.smartcr.webapp.cr;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.logging.Logger;

import org.vaadin.addons.lazyquerycontainer.EntityContainer;

import com.smartcr.classification.ClassificationFacade;
import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.core.ModelFacade;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.persitence.ChangeRequestDAO;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.shared.MouseEventDetails.MouseButton;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class CRMainTable extends Table {

	Logger logger = Logger.getLogger(CRMainTable.class.getName());

	private static final long serialVersionUID = -7772689802694839106L;

	private EntityContainer<ChangeRequest> entityContainer;

	public CRMainTable() {
		this.setSizeFull();
		this.addStyleName("borderless");
		this.setSelectable(true);
		this.setColumnCollapsingAllowed(true);
		this.setColumnReorderingAllowed(true);
		// data.removeAllContainerFilters();
		entityContainer = getCRContainer();
		this.setContainerDataSource(entityContainer);
		this.setVisibleColumns(CRTableColumns.getPropertyIDsArray());

		for (CRTableColumns column : CRTableColumns.values()) {
			this.setColumnHeader((Object) column.getPropertyId(),
					column.getText());
			this.setColumnExpandRatio(column.getPropertyId(), column.getRatio());
		}

		// this.setFooterVisible(true);
		// this.setColumnFooter("Time", "Total");
		// Allow dragging items to the reports menu
		this.setDragMode(TableDragMode.MULTIROW);
		this.setMultiSelect(true);

		// EDIT MODE disabled for now
		// t.setTableFieldFactory(new DefaultFieldFactory() {
		// @Override
		// public Field createField(Container container, Object itemId,
		// Object propertyId, Component uiContext) {
		// boolean editable = itemId.equals(editableId);
		// Field f = new TextField();
		// f.setCaption(null);
		// f.setWidth("100%");
		// f.setReadOnly(!editable);
		// return f;
		// }
		// });

		// Double click to edit
		addItemClickListener(new ItemClickListener() {
			@Override
			public void itemClick(ItemClickEvent event) {
				if (event.getButton() == MouseButton.LEFT
						&& event.isDoubleClick()) {
					ChangeRequest cr = entityContainer.getEntity(event
							.getItemId());
					cr = ModelFacade.getInstance().getCR(cr.getId());
					CRFormWindow crFormWindow = new CRFormWindow(cr);
					UI.getCurrent().addWindow(crFormWindow);
					crFormWindow.addCloseListener(new CloseListener() {
						private static final long serialVersionUID = -7478283198530389729L;
						@Override
						public void windowClose(CloseEvent e) {
							getEntityContainer().refresh();
						}
					});
				}
			}
		});

		this.addActionHandler(new Handler() {
			private static final long serialVersionUID = -2053583542457239880L;
			private Action autoAssignmentAction = new Action("Auto assign");
			private Action manualAssignmentAction = new Action(
					"Suggest developers");

			@Override
			public void handleAction(Action action, Object sender, Object target) {
				Item item = ((Table) sender).getItem(target);
				String propertyId = CRTableColumns.ID.getPropertyId();
				Property<Long> property = item.getItemProperty(propertyId);
				Long id = property.getValue();
				ChangeRequest cr = ModelFacade.getInstance().getCR(id);

				if (action == autoAssignmentAction) {
					try {
						// TODO Change to use the application configuration set
						// by the user.
						ClassificationFacade.getInstance().assignCR_(cr);
						ModelFacade.getInstance().updateCR(cr);
						getEntityContainer().commit();
					} catch (LearningException e) {
						Notification.show(e.getMessage(), Type.WARNING_MESSAGE);
						e.printStackTrace();
					}
				} else if (action == manualAssignmentAction) {
					// Show suggestions
				}
			}

			@Override
			public Action[] getActions(Object target, Object sender) {
				return new Action[] { autoAssignmentAction,
						manualAssignmentAction };
			}
		});

		this.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 4224829723655702225L;

			@Override
			public void valueChange(
					com.vaadin.data.Property.ValueChangeEvent event) {
				if (getValue() instanceof Set) {
				} else {
				}
			}
		});

		this.setImmediate(true);

		// group rows by month
		// t.setRowGenerator(new RowGenerator() {
		// @Override
		// public GeneratedRow generateRow(Table table, Object itemId) {
		// if (itemId.toString().startsWith("month")) {
		// Date date = (Date) table.getItem(itemId)
		// .getItemProperty("timestamp").getValue();
		// SimpleDateFormat df = new SimpleDateFormat();
		// df.applyPattern("MMMM yyyy");
		// GeneratedRow row = new GeneratedRow(df.format(date));
		// row.setSpanColumns(true);
		// return row;
		// }
		// return null;
		// }
		// });

		// t.addGeneratedColumn(TableColumns.TITLE, new ColumnGenerator() {
		// @Override
		// public Object generateCell(Table source, Object itemId,
		// Object columnId) {
		// final String title = source.getItem(itemId)
		// .getItemProperty(columnId).getValue().toString();
		// Button titleLink = new Button(title);
		// titleLink.addStyleName("link");
		// titleLink.addClickListener(new ClickListener() {
		// @Override
		// public void buttonClick(ClickEvent event) {
		// Window w = new MovieDetailsWindow(DataProvider
		// .getMovieForTitle(title), null);
		// UI.getCurrent().addWindow(w);
		// }
		// });
		// return title;
		// }
		// });
		this.sort(new Object[] { CRTableColumns.CREATED.getPropertyId() },
				new boolean[] { false });
	}

	@Override
	protected String formatPropertyValue(Object rowId, Object colId,
			Property<?> property) {
		if (colId.equals(CRTableColumns.CREATED.getPropertyId())
				|| colId.equals(CRTableColumns.UPDATED.getPropertyId())) {
			SimpleDateFormat df = new SimpleDateFormat();
			df.applyPattern("MM/dd/yyyy HH:mm");
			return df.format(((Date) property.getValue()).getTime());
		} else if (colId.equals(CRTableColumns.CREATED_BY.getPropertyId())
				|| colId.equals(CRTableColumns.FIXED_BY.getPropertyId())) {
			if (property.getValue() != null
					&& property.getValue() instanceof Author) {
				return ((Author) property.getValue()).getEmail();
			}
			return "";
		} else if (colId.equals(CRTableColumns.ID.getPropertyId())) {
			return property.getValue().toString();
		}
		return super.formatPropertyValue(rowId, colId, property);
	}

	private EntityContainer<ChangeRequest> getCRContainer() {
		EntityContainer<ChangeRequest> entityContainer = new EntityContainer<ChangeRequest>(
				ChangeRequestDAO.getInstance().getEntityManager(),
				ChangeRequest.class, null, 1000, true, true, false);
		for (CRTableColumns c : CRTableColumns.values()) {
			entityContainer.addContainerProperty(c.getPropertyId(),
					c.getType(), c.getDefaultValue(), true, true);
		}

		return entityContainer;
	}

	public void resetContainer() {
		setContainerDataSource(entityContainer);
	}

	public EntityContainer<ChangeRequest> getEntityContainer() {
		return entityContainer;
	}

}
