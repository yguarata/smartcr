package com.smartcr.webapp.cr;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;

import com.smartcr.core.IndexFacade;
import com.smartcr.core.ModelFacade;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.webapp.DashboardUI;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class CRListTab extends CssLayout {

	private static final IndexFacade indexFacade = IndexFacade.getInstance();
	private static final ModelFacade modelFacade = ModelFacade.getInstance();
	private static final long serialVersionUID = 8360137681302996284L;
	private CRMainTable crMainTable;
	private IndexedContainer searchContainer;

	public CRListTab() {
		setSizeFull();
		setCaption("List");

		HorizontalLayout toolbar = buildToolbar();
		addComponent(toolbar);

		crMainTable = new CRMainTable();
		crMainTable.setHeight("92%");

		addComponent(crMainTable);

		searchContainer = new IndexedContainer();
		for (CRTableColumns col : CRTableColumns.values()) {
			searchContainer.addContainerProperty(col.getPropertyId(),
					col.getType(), col.getDefaultValue());
		}
	}

	private HorizontalLayout buildToolbar() {
		HorizontalLayout toolbar = new HorizontalLayout();
		toolbar.setWidth("100%");
		toolbar.setSpacing(true);
		toolbar.setMargin(true);
		toolbar.addStyleName("toolbar");

		// Label title = new Label("Change requests");
		// title.addStyleName("h2");
		// title.setSizeUndefined();
		// toolbar.addComponent(title);
		// toolbar.setComponentAlignment(title, Alignment.MIDDLE_LEFT);

		final TextField filter = createSimpleFilter();
		toolbar.addComponent(filter);
		toolbar.setExpandRatio(filter, 1);
		toolbar.setComponentAlignment(filter, Alignment.MIDDLE_LEFT);

		final Button advancedSearch = new Button("Advanced search");
		advancedSearch.setStyleName("link");
		toolbar.addComponent(advancedSearch);
//		toolbar.setExpandRatio(advancedSearch, 1);
		toolbar.setComponentAlignment(advancedSearch, Alignment.MIDDLE_LEFT);
		
		Button newCRButton = new Button("New change request");
		newCRButton.addStyleName("small");
		newCRButton.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;
			@Override
			public void buttonClick(ClickEvent event) {
				CRFormWindow crFormWindow = new CRFormWindow(new ChangeRequest());
				UI.getCurrent().addWindow(crFormWindow);
				crFormWindow.addCloseListener(new CloseListener() {
					private static final long serialVersionUID = -7478283198530389729L;
					@Override
					public void windowClose(CloseEvent e) {
						crMainTable.getEntityContainer().refresh();
					}
				});
			}
		});

		toolbar.addComponent(newCRButton);
		toolbar.setComponentAlignment(newCRButton, Alignment.MIDDLE_LEFT);

		Button refresh = new Button("Refresh");
		refresh.addClickListener(new ClickListener() {
			private static final long serialVersionUID = -3688196954359942767L;

			@Override
			public void buttonClick(ClickEvent event) {
				crMainTable.getEntityContainer().refresh();
			}
		});
		refresh.addStyleName("small");
		toolbar.addComponent(refresh);
		toolbar.setComponentAlignment(refresh, Alignment.MIDDLE_LEFT);

		final Button newReport = new Button("Create Report From Selection");
		newReport.addClickListener(new ClickListener() {
			private static final long serialVersionUID = -1800729959876957507L;

			@Override
			public void buttonClick(ClickEvent event) {
				createNewReportFromSelection();
			}
		});
		newReport.setEnabled(false);
		newReport.addStyleName("small");
		toolbar.addComponent(newReport);
		toolbar.setComponentAlignment(newReport, Alignment.MIDDLE_LEFT);

		return toolbar;
	}

	void createNewReportFromSelection() {
		((DashboardUI) getUI()).openReports(crMainTable);
	}

	private TextField createSimpleFilter() {
		final TextField filter = new TextField();
		filter.setWidth("40%");
		filter.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 6318241444383652401L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				try {
					String query = (String) event.getProperty().getValue();
					if (StringUtils.isEmpty(query)) {
						crMainTable.resetContainer();
						return;
					}
					List<Document> docs = indexFacade.searchCRs(query);
					searchContainer.removeAllItems();
					for (Document d : docs) {
						ChangeRequest cr = modelFacade.getCR(Long.valueOf(d
								.get("id")));
						Item item = searchContainer.addItem(cr.getId());
						item.getItemProperty(
								CRTableColumns.TITLE.getPropertyId()).setValue(
								cr.getTitle());
						item.getItemProperty(CRTableColumns.ID.getPropertyId())
								.setValue(cr.getId());
						item.getItemProperty(
								CRTableColumns.CREATED.getPropertyId())
								.setValue(cr.getDateCreated());
						item.getItemProperty(
								CRTableColumns.CREATED_BY.getPropertyId())
								.setValue(cr.getCreatedBy());
						item.getItemProperty(
								CRTableColumns.FIXED_BY.getPropertyId())
								.setValue(cr.getFixedBy());
						item.getItemProperty(
								CRTableColumns.PROJECT.getPropertyId())
								.setValue(cr.getProject());
						item.getItemProperty(
								CRTableColumns.RESOLUTION.getPropertyId())
								.setValue(cr.getResolution());
						item.getItemProperty(
								CRTableColumns.STATUS.getPropertyId())
								.setValue(cr.getStatus());
						item.getItemProperty(
								CRTableColumns.UPDATED.getPropertyId())
								.setValue(cr.getDateUpdated());
					}
					crMainTable.setContainerDataSource(searchContainer);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		});

		filter.setInputPrompt("Search");
		
		filter.addBlurListener(new BlurListener() {
			private static final long serialVersionUID = 2380075146752288979L;
			@Override
			public void blur(BlurEvent event) {
				if (StringUtils.isEmpty(filter.getValue())) {
					crMainTable.resetContainer();
					return;
				}				
			}
		});
		
		filter.addShortcutListener(new ShortcutListener("Clear",
				KeyCode.ESCAPE, null) {
			private static final long serialVersionUID = 8988755570347462296L;

			@Override
			public void handleAction(Object sender, Object target) {
				filter.setValue("");
				crMainTable.setContainerDataSource(crMainTable
						.getEntityContainer());
			}
		});
		return filter;
	}

}
