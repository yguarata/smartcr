/**
 * DISCLAIMER
 * 
 * The quality of the code is such that you should not copy any of it as best
 * practice how to build Vaadin applications.
 * 
 * @author jouni@vaadin.com
 * 
 */

package com.smartcr.webapp.analytics;

import java.util.HashMap;
import java.util.Map;

import org.reflections.Reflections;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class AnalyticsView extends VerticalLayout implements View {

	private static final long serialVersionUID = -1988596437586731022L;
	private VerticalLayout analyticsVerticalLayout = new VerticalLayout();
	private Panel analyticsPanel = new Panel();
	private Map<String, IAnalytics> analyticsMap = new HashMap<String, IAnalytics>();

	@Override
	public void enter(ViewChangeEvent event) {
		addAnalyticsComponents();

		setSizeFull();
		addStyleName("timeline");

		HorizontalLayout toolbar = new HorizontalLayout();
		toolbar.setWidth("100%");
		toolbar.setSpacing(true);
		toolbar.setMargin(true);
		toolbar.addStyleName("toolbar");
		addComponent(toolbar);

		final ComboBox analyticsSelect = new ComboBox();
		analyticsSelect.setWidth("300px");
		analyticsSelect.setInputPrompt("Select analytics");
		for (String key : analyticsMap.keySet()) {
			analyticsSelect.addItem(key);
		}
		toolbar.addComponent(analyticsSelect);
		toolbar.setComponentAlignment(analyticsSelect, Alignment.MIDDLE_LEFT);
		analyticsSelect.addShortcutListener(new ShortcutListener("Select",
				KeyCode.ENTER, null) {

			@Override
			public void handleAction(Object sender, Object target) {
				String labelAnalytics = (String) analyticsSelect.getValue();
				selectAnalytics(analyticsMap.get(labelAnalytics));
			}
		});

		Button add = new Button("Select");
		add.addStyleName("default");
		add.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				String labelAnalytics = (String) analyticsSelect.getValue();
				selectAnalytics(analyticsMap.get(labelAnalytics));
			}
		});
		toolbar.addComponent(add);
		toolbar.setExpandRatio(add, 1);
		toolbar.setComponentAlignment(add, Alignment.MIDDLE_LEFT);

		analyticsPanel.setSizeFull();
		analyticsPanel.setContent(analyticsVerticalLayout);
		addComponent(analyticsPanel);
		setExpandRatio(analyticsPanel, 2);
	}

	private void addAnalyticsComponents() {
		Reflections reflections = new Reflections("com.smartcr.classification.test.core.test.core.webapp");
		for (Class<? extends IAnalytics> analytics : reflections
				.getSubTypesOf(IAnalytics.class)) {
			try {
				IAnalytics instance = analytics.newInstance();
				analyticsMap.put(instance.getLabel(), instance);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	private void selectAnalytics(IAnalytics analytics) {
		analyticsVerticalLayout.removeAllComponents();
		analyticsVerticalLayout.addComponent((Component) analytics);
	}
}
