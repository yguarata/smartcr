package com.smartcr.webapp.analytics;

import java.util.concurrent.TimeUnit;

import com.smartcr.core.ModelFacade;
import com.smartcr.webapp.util.Charts;
import com.smartcr.webapp.util.ToolbarWidgetsLayout;
import com.vaadin.ui.HorizontalLayout;

public class TimeStatisticsTab extends ToolbarWidgetsLayout implements
		IAnalytics {

	private static final ModelFacade modelFacade = ModelFacade.getInstance();
	private static final long serialVersionUID = -973178772855945451L;

	public TimeStatisticsTab() {
		HorizontalLayout row01 = newRow();
		row01.addComponent(createPanel(Charts.buildSingleCategoryBarChart(
				"Average days to solve a CR by resolution",
				modelFacade.getAverageTimeByResolution(TimeUnit.DAYS), "Days")));
	}

	@Override
	public String getLabel() {
		return "Change Request Time Statistics";
	}

}
