package com.smartcr.webapp.cr;

import java.util.Locale;

import com.smartcr.core.model.Author;
import com.vaadin.data.util.converter.Converter;

public class AuthorToStringConverter implements Converter<String, Author> {

	private static final long serialVersionUID = 1399533366818673354L;

	@Override
	public Author convertToModel(String value,
			Class<? extends Author> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		Author author = new Author();
		author.setEmail(value);
		return author;
	}

	@Override
	public String convertToPresentation(Author value,
			Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value != null) {
			return value.getEmail();
		} else {
			return "";
		}
	}

	@Override
	public Class<Author> getModelType() {
		return Author.class;
	}

	@Override
	public Class<String> getPresentationType() {
		return String.class;
	}

}