package com.smartcr.webapp.cr;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.smartcr.classification.ClassificationFacade;
import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.core.ModelFacade;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.model.persitence.AuthorDAO;
import com.smartcr.core.model.stateMachine.DefaultStateMachineImpl;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class CRFormWindow extends GenericWindow {

	Logger logger = Logger.getLogger(getClass().getName());
	private static final long serialVersionUID = 4279017414937571151L;
	private ChangeRequest cr;
	private TextField titleTextField = new TextField("Title");
	private TextArea descriptionTextArea = new TextArea("Description");
	private TextArea stepsTextArea = new TextArea("Steps to reproduce");
	private ComboBox comboStatus = new ComboBox("Status");
	private ComboBox comboResolution = new ComboBox("Resolution");
	private ComboBox comboSeverity = new ComboBox("Severidade");
	private ComboBox comboAssignTo = new ComboBox("Assign to");
	private ComboBox comboReportedBy = new ComboBox("Reported by");
	private TabSheet tabs;

	public CRFormWindow(ChangeRequest cr) {
		super("New Change Request");

		if (cr != null && cr.isPersisted()) {
			setCaption("Editing Change Request " + cr.getId());
		}

		this.cr = cr;

		this.tabs = new TabSheet();
		tabs.setSizeUndefined();
		setContent(tabs);
		getContent().setSizeUndefined();

		tabs.addComponent(buildBasicInfoTab());
		if (cr != null && cr.isPersisted()) {
			tabs.addComponent(buildCommentsTab());
			tabs.addComponent(buildModificationHistory());
		}
	}

	private Component buildModificationHistory() {
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setCaption("Modification History");
		return verticalLayout;
	}

	private Component buildCommentsTab() {
		final VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setSpacing(true);
		verticalLayout.setMargin(true);
		Panel panel = new Panel();
		panel.setWidth("1000px");
		panel.setHeight("550px");
		panel.setContent(verticalLayout);

		if (cr != null && cr.getComments() != null) {
			panel.setCaption("Comments (" + cr.getComments().size() + ")");
			for (Comment comment : cr.getComments()) {
				String label = comment.getAuthor().getName() + " wrote on "
						+ comment.getDateCreate() + ":";
				Label authorLabel = new Label(label);
				authorLabel.addStyleName("comment-header");
				verticalLayout.addComponent(authorLabel);
				Label commentLabel = new Label(comment.getComment());
				verticalLayout.addComponent(commentLabel);
			}
		} else {
			panel.setCaption("Comments (0)");
		}
		return panel;
	}

	private Component buildBasicInfoTab() {
		VerticalLayout mainVerticalLayout = new VerticalLayout();
		mainVerticalLayout.setCaption("Basic Information");

		HorizontalLayout horizontalFormLayouts = new HorizontalLayout();
		mainVerticalLayout.addComponent(horizontalFormLayouts);

		/* Left form fields */
		FormLayout leftFormLayout = new FormLayout();
		leftFormLayout.setSpacing(true);
		leftFormLayout.setMargin(true);

		horizontalFormLayouts.addComponent(leftFormLayout);

		titleTextField.setNullRepresentation("");
		titleTextField.setColumns(40);
		titleTextField.setValue(cr.getTitle());
		leftFormLayout.addComponent(titleTextField);

		descriptionTextArea.setNullRepresentation("");
		descriptionTextArea.setColumns(40);
		descriptionTextArea.setRows(15);
		descriptionTextArea.setValue(cr.getDescription());
		leftFormLayout.addComponent(descriptionTextArea);

		stepsTextArea.setNullRepresentation("");
		stepsTextArea.setColumns(40);
		stepsTextArea.setRows(13);
		stepsTextArea.setValue(cr.getStepsToReproduce());
		leftFormLayout.addComponent(stepsTextArea);

		/* Right form fields */
		FormLayout rightFormLayout = new FormLayout();
		rightFormLayout.setMargin(true);
		rightFormLayout.setSpacing(true);
		horizontalFormLayouts.addComponent(rightFormLayout);

		if (cr.isPersisted()) {
			rightFormLayout.addComponent(comboReportedBy);
			comboReportedBy.addItem(cr.getCreatedBy());
			comboReportedBy.setValue(cr.getCreatedBy());
			comboReportedBy.setEnabled(false);
		}

		rightFormLayout.addComponent(comboAssignTo);
		for (Author author : AuthorDAO.getInstance().findAll()) {
			comboAssignTo.addItem(author);
			if (author.equals(cr.getFixedBy())) {
				comboAssignTo.setValue(author);
			}
		}

		Button buttonAssign = new Button("Auto assign");
		rightFormLayout.addComponent(buttonAssign);
		buttonAssign.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 6128944335405031591L;

			@Override
			public void buttonClick(ClickEvent event) {
				try {
					if (ClassificationFacade.getInstance().assignCR_(cr)) {
						comboAssignTo.setValue(cr.getFixedBy());
					}
				} catch (LearningException e) {
					e.printStackTrace();
				}
			}
		});

		rightFormLayout.addComponent(comboStatus);
		comboStatus.setTextInputAllowed(false);
		for (String status : new DefaultStateMachineImpl()
				.getPossibleStatusTargets(cr.getStatus())) {
			comboStatus.addItem(status);
			if (cr.isPersisted()
					&& cr.getStatus() != null
					&& status.toLowerCase()
							.equals(cr.getStatus().toLowerCase())) {
				comboStatus.setValue(status);
			}
		}

		comboStatus.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				String selectedStatus = (String) event.getProperty().getValue();
				fillComboResolution(selectedStatus);
			}
		});

		rightFormLayout.addComponent(comboResolution);
		comboResolution.setTextInputAllowed(false);
		fillComboResolution(cr.getStatus());

		rightFormLayout.addComponent(comboSeverity);
		comboSeverity.setTextInputAllowed(false);
		if (cr.isPersisted()) {
			comboSeverity.addItem(cr.getSeverity());
			comboSeverity.setValue(cr.getSeverity());
		}

		/* Action buttons */
		Button saveBtn = new Button("Save");
		saveBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 5991779187530018401L;

			@Override
			public void buttonClick(ClickEvent event) {
				save();
				close();
			}
		});

		Button cancelBtn = new Button("Cancel");
		cancelBtn.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 6128944335405031591L;

			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setSpacing(true);
		buttonsLayout.setMargin(true);
		buttonsLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		buttonsLayout.addComponent(saveBtn);
		buttonsLayout.addComponent(cancelBtn);

		mainVerticalLayout.addComponent(buttonsLayout);
		mainVerticalLayout.setComponentAlignment(buttonsLayout,
				Alignment.MIDDLE_CENTER);

		mainVerticalLayout.setExpandRatio(horizontalFormLayouts, 3);
		mainVerticalLayout.setExpandRatio(buttonsLayout, 1);

		return mainVerticalLayout;
	}

	private void fillComboResolution(String selectedStatus) {
		List<String> resolutions = new DefaultStateMachineImpl()
				.getAvailableResolutionsForStatus(selectedStatus);
		if (resolutions != null && resolutions.size() > 0) {
			comboResolution.setVisible(true);
			for (String resolution : resolutions) {
				comboResolution.addItem(resolution);
				if (cr != null
						&& cr.getResolution() != null
						&& resolution.toLowerCase().equals(
								cr.getResolution().toLowerCase())) {
					comboResolution.setValue(resolution);
				}
			}
		} else {
			comboResolution.removeAllItems();
			comboResolution.setVisible(false);
		}
	}

	private void save() {
		cr.setTitle(titleTextField.getValue());
		cr.setDescription(descriptionTextArea.getValue());
		cr.setStepsToReproduce(stepsTextArea.getValue());
		cr.setDateUpdated(new Date());
		cr.setStatus((String) comboStatus.getValue());
		cr.setSeverity((String) comboSeverity.getValue());

		if (!cr.isPersisted()) {
			// TODO cr.setCreatedBy(creator);
			cr.setDateCreated(new Date());
		}

		if (comboAssignTo.isModified()) {
			cr.setFixedBy(ModelFacade.getInstance().getAuthor(
					((Author) comboAssignTo.getValue()).getId()));
		}

		if (comboResolution.isVisible()) {
			cr.setResolution((String) comboResolution.getValue());
		} else {
			cr.setResolution(null);
		}

		if (cr.getId() != null) {
			ModelFacade.getInstance().updateCR(cr);
		} else {
			ModelFacade.getInstance().insertCR(cr);
		}
		
		Notification.show("Change Request " + cr.getId() + " saved.",
				Type.HUMANIZED_MESSAGE);
	}
}
