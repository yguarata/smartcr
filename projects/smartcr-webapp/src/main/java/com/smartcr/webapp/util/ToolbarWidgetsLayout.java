package com.smartcr.webapp.util;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

public abstract class ToolbarWidgetsLayout extends VerticalLayout {

	private static final long serialVersionUID = 6936452063209879616L;
	protected HorizontalLayout toolbar;

	public ToolbarWidgetsLayout() {
		buildInterface();
	}
	
	public ToolbarWidgetsLayout(String caption) {
		setCaption(caption);
		buildInterface();
	}

	public void buildInterface() {
		setSizeFull();
		toolbar = new HorizontalLayout();
		toolbar.setWidth("100%");
		toolbar.setSpacing(true);
		toolbar.addStyleName("toolbar");
		addComponent(toolbar);
	}

	protected CssLayout createPanel(Component content) {
		CssLayout panel = new CssLayout();
		panel.addStyleName("layout-panel");
		panel.setSizeFull();

//		Button configure = new Button();
//		configure.addStyleName("configure");
//		configure.addStyleName("icon-cog");
//		configure.addStyleName("icon-only");
//		configure.addStyleName("borderless");
//		configure.setDescription("Configure");
//		configure.addStyleName("small");
//		configure.addClickListener(new ClickListener() {
//			private static final long serialVersionUID = 8666279470555440071L;
//
//			@Override
//			public void buttonClick(ClickEvent event) {
//				Notification.show("Not implemented in this demo");
//			}
//		});
//		panel.addComponent(configure);

		panel.addComponent(content);
		return panel;
	}

	protected HorizontalLayout newRow() {
		HorizontalLayout row01 = new HorizontalLayout();
		row01.setSizeFull();
		row01.setMargin(new MarginInfo(true, true, false, true));
		row01.setSpacing(true);
		addComponent(row01);
		setExpandRatio(row01, 1.5f);
		return row01;
	}

}
