package com.smartcr.webapp.cr;

import java.util.List;
import java.util.logging.Logger;

import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.IProgressMonitor;
import com.smartcr.core.util.Task;
import com.vaadin.ui.Label;

public class WebProgressMonitorProxy implements IProgressMonitor {

	Logger logger = Logger.getLogger(getClass().getName());
	private Label label;

	public WebProgressMonitorProxy(final Label notificationLabel) {
		this.label = notificationLabel;
	}

	@Override
	public Task beginTask(String name, int totalWork) {
		Task task = DefaultProgressMonitor.getInstance().beginTask(name, totalWork);
		task.setMonitor(this);
		return task;
	}

	@Override
	public void subTask(String name) {
		DefaultProgressMonitor.getInstance().subTask(name);
	}

	@Override
	public List<Task> getTasks() {
		return DefaultProgressMonitor.getInstance().getTasks();
	}

	@Override
	public void removeTask(Task sysoutTask) {
		DefaultProgressMonitor.getInstance().removeTask(sysoutTask);
	}

	@Override
	public void update(Task task, String extraInfo) {
		logger.info("Calling " + getClass().getName());
		DefaultProgressMonitor.getInstance().update(task, extraInfo);
		double percent = ((double) task.getWorked() / (double) task.getTotalWork()) * 100.0;
		label.setValue(extraInfo + " [" + (int)percent + "%]");
	}

}
