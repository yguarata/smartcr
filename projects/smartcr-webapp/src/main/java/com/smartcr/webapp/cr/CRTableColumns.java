package com.smartcr.webapp.cr;

import java.util.Date;

import com.smartcr.core.model.Author;

public enum CRTableColumns {

	ID("id", "#", Long.class, "", 0),
	TITLE("title", "Title", String.class, "", 3), 
	CREATED_BY("createdBy", "Created by", Author.class, "", 1), 
	FIXED_BY("fixedBy", "Fixed by", Author.class, "", 1), 
	PROJECT("project", "Project", String.class, "", 1), 
	COMPONENT("origin", "Component", String.class, "", 1), 
	STATUS("status", "Status", String.class, "", 1),
	RESOLUTION("resolution", "Resolution", String.class, "", 1),
	CREATED("dateCreated", "Created on", Date.class, new Date(), 1), 
	UPDATED("dateUpdated", "Updated on", Date.class, new Date(), 1);

	private String propertyId;
	private String text;
	private Class<?> type;
	private Object defaultValue;
	private float ratio;

	private CRTableColumns(String property, String text,
			Class<?> type, Object defaultValue, float ratio) {
		this.propertyId = property;
		this.text = text;
		this.type = type;
		this.defaultValue = defaultValue;
		this.ratio = ratio;
	}

	public float getRatio() {
		return ratio;
	}

	public String getPropertyId() {
		return this.propertyId;
	}

	public static Object[] getPropertyIDsArray() {
		Object[] objects = new Object[values().length];
		for (int i = 0; i < values().length; i++) {
			objects[i] = values()[i].getPropertyId();
		}
		return objects;
	}

	@Override
	public String toString() {
		return getText();
	}

	public String getText() {
		return text;
	}

	public Class<?> getType() {
		return this.type;
	}

	public Object getDefaultValue() {
		return this.defaultValue;
	}

}