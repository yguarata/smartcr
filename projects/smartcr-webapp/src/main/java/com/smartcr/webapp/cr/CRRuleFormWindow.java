package com.smartcr.webapp.cr;

import java.util.Arrays;

import com.smartcr.core.ModelFacade;
import com.smartcr.core.model.Rule;
import com.smartcr.core.model.Rule.RuleType;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

public class CRRuleFormWindow extends GenericWindow {

	private static final long serialVersionUID = 5735308676729493091L;
	private ComboBox comboType = new ComboBox("Rule type",
			Arrays.asList(RuleType.values()));
	private TextField nameField = new TextField("Name");
	private TextArea ruleContent = new TextArea("Rules content");
	private Button buttonSave = new Button("Save");
	private Button buttonCancel = new Button("Cancel", new ClickListener() {
		private static final long serialVersionUID = 6749215039013177815L;
		@Override
		public void buttonClick(ClickEvent event) {
			close();
		}
	});
	private Rule rule = null;

	public CRRuleFormWindow(Rule rule) {
		super("Rule Editor");
		this.rule = rule;

		if (this.rule != null) {
			comboType.select(rule.getRuleType());
			nameField.setValue(rule.getName());
			ruleContent.setValue(rule.getRuleContent());
		}

		buildInterface();
	}

	private void buildInterface() {
		FormLayout l = new FormLayout();
		l.setSpacing(true);
		l.setMargin(true);

		setContent(l);

		comboType.setInputPrompt("Select");
		comboType.setNullSelectionAllowed(false);
		comboType.setTextInputAllowed(false);

		nameField.setNullRepresentation("");
		nameField.setRequired(true);
		comboType.setRequired(true);
		ruleContent.setNullRepresentation("");
		ruleContent.setRequired(true);
		ruleContent.setRows(20);
		ruleContent.setColumns(60);
		ruleContent.setSizeFull();

		buttonSave.addClickListener(new ClickListener() {
			private static final long serialVersionUID = -5237665801216474195L;
			@Override
			public void buttonClick(ClickEvent event) {
				if (rule == null) {
					rule = new Rule();
				}
				rule.setName(nameField.getValue());
				rule.setRuleContent(ruleContent.getValue());

				RuleType type = comboType.getValue().equals(
						RuleType.SIMPLE.name()) ? RuleType.SIMPLE
						: RuleType.COMPLEX;

				rule.setRuleType(type);
				ModelFacade.getInstance().insertOrUpdateRule(rule);
				close();
			}
		});

		HorizontalLayout hLayout = new HorizontalLayout();
		hLayout.setSpacing(true);

		hLayout.addComponent(nameField);
		hLayout.addComponent(comboType);

		ruleContent.setStyleName(nameField.getStyleName());

		l.addComponent(hLayout);
		l.addComponent(ruleContent);

		HorizontalLayout buttonsLayout = new HorizontalLayout();
		buttonsLayout.setSpacing(true);
		buttonsLayout.setDefaultComponentAlignment(Alignment.MIDDLE_RIGHT);
		buttonsLayout.addComponent(buttonCancel);
		buttonsLayout.addComponent(buttonSave);

		l.addComponent(buttonsLayout);
	}

}
