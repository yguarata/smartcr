package com.smartcr.webapp.cr;

import java.io.File;
import java.util.Date;
import java.util.List;

import com.smartcr.classification.ClassificationFacade;
import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.core.IndexFacade;
import com.smartcr.core.ModelFacade;
import com.smartcr.core.index.Fetcher;
import com.smartcr.core.index.FetcherImplNotFound;
import com.smartcr.core.model.Rule;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.IAskPassword;
import com.smartcr.fetch.FetchersLoader;
import com.smartcr.webapp.util.ToolbarWidgetsLayout;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class CRSettingsTab extends ToolbarWidgetsLayout {

	private static final ModelFacade modelFacade = ModelFacade.getInstance();

	private static final ClassificationFacade assignmentFacade = ClassificationFacade
			.getInstance();

	private static final IndexFacade indexFacade = IndexFacade.getInstance();

	private static final long serialVersionUID = 5357223984147227182L;

	private static Label notificationLabel = new Label();

	Thread thread;

	public CRSettingsTab() {
		super("Settings");
		addStyleName("dashboard-view");
		
		HorizontalLayout row01 = newRow();

		row01.addComponent(createPanel(buildClassifierBlock()));
		row01.addComponent(createPanel(buildRulesBlock()));

		HorizontalLayout row02 = newRow();

		row02.addComponent(createPanel(buildFetchersBlock()));
		row02.addComponent(createPanel(buildRulesBlock()));

	}

	private Component buildFetchersBlock() {
		VerticalLayout layout = new VerticalLayout();
		layout.setCaption("Fetchers");
		layout.setSizeFull();

		Table table = new Table();
		table.setSizeFull();
		table.addStyleName("borderless");

		table.addContainerProperty("Name", Label.class, null);
		table.addContainerProperty("Last run", Date.class, null);
		table.addContainerProperty("", Button.class, null);

		List<Fetcher> fetchers = FetchersLoader.getInstance(new File("fetchers.xml")).getFetchers(
				new WebProgressMonitorProxy(notificationLabel));

		for (final Fetcher f : fetchers) {
			indexFacade.addFetcher(f);
			Label label = new Label(f.getName());
			Button button = new Button("Run");
			button.addStyleName("link");
			table.addItem(new Object[] { label, new Date(), button }, null);
			button.addClickListener(runFetcherCallback(f));
		}

		layout.addComponent(table);
		layout.addComponent(notificationLabel);

		layout.setExpandRatio(table, 3);
		layout.setExpandRatio(notificationLabel, 1);

		return layout;
	}

	private ClickListener runFetcherCallback(final Fetcher f) {
		return new ClickListener() {
			private static final long serialVersionUID = -7961965329194531345L;
			Fetcher fetcher = f;

			@Override
			public void buttonClick(ClickEvent event) {
				new Thread() {
					public void run() {
						try {
							indexFacade.insertCRsByRepositoryName(
									fetcher.getName(), 5000l, true,
									new AskPassword());
						} catch (FetcherImplNotFound e) {
							e.printStackTrace();
						}
					}
				}.start();
			}
		};
	}

	class AskPassword implements IAskPassword {
		@Override
		public void verifyPassword(Fetcher fetcher) {
			// TODO Auto-generated method stub

		}
	}

	private Component buildClassifierBlock() {
		final GridLayout layout = new GridLayout(3, 5);
		layout.setSpacing(true);
		layout.setCaption("Auto assignment options");
		layout.setWidth("100%");
		layout.setHeight("50%");

		buildTrainClassifierButton(layout);

		CheckBox autoAssignCheckBox = new CheckBox(
				"Auto assign CR when it is created");
		layout.addComponent(autoAssignCheckBox, 0, 1);

		Label labelAssign = new Label("Assignment strategies");
		layout.addComponent(labelAssign, 0, 2);

		OptionGroup strategyAlternative = new OptionGroup();
		strategyAlternative.setMultiSelect(false);
		final String oneStepOption = "Use one-step (applies simple Drools rules)";
		final String twoStepOption = "Use two-step (applies simple and complex Drools rules)";
		final String irOption = "Use only Information Retrieval";
		strategyAlternative.addItem(oneStepOption);
		strategyAlternative.addItem(twoStepOption);
		strategyAlternative.addItem(irOption);
		layout.addComponent(strategyAlternative, 0, 3);

		final CheckBox workloadDist = new CheckBox(
				"Use workload distribution in assignments");
		workloadDist.setVisible(false);
		layout.addComponent(workloadDist, 0, 4);

		final ComboBox algorithmsCombo = new ComboBox();
		algorithmsCombo.setVisible(false);
		algorithmsCombo.setNullSelectionAllowed(false);
		algorithmsCombo.setTextInputAllowed(false);
		algorithmsCombo.setInputPrompt("Distribution method");
		algorithmsCombo.setStyleName("");
		algorithmsCombo.addItem("algorithm 01");
		algorithmsCombo.addItem("algorithm 02");
		algorithmsCombo.addItem("algorithm 03");
		algorithmsCombo.setFilteringMode(FilteringMode.OFF);
		algorithmsCombo.setImmediate(true);
		layout.addComponent(algorithmsCombo, 1, 4);

		workloadDist.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				algorithmsCombo.setVisible((Boolean) event.getProperty()
						.getValue());
			}
		});

		strategyAlternative.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				String value = (String) event.getProperty().getValue();
				workloadDist.setEnabled(value.equals(irOption));
				algorithmsCombo.setEnabled(value.equals(irOption));
				workloadDist.setVisible(value.equals(irOption));
			}
		});

		return layout;
	}

	private void buildTrainClassifierButton(GridLayout main) {
		String labelStatus = "";
		String buttonLabel = "Train";

		if (!assignmentFacade.isClassifierTrained()
				&& !assignmentFacade.isClassifierTraining()) {
			labelStatus = "The classifier has not been trained yet.";
		} else if (assignmentFacade.isClassifierTraining()) {
			labelStatus = "The classifier is being trained. Please wait...";
		} else {
			labelStatus = "The classifier is trained. :)";
			buttonLabel = "Re-train";
		}

		final Label label = new Label(labelStatus);
		main.addComponent(label, 0, 0);

		final ProgressBar indicator = new ProgressBar(new Float(0.0));
		indicator.setIndeterminate(true);
		if (assignmentFacade.isClassifierTraining()) {
			indicator.setVisible(true);
		} else {
			indicator.setVisible(false);
		}

		main.addComponent(indicator, 2, 0);
		// Set polling frequency to 0.5 seconds.
		UI.getCurrent().setPollInterval(500);

		// Add a button to start working
		final Button button = new Button(buttonLabel);
		button.setStyleName("link");
		main.addComponent(button, 1, 0);

		if (assignmentFacade.isClassifierTraining()) {
			button.setCaption("Force stop");
		}

		class WorkThread extends Thread {
			public void run() {
				try {
					label.setValue("The classifier is being trained. Please wait...");
					assignmentFacade.trainCRAssigner(DefaultProgressMonitor
							.getInstance());
					label.setValue("The classifier is trained. :)");
					button.setCaption("Re-Train");
					indicator.setVisible(false);
				} catch (LearningException e) {
					label.setValue("There was some problem when training. :(");
					e.printStackTrace();
				}
			}
		}

		button.addClickListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				if (assignmentFacade.isClassifierTraining()) {
					assignmentFacade.invalidateClassifierTraining();
					try {
						thread.stop();
					} catch (Exception e) {
					}
					button.setCaption("Train");
					indicator.setVisible(false);
				} else {
					indicator.setVisible(true);
					thread = new WorkThread();
					button.setCaption("Force stop");
					thread.start();
				}
			}
		});

	}

	private Component buildRulesBlock() {
		VerticalLayout layout = new VerticalLayout();
		layout.setCaption("Drools rules");
		layout.setSizeFull();

		final Table table = new Table();
		table.setSizeFull();
		table.addStyleName("borderless");
		table.addContainerProperty("Name", Label.class, null);
		table.addContainerProperty("Type", Label.class, null);
		table.addContainerProperty("", Button.class, new Button("Edit"));
		table.addContainerProperty(" ", Button.class, new Button("Remove"));

		fillRulesTable(table);

		Button buttonNew = new Button("New rule", new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				openCRRuleWindow(table, null);
			}
		});
		buttonNew.setStyleName("link");

		layout.addComponent(buttonNew);
		layout.addComponent(table);
		layout.setExpandRatio(table, 2);

		return layout;
	}

	private void fillRulesTable(final Table table) {
		table.removeAllItems();
		for (final Rule rule : modelFacade.findAllRules()) {
			Label labelName = new Label(rule.getName());
			Label labelType = new Label(rule.getRuleType().name());

			Button buttonRemove = new Button("Delete");
			buttonRemove.addStyleName("link");
			buttonRemove.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					modelFacade.removeRule(rule);
					fillRulesTable(table);
				}
			});

			Button buttonEdit = new Button("Edit");
			buttonEdit.addStyleName("link");
			buttonEdit.addClickListener(new ClickListener() {
				@Override
				public void buttonClick(ClickEvent event) {
					openCRRuleWindow(table, modelFacade.getRule(rule.getId()));
				}
			});

			table.addItem(new Object[] { labelName, labelType, buttonEdit,
					buttonRemove }, null);
		}
	}

	private void openCRRuleWindow(final Table table, final Rule rule) {
		CRRuleFormWindow ruleFormWindow = new CRRuleFormWindow(rule);
		UI.getCurrent().addWindow(ruleFormWindow);
		ruleFormWindow.addCloseListener(new CloseListener() {
			@Override
			public void windowClose(CloseEvent e) {
				fillRulesTable(table);
			}
		});
	}

}
