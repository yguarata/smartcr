/**
 * DISCLAIMER
 * 
 * The quality of the code is such that you should not copy any of it as best
 * practice how to build Vaadin applications.
 * 
 * @author jouni@vaadin.com
 * 
 */

package com.smartcr.webapp.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.smartcr.core.model.persitence.DAORegister;
import com.smartcr.core.model.persitence.DatabaseConfig;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.VaadinRequest;
import com.vaadin.util.CurrentInstance;

public class DataProvider {

	public static Random rand = new Random();

	/**
	 * Initialize the data for this application.
	 */
	public DataProvider() {
//		loadMoviesData();
		loadTheaterData();

		Properties props = new Properties();
		try {
			props.load(getClass().getClassLoader().getResourceAsStream(
					"config.properties"));
			DatabaseConfig dbConfig = new DatabaseConfig();
			dbConfig.setDatabaseAuto(props.getProperty("dbAuto"));
			dbConfig.setDatabaseDialect(props.getProperty("dbDialect"));
			dbConfig.setDatabaseDriver(props.getProperty("dbDriver"));
			dbConfig.setDatabasePassword(props.getProperty("dbPassword"));
			dbConfig.setDatabaseShowSQL(props.getProperty("dbShowSql"));
			dbConfig.setDatabaseUrl(props.getProperty("dbUrl"));
			dbConfig.setDatabaseUser(props.getProperty("dbUser"));
			DAORegister.getInstance().configureDatabase(dbConfig);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

	}

	/**
	 * =========================================================================
	 * Movies in theaters
	 * =========================================================================
	 */

	/** Simple Movie class */
	public static class Movie {
		public final String title;
		public final String synopsis;
		public final String thumbUrl;
		public final String posterUrl;
		/** In minutes */
		public final int duration;
		public Date releaseDate = null;

		public int score;
		public double sortScore = 0;

		Movie(String title, String synopsis, String thumbUrl, String posterUrl,
				JsonObject releaseDates, JsonObject critics) {
			this.title = title;
			this.synopsis = synopsis;
			this.thumbUrl = thumbUrl;
			this.posterUrl = posterUrl;
			this.duration = (int) ((1 + Math.round(Math.random())) * 60 + 45 + (Math
					.random() * 30));
			try {
				String datestr = releaseDates.get("theater").getAsString();
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				releaseDate = df.parse(datestr);
				score = critics.get("critics_score").getAsInt();
				sortScore = 0.6 / (0.01 + (System.currentTimeMillis() - releaseDate
						.getTime()) / (1000 * 60 * 60 * 24 * 5));
				sortScore += 10.0 / (101 - score);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		public String titleSlug() {
			return title.toLowerCase().replace(' ', '-').replace(":", "")
					.replace("'", "").replace(",", "").replace(".", "");
		}

		public void reCalculateSortScore(Calendar cal) {
			if (cal.before(releaseDate)) {
				sortScore = 0;
				return;
			}
			sortScore = 0.6 / (0.01 + (cal.getTimeInMillis() - releaseDate
					.getTime()) / (1000 * 60 * 60 * 24 * 5));
			sortScore += 10.0 / (101 - score);
		}
	}

	/*
	 * List of movies playing currently in theaters
	 */
	private static ArrayList<Movie> movies = new ArrayList<Movie>();

	/**
	 * Get a list of movies currently playing in theaters.
	 * 
	 * @return a list of Movie objects
	 */
	public static ArrayList<Movie> getMovies() {
		return movies;
	}

	/**
	 * Initialize the list of movies playing in theaters currently. Uses the
	 * Rotten Tomatoes API to get the list. The result is cached to a local file
	 * for 24h (daily limit of API calls is 10,000).
	 */
	private static void loadMoviesData() {

		File cache;

		// TODO why does this sometimes return null?
		VaadinRequest vaadinRequest = CurrentInstance.get(VaadinRequest.class);
		if (vaadinRequest == null) {
			// PANIC!!!
			cache = new File("movies.txt");
		} else {
			File baseDirectory = vaadinRequest.getService().getBaseDirectory();
			cache = new File(baseDirectory + "/movies.txt");
		}

		JsonObject json = null;
		try {
			// TODO check for internet connection also, and use the cache anyway
			// if no connection is available
			if (cache.exists()
					&& System.currentTimeMillis() < cache.lastModified() + 1000
							* 60 * 60 * 24) {
				json = readJsonFromFile(cache);
			} else {
				// Get an API key from http://developer.rottentomatoes.com
				String apiKey = "5kxcucm784ftwuh25egkky4r";
				json = readJsonFromUrl("http://api.rottentomatoes.com/api/public/v1.0/lists/movies/in_theaters.json?page_limit=30&apikey="
						+ apiKey);
				// Store in cache
				FileWriter fileWriter = new FileWriter(cache);
				fileWriter.write(json.toString());
				fileWriter.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (json == null) {
			return;
		}

		JsonArray moviesJson;
		movies.clear();
		moviesJson = json.getAsJsonArray("movies");
		for (int i = 0; i < moviesJson.size(); i++) {
			JsonObject movieJson = moviesJson.get(i).getAsJsonObject();
			JsonObject posters = movieJson.get("posters").getAsJsonObject();
			if (!posters.get("profile").getAsString()
					.contains("poster_default")) {
				Movie movie = new Movie(movieJson.get("title").getAsString(),
						movieJson.get("synopsis").getAsString(), posters.get(
								"profile").getAsString(), posters.get(
								"detailed").getAsString(), movieJson.get(
								"release_dates").getAsJsonObject(), movieJson
								.get("ratings").getAsJsonObject());
				movies.add(movie);
			}
		}
	}

	/* JSON utility method */
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	/* JSON utility method */
	private static JsonObject readJsonFromUrl(String url) throws IOException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,
					Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JsonElement jelement = new JsonParser().parse(jsonText);
			JsonObject jobject = jelement.getAsJsonObject();
			return jobject;
		} finally {
			is.close();
		}
	}

	/* JSON utility method */
	private static JsonObject readJsonFromFile(File path) throws IOException {
		BufferedReader rd = new BufferedReader(new FileReader(path));
		String jsonText = readAll(rd);
		JsonElement jelement = new JsonParser().parse(jsonText);
		JsonObject jobject = jelement.getAsJsonObject();
		return jobject;
	}

	/**
	 * =========================================================================
	 * Countries, cities, theaters and rooms
	 * =========================================================================
	 */

	/* List of countries and cities for them */
	static HashMap<String, ArrayList<String>> countryToCities = new HashMap<String, ArrayList<String>>();

	static List<String> theaters = new ArrayList<String>() {
		private static final long serialVersionUID = 1L;
		{
			add("Threater 1");
			add("Threater 2");
			add("Threater 3");
			add("Threater 4");
			add("Threater 5");
			add("Threater 6");
		}
	};

	static List<String> rooms = new ArrayList<String>() {
		private static final long serialVersionUID = 1L;
		{
			add("Room 1");
			add("Room 2");
			add("Room 3");
			add("Room 4");
			add("Room 5");
			add("Room 6");
		}
	};

	/**
	 * Parse the list of countries and cities
	 */
	private static HashMap<String, ArrayList<String>> loadTheaterData() {

		/* First, read the text file into a string */
		StringBuffer fileData = new StringBuffer(2000);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				DataProvider.class.getResourceAsStream("cities.txt")));

		char[] buf = new char[1024];
		int numRead = 0;
		try {
			while ((numRead = reader.read(buf)) != -1) {
				String readData = String.valueOf(buf, 0, numRead);
				fileData.append(readData);
				buf = new char[1024];
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String list = fileData.toString();

		/*
		 * The list has rows with tab delimited values. We want the second (city
		 * name) and last (country name) values, and build a Map from that.
		 */
		countryToCities = new HashMap<String, ArrayList<String>>();
		for (String line : list.split("\n")) {
			String[] tabs = line.split("\t");
			String city = tabs[1];
			String country = tabs[tabs.length - 2];

			if (!countryToCities.containsKey(country)) {
				countryToCities.put(country, new ArrayList<String>());
			}
			countryToCities.get(country).add(city);
		}

		return countryToCities;

	}

	/**
	 * =========================================================================
	 * Transactions data, used in tables and graphs
	 * =========================================================================
	 */

	private static double totalSum = 0;

	public static double getTotalSum() {
		return totalSum;
	}

	public IndexedContainer getRevenueForTitle(String title) {
		// System.out.println(title);
		IndexedContainer revenue = new IndexedContainer();
		revenue.addContainerProperty("timestamp", Date.class, new Date());
		revenue.addContainerProperty("revenue", Double.class, 0.0);
		revenue.addContainerProperty("date", String.class, "");
		revenue.sort(new Object[] { "timestamp" }, new boolean[] { true });
		return revenue;
	}

	public IndexedContainer getRevenueByTitle() {
		IndexedContainer revenue = new IndexedContainer();
		revenue.addContainerProperty("Title", String.class, "");
		revenue.addContainerProperty("Revenue", Double.class, 0.0);
		revenue.sort(new Object[] { "Revenue" }, new boolean[] { false });

		// TODO sometimes causes and IndexOutOfBoundsException
		if (revenue.getItemIds().size() > 10) {
			// Truncate to top 10 items
			List<Object> remove = new ArrayList<Object>();
			for (Object id : revenue
					.getItemIds(10, revenue.getItemIds().size())) {
				remove.add(id);
			}
			for (Object id : remove) {
				revenue.removeItem(id);
			}
		}

		return revenue;
	}

	public static Movie getMovieForTitle(String title) {
		for (Movie movie : movies) {
			if (movie.title.equals(title))
				return movie;
		}
		return null;
	}

}
