package com.smartcr.webapp.analytics;

import java.util.Comparator;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

import com.smartcr.core.ModelFacade;
import com.smartcr.core.model.Author;
import com.smartcr.webapp.util.Charts;
import com.smartcr.webapp.util.ToolbarWidgetsLayout;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;

public class DevelopersStatisticasTab extends ToolbarWidgetsLayout implements
		IAnalytics {

	private static final ModelFacade modelFacade = ModelFacade.getInstance();
	private static final long serialVersionUID = 2233368291047997900L;

	public DevelopersStatisticasTab() {
		ComboBox developersCombo = new ComboBox();
		developersCombo.setTextInputAllowed(true);
		developersCombo.setInputPrompt("Select a developer");
		for (Author dev : modelFacade.findAllAuthors()) {
			if (StringUtils.isNotEmpty(dev.getEmail())) {
				developersCombo.addItem(dev.getEmail());
			} else if (StringUtils.isNotEmpty(dev.getName())) {
				developersCombo.addItem(dev.getName());
			} else {
			}
		}

		developersCombo.addValueChangeListener(new ValueChangeListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				Notification.show("Feature not implemented yet!");
			}
		});

		toolbar.addComponent(developersCombo);

		HorizontalLayout row01 = newRow();

		row01.addComponent(createPanel(Charts.buildSingleCategoryBarChart(
				"Change requests created by developers",
				covertMap(modelFacade.getCRCreatedByCount()), "Change requests")));

		row01.addComponent(createPanel(Charts.buildSingleCategoryBarChart(
				"Change requests fixed by developers",
				covertMap(modelFacade.getCRFixedByCount()), "Change requests")));
	}

	private SortedMap<String, Long> covertMap(
			final SortedMap<String, Integer> map) {
		SortedMap<String, Long> mapConverted = new TreeMap<String, Long>(
				new Comparator<String>() {
					@Override
					public int compare(String o1, String o2) {
						return map.get(o2).compareTo(map.get(o1));
					}

				});

		for (Entry<String, Integer> entry : map.entrySet()) {
			if (entry.getValue() > 1) { // Excluded developer with only one CR
				mapConverted.put(entry.getKey(), entry.getValue().longValue());
			}
		}
		return mapConverted;
	}

	@Override
	public String getLabel() {
		return "Developers Statistics";
	}

}
