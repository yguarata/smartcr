package com.smartcr.webapp.analytics;


public interface IAnalytics {

	public String getLabel();
	
}
