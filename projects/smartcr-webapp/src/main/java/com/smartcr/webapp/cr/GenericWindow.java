package com.smartcr.webapp.cr;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.shared.ui.window.WindowMode;
import com.vaadin.ui.Window;

public class GenericWindow extends Window {
	
	private static final long serialVersionUID = -701895244895787356L;

	public GenericWindow(String caption) {
		setCaption(caption);
		center();
		setCloseShortcut(KeyCode.ESCAPE, null);
		setResizable(false);
		setClosable(true);
		setWindowMode(WindowMode.NORMAL);

		addStyleName("no-vertical-drag-hints");
		addStyleName("no-horizontal-drag-hints");
	}

}
