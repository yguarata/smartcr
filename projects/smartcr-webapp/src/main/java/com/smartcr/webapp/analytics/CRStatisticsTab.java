package com.smartcr.webapp.analytics;

import com.smartcr.core.ModelFacade;
import com.smartcr.webapp.util.Charts;
import com.smartcr.webapp.util.ToolbarWidgetsLayout;
import com.vaadin.ui.HorizontalLayout;

public class CRStatisticsTab extends ToolbarWidgetsLayout implements
		IAnalytics {

	private static final ModelFacade modelFacade = ModelFacade.getInstance();
	private static final long serialVersionUID = 5357223984147227182L;

	public CRStatisticsTab() {
		HorizontalLayout row01 = newRow();

		row01.addComponent(createPanel(Charts.buidPieChart(
				"Change requests by status", modelFacade.getCRStatusCount())));

		row01.addComponent(createPanel(Charts.buidPieChart(
				"Change requests by resolution",
				modelFacade.getCRResolutionCount())));

		HorizontalLayout row02 = newRow();

		row02.addComponent(createPanel(Charts.buidPieChart(
				"Change request by component",
				modelFacade.getCRComponentCount())));

		row02.addComponent(createPanel(Charts.buidPieChart(
				"Change request by projects", modelFacade.getCRProjectCount())));

	}

	@Override
	public String getLabel() {
		return "Change Request Statistics";
	}

}
