<?php

require_once( 'api.php' );

// class MantisPlugin {}

class AssignmentPlugin extends MantisPlugin {
	function register() {
		$this->wsdlURL = "http://localhost:8081/smartcr/service?wsdl";
		$this->wsdlOptions = array("trace" => 1, "exception" => 1, 'cache_wsdl' => WSDL_CACHE_NONE);

		$this->name = 'Auto Assignment Plugin <br/>(Click here to export current issues and train the classifier. It may take a while.)<br/>'; #Proper name of plugin
		$this->description = 'Auto assignment of issues'; #Short description of the plugin
		$this->page = 'export_bugs'; #Default plugin page

		$this->version = '0.1'; #Plugin version string
		$this->requires = array( #Plugin dependencies, array of basename => version pairs
				'MantisCore' => '=> 1.2.11',  #Should always depend on an appropriate version of MantisBT
		);

		$this->author = 'Yguaratã Cavalcanti'; #Author/team name
		$this->contact = 'yguarata@gmail.com'; #Author/team e-mail address
		$this->url = 'http://www.yguarata.com'; #Support webpage
	}


	function events() {
		return array(
				"EVENT_MENU_ISSUE" => EVENT_TYPE_EXECUTE,
				"EVENT_REPORT_BUG" => EVENT_TYPE_EXECUTE,
				"EVENT_UPDATE_BUG" => EVENT_TYPE_CHAIN,
				"EVENT_BUGNOTE_ADD" => EVENT_TYPE_EXECUTE,
				"EVENT_BUGNOTE_DELETED" => EVENT_TYPE_EXECUTE,
				"EVENT_BUGNOTE_EDIT" => EVENT_TYPE_EXECUTE,
		);
	}

	public function hooks() {
		return array(
				"EVENT_MENU_ISSUE" => "addAssignButton",
				"EVENT_REPORT_BUG" => "createChangeRequest",
				"EVENT_UPDATE_BUG" => "updateChangeRequest",
				"EVENT_BUGNOTE_ADD" => "addComment",
				"EVENT_BUGNOTE_DELETED" => "deleteComment",
				"EVENT_BUGNOTE_EDIT" => "editComment",
		);
	}

	public function addAssignButton($pEvent) {
		$client = new SoapClient($this->wsdlURL, $this->wsdlOptions);
		return array("<a href='" .
				plugin_page('assign.php') .
				"&id=" . gpc_get_int( 'id' ) .
				"' title='Tries to assign this issue to " .
				"some suitable developer.'>Auto assign</a>");
	}

	public function createChangeRequest($event, $bugData, $issueID) {
		$client = new SoapClient($this->wsdlURL, $this->wsdlOptions);
		$params = mapChangeRequest($bugData);
		// TODO: tratar datas
		$response = $client->createChangeRequest($params);
	}

	public function updateChangeRequest($event, $bugData, $issueID) {
		$client = new SoapClient($this->wsdlURL, $this->wsdlOptions);
		$params = mapChangeRequest($bugData);
		$response = null;
		$exists = $client->changeRequestExists(array('id' => $params->id))->return;
		// TODO: tratar datas
		if ($exists == 1) {
			$response = $client->updateChangeRequest($params);
		} else {
			$response = $client->createChangeRequest($params);
		}

		return $bugData;
	}

	public function addComment($event, $issueID, $commentID) {
		$client = new SoapClient($this->wsdlURL, $this->wsdlOptions);
		$noteParams = new stdClass();
		$noteParams->commentId = $commentID;
		$noteParams->crId = $issueID;
		$noteParams->comment = bugnote_get_text( $commentID );
		$authorID = bugnote_get_field( $commentID, 'reporter_id' );
		$noteParams->authorEmail = user_get_email($authorID);
		$client->addChangeRequestComment($noteParams);
	}
	
	public function editComment($event, $issueID, $commentID) {
		$client = new SoapClient($this->wsdlURL, $this->wsdlOptions);
		$noteParams = new stdClass();
		$noteParams->id = $commentID;
		$noteParams->comment = bugnote_get_text( $commentID );
		$client->editChangeRequestComment($noteParams);
	}
	
	public function deleteComment($event, $issueID, $commentID) {
		$client = new SoapClient($this->wsdlURL, $this->wsdlOptions);
		$noteParams = new stdClass();
		$noteParams->id = $commentID;
		$client->deleteChangeRequestComment($noteParams);
	}
}

/* CLI tests */
function tests() {
	$wsdlURL = "http://localhost:8081/smartcr/service?wsdl";
	$wsdlOptions = array("trace" => 1, "exception" => 1, 'cache_wsdl' => WSDL_CACHE_NONE);
	$client = new SoapClient($wsdlURL, $wsdlOptions);

	$params = new stdClass();
	$params->id = 5000;
	$params->title = 'Some title';

	$exists = $client->changeRequestExists(array('id' => $params->id))->return;

	if ($exists == 1) {
		$client->updateChangeRequest($params);
	} else {
		$client->createChangeRequest($params);
	}
}

//tests();

?>
