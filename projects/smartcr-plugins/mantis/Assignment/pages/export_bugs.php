<?php

function export_all_bugs() {
	$wsdlURL = "http://localhost:8081/smartcr/service?wsdl";
	$wsdlOptions = array("trace" => 1, "exception" => 1, 'cache_wsdl' => WSDL_CACHE_NONE);
	$client = new SoapClient($wsdlURL, $wsdlOptions);

	$t_query = "SELECT id FROM mantis_bug_table;";
	$t_result = db_query_bound( $t_query, array() );
	$ids = array();
	$i = 0;

	echo 'Exporting bugs to SmartCR...<br/>';

	while( $t_row = db_fetch_array( $t_result ) ) {
		$ids[i] = $t_row['id'];
		$params = mapChangeRequest(bug_get($ids[i], true));
		try {
			$response = $client->createChangeRequest($params);
			$notes = bugnote_get_all_bugnotes( $params->id );
				
			foreach ($notes as $note) {
				$noteParams = new stdClass();
				$noteParams->commentId = $note->id;
				$noteParams->crId = $params->id;
				$noteParams->comment = bugnote_get_text( $note->id );
				$authorID = bugnote_get_field( $note->id, 'reporter_id' );
				$noteParams->authorEmail = user_get_email($authorID);
				try {
					$client->addChangeRequestComment($noteParams);
				} catch (Exception $e) {
					echo 'Skiped comment ' . $note->id . ' from CR ' . $ids[i]; 
				}
			}
				
			echo 'Exported CR ' . $ids[i] . '<br/>';
		} catch (Exception $e) {
			echo 'Skiped CR ' . $ids[i] . '<br/>';
		}
		$i++;
	}
	return $ids;
}

export_all_bugs();

?>