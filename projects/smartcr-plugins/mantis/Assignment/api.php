<?php 
function mapChangeRequest($issue) {
	$params = new stdClass();
	$params->id = $issue->id;
	$params->description = $issue->description . ' ' . $issue->additional_information;
	$params->title = $issue->summary;
	$params->steps = $issue->steps_to_reproduce;
	$params->os = $issue->os;
	$params->origin = null;
	$params->platform = $issue->platform;
	$params->project = project_get_name($issue->project_id);
	$params->resolution = get_enum_element('resolution', $issue->resolution);
	$params->severity = get_enum_element('severity', $issue->severity);
	$params->priority = get_enum_element('priority', $issue->priority);
	$params->status = get_enum_element('status', $issue->status);
	$params->target = null;
	$params->version = $issue->version;
	$params->dtDue = $issue->date_due;
	$params->dtUpdated = $issue->last_updated;
	$params->dtCreated = $issue->last_created;
	$params->dtClosed = null;
	$params->dtFixed = null;
	$params->creatorEmail = user_get_email($issue->reporter_id);
	$params->fixerEmail = user_get_email($issue->handler_id);
	$params->closerEmail = null;
	return $params;
}
?>