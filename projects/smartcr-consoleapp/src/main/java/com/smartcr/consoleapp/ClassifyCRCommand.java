package com.smartcr.consoleapp;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;

@Parameters(commandDescription = "Perform automatic assignment of a CR.")
public class ClassifyCRCommand implements ICommand {

	@Parameter(description = "The of the CR to be assigned.", required = true)
	private List<String> ids;

	@Parameter(names = "--recommend", description = "Size of the recommendation list.")
	private int distributionSize = 10;

	@Override
	public void run() {
		for (String id : ids) {
			ChangeRequest cr = modelFacade.getCR(Long.valueOf(id));
			Author author = assignmentFacade.choseAuthorForCR(cr);
			String email = author.getEmail();
			System.out.println("The CR should be assigned to: " + email);
			System.out.println("List of recommendations for CR id=" + id + ":");
			try {
				printRecommendations(assignmentFacade.recommendAuthorsForCR(cr));
			} catch (LearningException e) {
				e.printStackTrace();
			}
		}
	}

	private void printRecommendations(Map<Author, Double> distributions) {
		ValueComparator bvc = new ValueComparator(distributions);
		TreeMap<Author, Double> sortedMap = new TreeMap<Author, Double>(bvc);
		sortedMap.putAll(distributions);
		int distSize = distributions.size();
		int size = (distributionSize > distSize) ? distSize : distributionSize;
		for (int i = 0; i < size; i++) {
			Author key = (Author) sortedMap.keySet().toArray()[i];
			System.out.println(String.format("    %-40s %s", key.getEmail()
					+ ":", distributions.get(key)));
		}
	}

	class ValueComparator implements Comparator<Author> {

		Map<Author, Double> base;

		public ValueComparator(Map<Author, Double> base) {
			this.base = base;
		}

		// Note: this comparator imposes orderings that are inconsistent with
		// equals.
		public int compare(Author a, Author b) {
			if (base.get(a) >= base.get(b)) {
				return -1;
			} else {
				return 1;
			} // returning 0 would merge keys
		}
	}

	@Override
	public void printHelp() {

	}

}
