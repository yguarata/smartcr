package com.smartcr.consoleapp;

import com.smartcr.classification.ClassificationFacade;
import com.smartcr.core.ModelFacade;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.IProgressMonitor;



public interface ICommand {
	static final ModelFacade modelFacade = ModelFacade.getInstance();
	static final ClassificationFacade assignmentFacade = ClassificationFacade.getInstance();
	static final IProgressMonitor progressMonitor = DefaultProgressMonitor.getInstance();
	void run();
	void printHelp();
}
