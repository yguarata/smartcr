package com.smartcr.consoleapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.History;
import com.smartcr.core.util.ConsolePrint;

@Parameters(commandDescription = "Print the details of a given CR.")
public class CRCommand implements ICommand {

	@Parameter(description = "The id of the CR to be printed.", required = true)
	private List<String> ids;
	
	@Parameter(names = "--complete", description = "Prints complete information.")
	private boolean complete = false;
	
	@Parameter(names = "--assignment-history", description = "Prints the assignment history of the CRs.")
	private boolean assignmentHistory = false;

	@Parameter(names = "--fixed-by-history")
	private boolean historyFixedBy = false;
	
	@Override
	public void run() {
		for (String id : ids) {
			ChangeRequest cr = modelFacade.getCR(Long.valueOf(id));
			if (cr != null) {
				if (assignmentHistory) {
					List<History> sortedHistory = new ArrayList<History>(cr.getHistory());
					Collections.sort(sortedHistory);
					for (History history: sortedHistory) {
						if (history.getField().toLowerCase().contains("atribuído")) {
							System.out.println(history.getDate() + ": " + history.getChange());
						}
					}
				} else if (historyFixedBy) {
					for (History h: cr.getHistory()) {
						if (h.getChange().contains("=> corrigido")) {
							System.out.println(h);
						}
					}
				}else {
					if (complete) {
						ConsolePrint.crComplete(cr);
					} else {
						ConsolePrint.crShort(cr);
					}
				}
			} else {
				System.out.println("The CR " + id + " dot not exist!");
			}
		}
	}

	@Override
	public void printHelp() {

	}

}
