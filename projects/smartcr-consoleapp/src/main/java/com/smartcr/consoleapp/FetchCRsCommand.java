package com.smartcr.consoleapp;

import java.io.IOException;

import jline.console.ConsoleReader;

import org.apache.commons.lang.StringUtils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.smartcr.core.IndexFacade;
import com.smartcr.core.index.Fetcher;
import com.smartcr.core.index.FetcherImplNotFound;
import com.smartcr.core.util.IAskPassword;

@Parameters(commandDescription = "Fetch CRs to store locally. If no repository name is specified, all will be fetched.")
public class FetchCRsCommand implements ICommand, IAskPassword {

	@Parameter(names = "--repository", description = "The name of the configured repository to be fetched.")
	private String repositoryName;

	@Parameter(names = "--limit", description = "The amount of CRs to be fetched.")
	private Long limit;

	@Parameter(names = "--continue", description = "Continue the fetching from the last fetched CR. The CRs are fetched in the ascending order of their IDs.")
	private boolean continueFromLast = false;

	@Override
	public void run() {
		try {
			System.out.println("Fetching CRs to store locally...");

			if (StringUtils.isEmpty(repositoryName)) {
				IndexFacade.getInstance().insertCRsFromAllFetchers(limit, continueFromLast,
						this);
			} else {
				IndexFacade.getInstance().insertCRsByRepositoryName(repositoryName, limit,
						continueFromLast, this);
			}
			System.out.println("Done.");
		} catch (FetcherImplNotFound e) {
			e.printStackTrace();
		}
	}

	public void verifyPassword(Fetcher fetcher) {
		ConsoleReader reader;
		try {
			reader = new ConsoleReader();
			if (StringUtils.isEmpty(fetcher.getRepositoryConfig().getUser())) {
				String user = reader.readLine(fetcher.getRepositoryConfig().getName()
									+ " username: ").replace("\n", "");
				fetcher.getRepositoryConfig().setUser(user);
			}
			if (StringUtils.isEmpty(fetcher.getRepositoryConfig().getPassword())) {
				String pwd = reader.readLine("password: ", '*').replace("\n", "");
				fetcher.getRepositoryConfig().setPassword(pwd);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void printHelp() {

	}

}
