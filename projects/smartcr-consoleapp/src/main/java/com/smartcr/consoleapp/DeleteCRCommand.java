package com.smartcr.consoleapp;

import java.io.IOException;
import java.util.List;

import jline.console.ConsoleReader;

import org.apache.commons.collections.CollectionUtils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.core.util.IProgressMonitor;
import com.smartcr.core.util.Task;

@Parameters(commandDescription = "Delete CRs.")
public class DeleteCRCommand implements ICommand {

	private static final IProgressMonitor monitor = DefaultProgressMonitor
			.getInstance();

	@Parameter(description = "The IDs of the CRs that will be deleted.")
	private List<String> ids;

	@Override
	public void run() {
		if (CollectionUtils.isNotEmpty(ids)) {
			String msg = "You must provide the CR IDs!";
			System.out.println(msg);
			return;
		}

		if (CollectionUtils.isNotEmpty(ids)) {
			deleteSpecifiedCRs();
		} else {
			try {
				deleteAllCRs();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void deleteAllCRs() throws IOException {
		ConsoleReader reader = new ConsoleReader();
		String msg = "This action will delete all CRs.";
		msg += ". Are you sure [yes/no]? ";
		String input = reader.readLine(msg).toLowerCase();

		if (input.equals("yes")) {
			List<ChangeRequest> crs;
			crs = modelFacade.findAllCRs();
			msg = "deletion of all " + crs.size() + " CRs";
			Task task = monitor.beginTask(msg, crs.size());
			for (ChangeRequest cr : crs) {
				modelFacade.deleteCR(cr);
				task.unitWorked("deleted CR " + cr.getId());
			}
			task.end();
		}
	}

	private void deleteSpecifiedCRs() {
		String msg = "deletion of " + ids.size() + " CRs";
		Task task = monitor.beginTask(msg, ids.size());
		for (String id : ids) {
			modelFacade.deleteCR(Long.valueOf(id));
			task.unitWorked("deleted CR " + id);
		}
		task.end();
	}

	@Override
	public void printHelp() {
	}
}
