package com.smartcr.consoleapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.History;

@Parameters(commandDescription = "List the developers.")
public class DevelopersCommand implements ICommand {

	@Parameter(names = "--count", description = "Print the amount of developers.")
	private boolean count = false;

	@Parameter(names = "--assigned", description = "Print statistics about developers.")
	private boolean assignedDevelopers = false;

	@Parameter(names = "--assigned-in-some-time", description = "List all the developers which have been assigned to CRs. Works only for Mantis.")
	private boolean assignedInSomeTime = false;

	@Parameter(names = "--fixedby", description = "List all the developers which have changed the status do fixed. Works only for Mantis.")
	private boolean fixedby = false;

	@Parameter(names = "--project")
	private List<String> projects;

	@Override
	public void run() {
		if (count) {
			System.out.println(modelFacade.countAuthors());
		} else if (assignedInSomeTime) {
			getAssignedDevsInSomeTime();
		} else if (assignedDevelopers) {
			getAssignedDevs();
		} else if (fixedby) {
			getFixedBy();
		} else {
			for (Author author : modelFacade.findAllAuthors()) {
				System.out.println(author.getName());
			}
		}
	}

	private void getFixedBy() {
		Set<Author> authors = new HashSet<Author>();
		List<ChangeRequest> crs = new ArrayList<ChangeRequest>();
		if (projects != null) {
			for (String s : projects) {
				crs.addAll(modelFacade.findCRsByProject(s));
			}
		} else {
			crs.addAll(modelFacade.findAllCRs());
		}

		for (ChangeRequest cr : crs) {
			for (History h : cr.getHistory()) {
				if (h.getChange().toLowerCase().contains("=> corrigido")) {
					authors.add(h.getAuthor());
				}
			}
		}

		if (count) {
			System.out.println(authors.size());
		} else {
			for (Author a : authors) {
				System.out.println(a);
			}
		}
	}

	private void getAssignedDevs() {
		HashMap<Long, Long> countDevs = new HashMap<Long, Long>();
		for (ChangeRequest cr : modelFacade.findAssignedCRs()) {
			Long authorId = cr.getFixedBy().getId();
			if (!countDevs.containsKey(authorId)) {
				countDevs.put(cr.getFixedBy().getId(), new Long(1));
			} else {
				Long count = countDevs.get(authorId);
				count++;
				countDevs.put(cr.getFixedBy().getId(), count);
			}
		}
		System.out.println("=== Developers with assigned CRs ===");
		for (Long id : countDevs.keySet()) {
			System.out.println(String.format("%s: %d",
					modelFacade.getAuthor(id), countDevs.get(id)));
		}
	}

	private void getAssignedDevsInSomeTime() {
		Set<Author> authors = new HashSet<Author>();
		for (History history : modelFacade.findAllHistory()) {
			if (history.getField().toLowerCase().contains("atribuído")) {
				String authorName = null;
				String change = history.getChange().trim();
				if (change.startsWith("=>")) {
					authorName = change.replace("=>", "").trim();
				} else if (change.indexOf("=>") > 0 && !change.endsWith("=>")) {
					authorName = change.split("=>")[1].trim();
				} else {
					// do nothing
				}
				Author author = modelFacade.getAuthorByName(authorName);
				if (authorName != null && authors.add(author)) {
					System.out.println(author);
				}
			}
		}
	}

	@Override
	public void printHelp() {
	}

}
