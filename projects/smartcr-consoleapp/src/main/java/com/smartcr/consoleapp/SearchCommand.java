package com.smartcr.consoleapp;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.smartcr.core.IndexFacade;
import com.smartcr.core.util.ConsolePrint;

@Parameters(commandDescription = "Search the indexed artifacts.", separators = "=")
public class SearchCommand implements ICommand {

	@Parameter(description = "query.", required = true)
	private List<String> query = null;

	@Override
	public void run() {
		try {
			System.out.println("Searching for '" + query + "'");
			List<Document> result = IndexFacade.getInstance().searchCRs(query.get(0));
			for (Document document : result) {
				ConsolePrint.crShort(modelFacade.getCR(Long.valueOf(document.get("id"))));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void printHelp() {
		System.out.println("search -q --query");
	}

	public static String getCommandName() {
		return "search";
	}

}
