package com.smartcr.consoleapp;

import com.beust.jcommander.Parameters;
import com.smartcr.core.IndexFacade;
import com.smartcr.core.index.Fetcher;

@Parameters(commandDescription = "List all configured repositories.")
public class RepositoryCommand implements ICommand {

	@Override
	public void run() {
		for (Fetcher s : IndexFacade.getInstance().getCRFetchers()) {
			String out = String.format("%s (type: %s)", s.getRepositoryConfig()
					.getName(), s.getRepositoryConfig().getType());
			System.out.println(out);
		}
	}

	@Override
	public void printHelp() {
		// TODO Auto-generated method stub

	}

}
