package com.smartcr.consoleapp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.History;
import com.smartcr.core.util.ConsolePrint;

@Parameters(commandDescription = "List the CRs.")
public class ListCRsCommand implements ICommand {

	@Parameter(names = "--limit", description = "The amount of registers to list.")
	private Long limit;

	@Parameter(names = "--unassigned", description = "List unassigned CRs. This filter exclude the others.")
	private boolean unassigned = false;

	@Parameter(names = "--assigned", description = "List assigned CRs. This filter exclude the others.")
	private boolean assigned = false;

	@Parameter(names = "--closed", description = "List closed CRs. This filter exclude the others.")
	private boolean closed = false;

	@Parameter(names = "--all", description = "List all CRs. This filter exclude the others.")
	private boolean all = false;

	@Parameter(names = "--show-projects", description = "List all the projects set in the CRs. This filter exclude the others.")
	private boolean showProjects = false;
	
	@Parameter(names = "--project", description = "List CRs of the given project(s).")
	private List<String> project;

	@Parameter(names = "--fixedby", description = "Filter the CRs fixed by author. Argument: email of part of it.")
	private String fixedby = null;

	@Parameter(names = "--closedby", description = "Filter the CRs closed by author. Argument: email of part of it.")
	private String closedby = null;

	@Parameter(names = "--createdby", description = "Filter the CRs created by author. Argument: email of part of it.")
	private String createdby = null;

	@Parameter(names = "--status", description = "Filter the CRs according to the status.")
	private String status;

	@Parameter(names = "--resolution", description = "Filter the CRs according to the resolution.")
	private String resolution;

	@Parameter(names = "--origin", description = "Filter the CRs according to the origin (life-cycle phase where the CR where identified: requirements, implementation, etc.).")
	private String origin;

	@Parameter(names = "--severity", description = "Filter the CRs according to the severity.")
	private String severity;

	@Parameter(names = "--count", description = "Print the amount of CRs.")
	private boolean count = false;

	@Parameter(names = "--show-fixedby-history")
	private boolean fixedByHistory = false;
	
	@Override
	public void run() {
		List<ChangeRequest> crs = new ArrayList<ChangeRequest>();

		if (unassigned) {
			crs.addAll(modelFacade.findUnassignedCRs());
		} else if (assigned) {
			crs.addAll(modelFacade.findAssignedCRs());
		} else if (closed) {
			crs.addAll(modelFacade.findClosedCRs());
		} else if (all) {
			crs.addAll(modelFacade.findAllCRs());
		} else if (showProjects) {
			for (String p : modelFacade.findAllProjects()) {
				System.out.println(p);
			}
		} else if (project != null) {
			for (String p: project) {
				ChangeRequest example = new ChangeRequest();
				example.setProject(p);
				crs.addAll(modelFacade.findCRByExample(example));
			}
		}else {
			ChangeRequest crExample = new ChangeRequest();
			crExample.setFixedBy(new Author(null, fixedby));
			crExample.setClosedBy(new Author(null, closedby));
			crExample.setCreatedBy(new Author(null, createdby));
			crExample.setStatus(status);
			crExample.setResolution(resolution);
			crExample.setOrigin(origin);
			crExample.setSeverity(severity);
			crs = modelFacade.findCRByExample(crExample);
		}

		if (CollectionUtils.isEmpty(crs)) {
			System.out.println("No CRs found.");
		} else {
			if (count) {
				System.out.println(crs.size());
			} else {
				if (limit != null) {
					crs = crs.subList(0, limit.intValue());
				}
				System.out.println(String.format("Listing %d CRs...", crs.size()));
				for (ChangeRequest cr : crs) {
					if (fixedByHistory) {
						ConsolePrint.crShort(cr);
						Set<Author> fixedBy = new HashSet<Author>();
						for (History h: cr.getHistory()) {
							if (h.getChange().contains("=> corrigido")) {
								fixedBy.add(h.getAuthor());
							}
						}
						System.out.println("    Fixed by " + fixedBy.size() + ": " + fixedBy);
					} else {
						ConsolePrint.crShort(cr);
					}
				}
			}
		}
	}

	@Override
	public void printHelp() {
	}

	public Long getLimit() {
		return limit;
	}

	public void setLimit(Long end) {
		this.limit = end;
	}

}
