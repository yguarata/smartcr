package com.smartcr.consoleapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jline.console.ConsoleReader;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.model.History;
import com.smartcr.core.util.StringUtil;

@Parameters(commandDescription = "Merge duplicate authors. Works only for SERPRO")
public class MergeAuthorsCommand implements ICommand {

	@Parameter(names = "--interative", description = "Runs in interative mode, asking confirmation for each merge.")
	private boolean interative = false;
	
	@Parameter(names = "--from", description = "ID of the author which will be merged into other.")
	private String from = null;
	
	@Parameter(names = "--to", description = "ID of the author which will receive data from other author.")
	private String to = null;

	@Override
	public void run() {
		try {
			if (from != null && to != null) {
				mergeAuthorsByIDs();
				return;
			} else {
				mergeAuthorsAutomatically();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void mergeAuthorsAutomatically() throws IOException {
		boolean hasMergedAuthors = false;
		Map<Author, Author> mergedAuthors = new HashMap<Author, Author>();
		List<Author> allAuthors = modelFacade.findAllAuthors();
		int count = 0;
		List<Author> authorsWithEmail = new ArrayList<Author>();
		List<Author> authorsWithoutEmail = new ArrayList<Author>();
		
		for (Author author : allAuthors) {
			if (author.getEmail() != null && !author.getEmail().isEmpty() && author.getEmail().contains("@")) {
				authorsWithEmail.add(author);
			} else {
				authorsWithoutEmail.add(author);
			}
		}

		int total = authorsWithoutEmail.size();
		
		for (Author withEmail : authorsWithEmail) {
			String emailFirstName = null, emailLastName = null;
			String names[] = withEmail.getEmail().split("@")[0].split("\\.");
			
			if (names[0].contains("-")) {
				emailFirstName = names[0].split("-")[0].toLowerCase();
			} else {
				emailFirstName = names[0].toLowerCase();
			}

			if (names.length > 1) {
				if (names[1].contains("-")) {
					emailLastName = names[1].split("-")[1].toLowerCase();
				} else {
					emailLastName = names[1].toLowerCase();
				}
			}

			for (Author withoutEmail : authorsWithoutEmail) {
				
				String completeName = StringUtil.removeAccents(withoutEmail.getName()).toLowerCase();
				
				List<String> noEmailNames = new ArrayList<String>(Arrays.asList(completeName.split(" ")));
				String firstName = noEmailNames.get(0);
				String lastName = noEmailNames.get(noEmailNames.size() - 1);
				
				if ((emailFirstName.equals(firstName) && emailLastName.equals(lastName)) ||
						(noEmailNames.contains(emailFirstName) && noEmailNames.contains(emailLastName))) {
					
					hasMergedAuthors = true;
					System.out.println("");
					System.out.println("=== The following authors are similars:");
					System.out.println(" - " + withEmail);
					System.out.println(" - " + withoutEmail);
					
					if (interative == true) {
						System.out.println("Do you want to merge them? (yes, no)");
						ConsoleReader consoleReader = new ConsoleReader();
						String input = consoleReader.readLine();
						if (input.toLowerCase().equals("yes") && !mergedAuthors.containsKey(withEmail)) {
							mergeAuthors(withEmail, withoutEmail);
						}
					} else if (!mergedAuthors.containsKey(withEmail)) {
						mergeAuthors(withEmail, withoutEmail);
					}
					mergedAuthors.put(withEmail, withoutEmail);
					System.out.println("Merged " + ++count + " of " + total);
				}
			}
		}
		if (!hasMergedAuthors) {
			System.out.println("No authors condidates to merge.");
		} else {
			System.out.println("Merged authors");
			for (Entry<Author, Author> merged: mergedAuthors.entrySet()) {
				System.out.println(merged.getValue() + " => " + merged.getKey());
			}
		}
	}

	private void mergeAuthorsByIDs() {
		Author from = modelFacade.getAuthor(Long.valueOf(this.from));
		Author to = modelFacade.getAuthor(Long.valueOf(this.to));
		mergeAuthors(to, from);
	}

	private void mergeAuthors(Author authorMergeInto, Author authorMergeFrom) {
		System.out.println("Merging " + authorMergeInto + " and " + authorMergeFrom);
		for (ChangeRequest cr : modelFacade.findAllCRs()) {
			boolean updated = false;
			if (cr.getFixedBy() != null && cr.getFixedBy().equals(authorMergeFrom)) {
				cr.setFixedBy(authorMergeInto);
				updated = true;
			}
			if (cr.getCreatedBy() != null && cr.getCreatedBy().equals(authorMergeFrom)) {
				cr.setCreatedBy(authorMergeInto);
				updated = true;
			}
			if (cr.getClosedBy() != null && cr.getClosedBy().equals(authorMergeFrom)) {
				cr.setClosedBy(authorMergeInto);
				updated = true;
			}

			if (updated) {
				modelFacade.updateCR(cr);
			}
		}

		for (Comment comment : modelFacade.findCommentsByAuthor(authorMergeFrom)) {
			comment.setAuthor(authorMergeInto);
			modelFacade.updateComment(comment);
		}

		for (History history : modelFacade.findHistoryByAuthor(authorMergeFrom)) {
			history.setAuthor(authorMergeInto);
			modelFacade.updateHistory(history);
		}

		// Set the complete name from the old author
		authorMergeInto.setName(authorMergeFrom.getName());
		modelFacade.updateAuthor(authorMergeInto);
		modelFacade.deleteAuthor(authorMergeFrom);
	}

	@Override
	public void printHelp() {

	}

}
