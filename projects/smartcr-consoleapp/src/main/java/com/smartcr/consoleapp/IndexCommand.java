package com.smartcr.consoleapp;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.smartcr.core.IndexFacade;

@Deprecated
@Parameters(commandDescription = "Manage the index of artifacts.")
public class IndexCommand implements ICommand {

	@Parameter(names = "--create", description = "Indicates that the index must be (re)created. If not provided, the index is updated.")
	private boolean create = false;

	@Override
	public void printHelp() {
	}

	@Override
	public void run() {
		if (create) {
			System.out.println("Indexing CRs...");
			IndexFacade.getInstance().createIndex();
		} else {
			System.out.println("Updating index of CRs...");
			IndexFacade.getInstance().updateIndex();
		}
		System.out.println("Done.");
	}

}
