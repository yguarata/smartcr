package com.smartcr.consoleapp;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(commandDescription = "Print help about commands.")
public class HelpCommand implements ICommand {

	@Parameter(description = "Command name.")
	private List<String> commandName;
	private JCommander jCommander;

	public HelpCommand(JCommander jCommander) {
		this.jCommander = jCommander;
	}

	@Override
	public void run() {
		if (CollectionUtils.isNotEmpty(commandName)) {
			jCommander.usage(commandName.get(0));
		} else {
			System.out.println("The following commands are available:");
			for (String cName : jCommander.getCommands().keySet()) {
				String fmt = String.format("%-15s %s", cName,
						jCommander.getCommandDescription(cName));
				System.out.println(fmt);
			}
		}
	}

	@Override
	public void printHelp() {

	}

}
