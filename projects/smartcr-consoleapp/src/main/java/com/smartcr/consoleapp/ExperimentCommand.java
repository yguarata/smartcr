package com.smartcr.consoleapp;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.LogManager;

import org.apache.commons.io.IOUtils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.google.common.collect.Lists;
import com.smartcr.classification.assignment.IAssignmentStrategy.StrategyMode;
import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.classification.context.RandomWorkloadBalancing;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.History;
import com.smartcr.core.model.Rule;
import com.smartcr.core.model.Rule.RuleType;

@Parameters(commandDescription = "Perform experiments. This command only applies to CRs from the SERPRO Mantis.")
public class ExperimentCommand implements ICommand {

	private static final int FOLDS = 10;

	private static final int RANDOMIZATIONS = 10;

	@Parameter(names = "--svm")
	private boolean svm = false;
	
	@Parameter(names = "--proposed-approach")
	private boolean proposedApproach = false;

	@Override
	public void run() {
		LogManager.getLogManager().reset();
		List<ChangeRequest> crs = new ArrayList<ChangeRequest>();
		crs.addAll(modelFacade.findCRsByProject("Programacao_Financeira_V2.1")); // Module A
//		crs.addAll(modelFacade.findCRsByProject("Documento_Habil_V2.1")); // Module B
		crs = normalizeFixedBy(crs);
		if (svm) {
			experimentSVM(crs);
		} else if (proposedApproach) {
			experimentProposedApproach(crs);
		} else {
			// Nothing to do
		}
	}

	private void experimentSVM(List<ChangeRequest> crs) {
		try {
			SortedMap<String, SortedMap<String,Total>> totals = new TreeMap<String, SortedMap<String,Total>>();
			// Performs a 10 fold cross validation, 10 times
			for (Integer execution = 0; execution < RANDOMIZATIONS; execution++) {
				totals.put(execution.toString(), new TreeMap<String, Total>());
				long seed = System.nanoTime();
				Collections.shuffle(crs, new Random(seed));
				// Performs a 10 fold cross validation
				for (Integer foldNumber = 0; foldNumber < FOLDS; foldNumber++) {
					List<List<ChangeRequest>> folds = new ArrayList<List<ChangeRequest>>(Lists.partition(crs, crs.size() / FOLDS));
					List<ChangeRequest> guessingSet = folds.remove(foldNumber.intValue());
					List<ChangeRequest> trainingSet = new ArrayList<ChangeRequest>(crs.size());
					Set<ChangeRequest> notCorrect = new HashSet<ChangeRequest>();
					for (List<ChangeRequest> list : folds) {
						trainingSet.addAll(list);
					}
					assignmentFacade.trainCRAssigner(trainingSet, progressMonitor);
					// Try to classify the CRs
					for (ChangeRequest cr : guessingSet) {
						Author guessedAuthor = assignmentFacade.choseAuthorForCR(cr);
						if (!getFixers(cr).contains(guessedAuthor)) {
							notCorrect.add(cr);
						}
					}
					
					Total total = new Total();
					total.totalInPercent = ((double) guessingSet.size() - notCorrect.size())/ ((double) guessingSet.size());
					totals.get(execution.toString()).put(foldNumber.toString(), total);
				}
			}
			printTotals(totals);
		} catch (LearningException e) {
			e.printStackTrace();
		}
	}

	public void experimentProposedApproach(List<ChangeRequest> crs) {
		try {
			// Clean rules
			for (Rule rule : modelFacade.findAllRules()) {
				modelFacade.deleteRule(rule);
			}
			
			// Insert simple rules
			String simpleRules = IOUtils.toString(this.getClass().getResourceAsStream("simpleRules.drl"));
			Rule simpleRule = new Rule();
			simpleRule.setName("All simple rules");
			simpleRule.setRuleContent(simpleRules);
			simpleRule.setRuleType(RuleType.SIMPLE);
			modelFacade.insertOrUpdateRule(simpleRule);
			
			//Insert complex rules
			String complexRules = IOUtils.toString(this.getClass().getResourceAsStream("complexRules.drl"));
			Rule complexRule = new Rule();
			complexRule.setName("All simple rules");
			complexRule.setRuleContent(complexRules);
			complexRule.setRuleType(RuleType.SIMPLE);
			modelFacade.insertOrUpdateRule(complexRule);
			
			// Performs a 10 fold cross validation, 10 times
			SortedMap<String, SortedMap<String,Total>> totals = new TreeMap<String, SortedMap<String,Total>>();
			for (Integer execution = 0; execution < RANDOMIZATIONS; execution++) {
				totals.put(execution.toString(), new TreeMap<String, Total>());
				long seed = System.nanoTime();
				Collections.shuffle(crs, new Random(seed));
				
				// Performs a 10 fold cross validation
				for (Integer foldNumber = 0; foldNumber < FOLDS; foldNumber++) {
					List<List<ChangeRequest>> folds = new ArrayList<List<ChangeRequest>>(Lists.partition(crs, crs.size() / FOLDS));
					List<ChangeRequest> guessingSet = folds.remove(foldNumber.intValue());
					List<ChangeRequest> trainingSet = new ArrayList<ChangeRequest>(crs.size());
					Set<ChangeRequest> notCorrect = new HashSet<ChangeRequest>();

					double totalWithSimpleRules = 0;
					double totalWithComplexRules = 0;
					double totalWithSVM = 0;
					
					for (List<ChangeRequest> list : folds) {
						trainingSet.addAll(list);
					}
					
					assignmentFacade.trainCRAssigner(trainingSet, progressMonitor);
					// Try to classify the CRs
					for (ChangeRequest cr : guessingSet) {
						boolean assignedWithSimpleRules = false;
						boolean assignedWithComplexRules = false;
						boolean assignedWithSVM = false;
						
						// Tries simple rules
						assignedWithSimpleRules = assignmentFacade.assignCR_(cr, 
								new RandomWorkloadBalancing(), StrategyMode.SIMPLE_RULES_ONLY);
						
						// Tries complex rules
						if (!assignedWithSimpleRules) {
							assignedWithComplexRules = assignmentFacade.assignCR_(cr, 
									new RandomWorkloadBalancing(), StrategyMode.COMPLEX_RULES_ONLY);
						}
						
						// Tries with SVM
						if (!assignedWithSimpleRules && !assignedWithComplexRules) { 
							// Set<Author> authors = assignmentFacade.recommendAuthorsForCR(cr).keySet();
							// assignmentFacade.assignCR(cr, authors, WorkloadBalancingType.MANTIS);
							cr.setFixedBy(assignmentFacade.choseAuthorForCR(cr));
							assignedWithSVM = true;
						}
						
						Author guessedAuthor = cr.getFixedBy();
						if (getFixers(cr).contains(guessedAuthor)) {
							if (assignedWithSimpleRules) {
								totalWithSimpleRules++;
							} else if (assignedWithComplexRules) {
								totalWithComplexRules++;
							} else if (assignedWithSVM) {
								totalWithSVM++;
							}
						} else {
							notCorrect.add(cr);
						}
					}
					
					Total total = new Total();
					total.totalInPercent = ((double) guessingSet.size() - notCorrect.size()) / ((double) guessingSet.size());
					total.totalWithComplexRules = (double)totalWithComplexRules / (double)(totalWithSimpleRules + totalWithComplexRules + totalWithSVM);
					total.totalWithSimpleRules = (double)totalWithSimpleRules / (double)(totalWithSimpleRules + totalWithComplexRules + totalWithSVM);
					total.totalWithSVM = (double) totalWithSVM / (double)(totalWithSimpleRules + totalWithComplexRules + totalWithSVM);
					totals.get(execution.toString()).put(foldNumber.toString(), total);
				}
			}
			
			printTotals(totals);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LearningException e) {
			e.printStackTrace();
		}
	}
	
	private void printTotals(SortedMap<String, SortedMap<String, Total>> totals) {
		// Print totals
		System.out.println("-*- Results for SVM -*-");
		String foldHeader = "        ";
		for (int i = 0; i < FOLDS; i++) {
			foldHeader += " & \\textbf{Fold " + i + "}";
		}
		foldHeader += " & \\textbf{Min} & \\textbf{Max} & \\textbf{Avg} \\\\";
		System.out.println(foldHeader);
		for (Entry<String, SortedMap<String, Total>> total : totals.entrySet()) {
			String exec = "\\textbf{Random " + total.getKey() + "}";
			String folds = "";
			BigDecimal min = null, max = null, avgTotal = new BigDecimal(0.0);
			for (Entry<String, Total> fold: total.getValue().entrySet()) {
				double totalInPercent = fold.getValue().totalInPercent;
				folds += " & " + new BigDecimal(totalInPercent).setScale(2, RoundingMode.HALF_UP).doubleValue();
				folds += String.format(" (%s, %s, %s)", fold.getValue().totalWithSimpleRules, 
						fold.getValue().totalWithComplexRules, fold.getValue().totalWithSVM);
				avgTotal = avgTotal.add(new BigDecimal(totalInPercent));
				if (min == null && max == null) {
					min = max = new BigDecimal(totalInPercent);
				} else {
					if (totalInPercent < min.doubleValue()) {
						min = new BigDecimal(totalInPercent);
					} else if (totalInPercent > max.doubleValue()) {
						max = new BigDecimal(totalInPercent);
					}
				}
			}
			BigDecimal avg = avgTotal.divide(new BigDecimal((double)total.getValue().size()));
			avg = avg.setScale(2, RoundingMode.HALF_UP);
			min = min.setScale(2, RoundingMode.HALF_UP);
			max = max.setScale(2, RoundingMode.HALF_UP);
			folds += " & " + min + " & " + max + " & " + avg + "\\\\";
			
			System.out.println(exec + folds);
		}
	}
	
	public List<ChangeRequest> normalizeFixedBy(List<ChangeRequest> crs) {
		Iterator<ChangeRequest> iterator = crs.iterator();
		while (iterator.hasNext()) {
			ChangeRequest cr = iterator.next();
			List<Author> fixers = getFixers(cr);
			if (fixers.size() >= 1) {
				System.out.println("Changing from " + cr.getFixedBy().getEmail() + 
						" to " + fixers.get(0).getEmail());
				cr.setFixedBy(fixers.get(0));
			} else {
				// The CR has never been fixed.
				// Thus, remove it from the dataset.
				iterator.remove();
			}
		}
		
		return crs;
	}

	private List<Author> getFixers(ChangeRequest cr) {
		List<Author> fixers = new ArrayList<Author>();
		for (History h : cr.getHistory()) {
			if (h.getChange().contains("=> corrigido")
					&& !fixers.contains(h.getAuthor())) {
				fixers.add(h.getAuthor());
			}
		}
		return fixers;
	}
	
	public List<Author> getAllAssignedDevelopers(ChangeRequest cr) {
		List<Author> authors = new ArrayList<Author>(cr.getHistory().size());
		authors.add(cr.getFixedBy());
		for (History history: cr.getHistory()) {
			if (history.getField().toLowerCase().contains("atribuído")) {
				String change = history.getChange().trim();
				String name = null;
				if (change.startsWith("=>")) {
					name = change.replace("=>", "");
				} else if (!change.endsWith("=>")) {
					name = change.split("=>")[1].trim();
				}
				if (name != null) {
					authors.add(modelFacade.getAuthorByName(name));
				}
			}
		}
		return authors;
	}
	
	@Override
	public void printHelp() {

	}
	
	class Total {
		public double totalInPercent = 0.0;
		public double totalWithSimpleRules = 0;
		public double totalWithComplexRules = 0;
		public double totalWithSVM = 0;
		
	}
	
	public static void main(String[] args) {
//		Console.configureIndex();// Init database
//		List<ChangeRequest> crs = new ArrayList<ChangeRequest>();
//		crs.addAll(modelFacade.findAssignedCRsByProject("Programacao_Financeira_V2.1"));
//		crs.addAll(modelFacade.findAssignedCRsByProject("Documento_Habil_V2.1"));
//		Map<String, Map<Author, Integer>> featureToAuthor = new HashMap<String, Map<Author, Integer>>();
		
		// count by feature
//		for (ChangeRequest cr: crs) {
//			String[] tt = new String[]{cr.getTitle(), cr.getDescription(), cr.getStepsToReproduce()};
//			for (String t: tt) {
//				if (t != null && t.contains("[") && t.contains("]")) {
//					int i = t.indexOf("[");
//					int j = t.indexOf("]");
//					String feature = t.substring(i, j + 1).toLowerCase()
//							.replace(" ", "");
//					feature = StringUtil.removeAccents(feature);
//					List<Author> fixers = new ExperimentCommand().getFixers(cr);
//
//					if (!featureToAuthor.containsKey(feature)) {
//						featureToAuthor.put(feature,
//								new HashMap<Author, Integer>());
//					}
//
//					for (Author fixer : fixers) {
//						if (!featureToAuthor.get(feature).containsKey(fixer)) {
//							featureToAuthor.get(feature).put(fixer, 1);
//						} else {
//							int newCount = featureToAuthor.get(feature).get(
//									fixer) + 1;
//							featureToAuthor.get(feature).put(fixer, newCount);
//						}
//					}
//				}
//			}
//		}
//		for (Entry<String, Map<Author, Integer>> entry: featureToAuthor.entrySet()) {
//			System.out.println(entry.getKey());
//			for (Entry<Author, Integer> author: entry.getValue().entrySet()) {
//				System.out.println("    " + author.getKey().getName() + ": " + author.getValue());
//			}
//		}
		
//		featureToAuthor.clear();
//		
//		// count by severity
//		for (ChangeRequest cr: crs) {
//			String feature = cr.getSeverity();
//			feature = StringUtil.removeAccents(feature);
//			
//			if (!featureToAuthor.containsKey(feature)) {
//				featureToAuthor.put(feature, new HashMap<Author, Integer>());
//			}
//			
//			if (!featureToAuthor.get(feature).containsKey(cr.getFixedBy())) {
//				featureToAuthor.get(feature).put(cr.getFixedBy(), 1);
//			} else {
//				int newCount = featureToAuthor.get(feature).get(cr.getFixedBy()) + 1;
//				featureToAuthor.get(feature).put(cr.getFixedBy(), newCount);
//			}
//		}
//		for (Entry<String, Map<Author, Integer>> entry: featureToAuthor.entrySet()) {
//			System.out.println(entry.getKey());
//			for (Entry<Author, Integer> author: entry.getValue().entrySet()) {
//				System.out.println("    " + author.getKey().getName() + ": " + author.getValue());
//			}
//		}
//		
//		new ExperimentCommand().normalizeFixedBy(crs);
		
	}
	
}
