package com.smartcr.consoleapp;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import jline.TerminalFactory;
import jline.console.ConsoleReader;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.smartcr.core.IndexFacade;
import com.smartcr.core.index.Fetcher;
import com.smartcr.core.model.persitence.DAORegister;
import com.smartcr.core.model.persitence.DatabaseConfig;
import com.smartcr.core.util.DefaultProgressMonitor;
import com.smartcr.fetch.FetchersLoader;
import com.smartcr.util.ConfigXML;

public class Console {

	@Parameter(names = "help", arity = 1)
	private String help = "main";

	private JCommander jCommander;
	private HashMap<String, ICommand> commandActions;
	
	public Console() {
		configureIndex(); // First of all, configure the index.
		jCommander = new JCommander(this);
		commandActions = new HashMap<String, ICommand>();
		addCommand("list-cr", new ListCRsCommand());
		addCommand("delete-cr", new DeleteCRCommand());
		addCommand("list-dev", new DevelopersCommand());
		addCommand("list-repo", new RepositoryCommand());
		addCommand("search-cr", new SearchCommand());
		addCommand("get-cr", new CRCommand());
		addCommand("index-cr", new IndexCommand());
		addCommand("train", new TrainCRClassifierCommand());
		addCommand("assign-cr", new ClassifyCRCommand());
		addCommand("fetch-cr", new FetchCRsCommand());
		addCommand("merge-authors", new MergeAuthorsCommand());
		addCommand("expt", new ExperimentCommand());
		addCommand("help", new HelpCommand(jCommander));
	}

	public static void configureIndex() {
		IndexFacade.getInstance().setIndexPath(
				ConfigXML.getInstance().getIndexPath());
		DatabaseConfig dbConfig = new DatabaseConfig();
		dbConfig.setDatabaseAuto(ConfigXML.getInstance().getDatabaseAuto());
		dbConfig.setDatabaseDialect(ConfigXML.getInstance()
				.getDatabaseDialect());
		dbConfig.setDatabaseDriver(ConfigXML.getInstance().getDatabaseDriver());
		dbConfig.setDatabasePassword(ConfigXML.getInstance()
				.getDatabasePassword());
		dbConfig.setDatabaseShowSQL(ConfigXML.getInstance()
				.getDatabaseShowSQL());
		dbConfig.setDatabaseUrl(ConfigXML.getInstance().getDatabaseUrl());
		dbConfig.setDatabaseUser(ConfigXML.getInstance().getDatabaseUser());
		DAORegister.getInstance().configureDatabase(dbConfig);
		for (Fetcher f : FetchersLoader.getInstance(new File("configuration.xml"))
				.getFetchers(DefaultProgressMonitor.getInstance())) {
			IndexFacade.getInstance().addFetcher(f);
		}
	}

	private void addCommand(String name, ICommand devCommand) {
		jCommander.addCommand(name, devCommand);
		commandActions.put(name, devCommand);
	}

	public void run(String[] args) throws IOException {
		try {
			jCommander.parse(args);
			String command = jCommander.getParsedCommand();
			if (command != null) {
				commandActions.get(command).run();
			}
		} catch (ParameterException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) throws Exception {
		ConsoleReader reader = new ConsoleReader();
		reader.setPrompt("prompt (exit, to exit)> ");
		String input = "";
		Console console = new Console();
		console.run(new String[] { "help" });
		while (true) {
			input = reader.readLine().toLowerCase();
			if (input.equals("exit")) {
				TerminalFactory.get().restore();
				System.exit(0);
			} else {
				input = input.replace("\n", "");
				List<String> opts = Arrays.asList(input.split(" "));
				String[] optsArray = new String[opts.size()];

				for (int i = 0; i < opts.size(); i++) {
					optsArray[i] = opts.get(i);
				}

				console.run(optsArray);
				console = new Console();
			}
		}
	}

	public JCommander getjCommander() {
		return jCommander;
	}

	public void setjCommander(JCommander jCommander) {
		this.jCommander = jCommander;
	}

	public boolean isHelp() {
		return help != null;
	}

}
