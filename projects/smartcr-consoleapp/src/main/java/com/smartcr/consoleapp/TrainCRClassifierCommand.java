package com.smartcr.consoleapp;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.smartcr.classification.ClassificationFacade;
import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.DefaultProgressMonitor;

@Parameters(commandDescription = "Train the CR assignment classifier.")
public class TrainCRClassifierCommand implements ICommand {

	@Parameter(names = "--project", description = "Restrict the training to the specified project.")
	private String project = null;

	/**
	 * Train the classifier with the assigned CRs.
	 */
	@Override
	public void run() {
		try {
			System.out.println("Training the classifier...");
			List<ChangeRequest> notUsedCRs;
			
			if (project != null) {
				notUsedCRs = ClassificationFacade.getInstance()
						.trainCRAssignerByProject(
								DefaultProgressMonitor.getInstance(), project);
			} else {
				notUsedCRs = ClassificationFacade.getInstance().trainCRAssigner(
						DefaultProgressMonitor.getInstance());
			}
			
			if (CollectionUtils.isNotEmpty(notUsedCRs)) {
				System.out.println("The following CRs could not be trained:");
				for (ChangeRequest changeRequest : notUsedCRs) {
					System.out.println("ID=" + changeRequest.getId());
				}
			}
			System.out.println("Done.");
		} catch (LearningException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void printHelp() {
	}

}
