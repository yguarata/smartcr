package com.smartcr.util;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ConfigXML {

	static Logger logger = Logger.getLogger(ConfigXML.class.getName());
	private static ConfigXML instance;
	private Document doc;
	private boolean testMode = false;

	private ConfigXML() {
		try {
			doc = parseXMLFile();
		} catch (Exception e) {
			System.out
					.println("It was not possible to read the configuration file.");
			e.printStackTrace();
		}
	}

	public static ConfigXML getInstance() {
		if (instance == null) {
			instance = new ConfigXML();
		}
		return instance;
	}

	public String getDatabaseUser() {
		if (isTestMode()) {
			return getDataBaseAttribute("userForTest");
		} else {
			return getDataBaseAttribute("user");
		}
	}

	public String getDatabasePassword() {
		if (isTestMode()) {
			return getDataBaseAttribute("passwordForTest");
		} else {
			return getDataBaseAttribute("password");
		}
	}

	public String getDatabaseAuto() {
		return getDataBaseAttribute("auto");
	}

	public String getDatabaseUrl() {
		String home = System.getProperty("user.home");
		if (isTestMode()) {
			return getDataBaseAttribute("urlForTest").replace("$home$", home);
		} else {
			return getDataBaseAttribute("url").replace("$home$", home);
		}
	}

	public String getDatabaseDriver() {
		return getDataBaseAttribute("driver-class");
	}

	public String getDatabaseDialect() {
		return getDataBaseAttribute("dialect");
	}

	public String getDatabaseShowSQL() {
		return getDataBaseAttribute("show-sql");
	}

	private String getDataBaseAttribute(String attr) {
		try {
			NodeList nList = doc.getElementsByTagName("database");
			return ((Element) nList.item(0)).getElementsByTagName(attr).item(0)
					.getTextContent();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "It is necessary to define the database "
					+ "atttribute '" + attr + "' in the configuration file.", e);
			System.exit(1);
			return null;
		}
	}

	public String getIndexPath() {
		try {
			String home = System.getProperty("user.home");
			NodeList nList = doc.getElementsByTagName("index");
			String path = ((Element) nList.item(0)).getElementsByTagName("path")
					.item(0).getTextContent();
			return path.replace("$home$", home);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "It is necessary to define the index "
					+ "path in the configuration file.", e);
			System.exit(1);
			return null;
		}
	}

	private Document parseXMLFile() throws ParserConfigurationException,
			SAXException, IOException {
		File fXmlFile = new File("configuration.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		return doc;
	}

	public boolean isTestMode() {
		return testMode;
	}

	public void setTestMode(boolean testMode) {
		this.testMode = testMode;
	}

}
