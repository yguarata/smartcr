package com.smartcr.core.util;

import org.apache.commons.lang.StringUtils;

public class Task {

	private IProgressMonitor monitor;
	private String name;
	private int totalWork = 0;
	private int worked = 0;
	
	public Task(String name, int totalWork, IProgressMonitor monitor) {
		this.name = name;
		this.totalWork = totalWork;
		this.monitor = monitor;
		System.out.println("Performing " + getName() + "...");
	}

	public String getName() {
		return this.name;
	}
	
	public void end() {
		monitor.removeTask(this);
	}
	
	@Override
	public boolean equals(Object obj) {
		Task other = (Task) obj;
		return this.name.equals(other.getName());
	}

	public IProgressMonitor getMonitor() {
		return monitor;
	}

	public void setMonitor(IProgressMonitor monitor) {
		this.monitor = monitor;
	}

	public int getTotalWork() {
		return totalWork;
	}

	public void setTotalWork(int totalWork) {
		this.totalWork = totalWork;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWorked() {
		return worked;
	}

	public void unitWorked(String extraInfo) {
		this.worked(++worked, extraInfo);
	}
	
	public void worked(int worked, String extraInfo) {
		this.worked = worked;
		update(extraInfo);
		monitor.update(this, extraInfo);
	}
	
	public void update(String extraInfo) {
		float workedFloat = worked;
		float totalWorkFloat = this.totalWork;
		double total = (workedFloat/totalWorkFloat)*100.0;
		if (!StringUtils.isEmpty(extraInfo)) {
			System.out.print(String.format("\r%s%% [%s]", Math.round(total), extraInfo));
		} else {
			System.out.print(String.format("\r%s%%", Math.round(total)));
		}
		if (total == 100) {
			end();
			System.out.println("\n" + StringUtils.capitalize(name) + " is done.");
		}
	}
	
	public static void main(String[] args) {
		Task task = new Task("some", 100, DefaultProgressMonitor.getInstance());
		task.worked(10, null);
		task.worked(20, null);
		task.worked(21, null);
		task.worked(22, null);
	}

}
