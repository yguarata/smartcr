package com.smartcr.core.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.persitence.BaseDAO;
import com.smartcr.core.model.persitence.ChangeRequestDAO;
import com.smartcr.core.util.IProgressMonitor;
import com.smartcr.core.util.RepositoryConfig;

public class Fetcher {

	protected RepositoryConfig repositoryConfig;
	protected IProgressMonitor monitor;
	private boolean fetchFromLastID;
	private BaseDAO<ChangeRequest> crDAO = ChangeRequestDAO.getInstance();
	private IChangeRequestFetcher fetcherImpl;

	public Fetcher(IChangeRequestFetcher fetcherImpl, RepositoryConfig config,
			IProgressMonitor monitor) {
		this.fetcherImpl = fetcherImpl;
		fetcherImpl.configure(config);
		repositoryConfig = config;
		this.monitor = monitor;
	}

	public IChangeRequestFetcher getFetcherImpl() {
		return fetcherImpl;
	}

	public void setFetcherImpl(IChangeRequestFetcher fetcherImpl) {
		this.fetcherImpl = fetcherImpl;
	}

	public ChangeRequest getChangeRequestSessioned(String id) {
		ChangeRequest cr = null;
		try {
			fetcherImpl.startWorkSession();
			cr = fetcherImpl.getChangeRequest(id);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fetcherImpl.stopWorkSession();
		}
		return cr;
	}

	public List<ChangeRequest> findAllChangeRequests() {
		List<ChangeRequest> crs = new ArrayList<ChangeRequest>();
		try {
			fetcherImpl.startWorkSession();
			crs.addAll(fetcherImpl.findAllChangeRequests());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fetcherImpl.stopWorkSession();
		}
		return crs;
	}

	public List<ChangeRequest> findChangeRequestsByAvailableIDs() {
		Set<String> ids = fetcherImpl.findAvailableChangeRequestIDs();
		List<ChangeRequest> crs = new ArrayList<ChangeRequest>();
		try {
			fetcherImpl.startWorkSession();
			for (String id : ids) {
				crs.add(fetcherImpl.getChangeRequest(id));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fetcherImpl.stopWorkSession();
		}
		return crs;
	}

	/**
	 * Get the final list of IDs to retrieve, applying the limit constraint if
	 * provided in the configuration file. The final list is ascendent ordered.
	 * 
	 * @return
	 */
	public Set<String> getActualChangeRequestIDs() {
		List<String> ids = new ArrayList<String>();
		try {
			fetcherImpl.startWorkSession();
			ids = new ArrayList<String>(
					fetcherImpl.findAvailableChangeRequestIDs());
			if (isFetchFromLastID()) {
				int start = getLastFecthedIDIndex(ids);
				ids = ids.subList(start+1, ids.size());
			}
			if (repositoryConfig.getLimit() != null) {
				ids = ids.subList(0, repositoryConfig.getLimit().intValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			fetcherImpl.stopWorkSession();
		}
		return new LinkedHashSet<String>(ids);
	}

	private int getLastFecthedIDIndex(List<String> ids) {
		List<ChangeRequest> crs = crDAO.findAll();
		if (CollectionUtils.isEmpty(crs)) {
			return 0;
		}
		List<Long> orids = new ArrayList<Long>();
		for (ChangeRequest cr : crs) {
			orids.add(new Long(cr.getOriginalId()));
		}
		Collections.sort(orids);
		Long lastOrid = orids.get(orids.size() - 1);
		return ids.indexOf(lastOrid.toString());
	}

	@Override
	public boolean equals(Object obj) {
		Fetcher other = (Fetcher) obj;
		return this.getRepositoryConfig().getName()
				.equals(other.getRepositoryConfig().getName());
	}

	public RepositoryConfig getRepositoryConfig() {
		return repositoryConfig;
	}

	/**
	 * Gets the repository name.
	 * 
	 * @return
	 */
	public String getName() {
		return repositoryConfig.getName();
	}

	public String getUser() {
		return repositoryConfig.getUser();
	}

	public String getPassword() {
		return repositoryConfig.getPassword();
	}

	public boolean isFetchFromLastID() {
		return fetchFromLastID;
	}

	public String getUrl() {
		return repositoryConfig.getUrl();
	}

	public void setFetchFromLastID(boolean bool) {
		this.fetchFromLastID = bool;
	}

	public IProgressMonitor getMonitor() {
		return monitor;
	}

	public void setMonitor(IProgressMonitor monitor) {
		this.monitor = monitor;
	}
}