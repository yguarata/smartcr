package com.smartcr.core.util;

import java.util.LinkedHashSet;
import java.util.Set;


public class RepositoryConfig {

	private String name;
	private RepositoryType type;
	private String user;
	private String password;
	private String url;
	private String fetcherClass;
	private Long limit;
	private Set<String> ids = new LinkedHashSet<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RepositoryType getType() {
		return type;
	}

	public void setType(RepositoryType type) {
		this.type = type;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFetcherClass() {
		return fetcherClass;
	}

	public void setFetcherClass(String fetcherClass) {
		this.fetcherClass = fetcherClass;
	}

	public Long getLimit() {
		return limit;
	}

	public void setLimit(Long limit) {
		this.limit = limit;
	}

	public Set<String> getIds() {
		return ids;
	}

	public void setIds(Set<String> ids) {
		this.ids = ids;
	}

}
