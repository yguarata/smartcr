package com.smartcr.core.index;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Query;

import com.smartcr.core.model.ChangeRequest;

public interface IIndex {

	public void addFetcher(Fetcher fetcher);

	public void updateIndex() throws IOException,
			FetcherImplRequiredException, IndexException;

	public void createIndex() throws IOException,
			FetcherImplRequiredException, IndexException;

	/**
	 * Store the CRs one by one from all fetchers configured.
	 * 
	 * @return A hash map indicating the IDs of the change requests that could
	 *         not be retrieved with each fetcher.
	 */
	public HashMap<Fetcher, List<String>> storeAllChangeRequestsFromFetchers();

	/**
	 * Store the CRs one by one.
	 * 
	 * @param crFetcher
	 *            The fetcher which will retrieve the CRs remotely.
	 * @return A list of Change Request IDs which could not be retrieved.
	 */
	public List<String> storeAllChangeRequests(String repositoryName);
	
	public List<String> storeAllChangeRequests(Fetcher fetcher);

	public void indexChangeRequest(ChangeRequest cr)
			throws IOException, IndexException;

	public void indexChangeRequests(List<ChangeRequest> crs)
			throws IOException, IndexException;

	public int count() throws IOException;

	public List<Document> search(String queryStr) throws IOException,
			ParseException;

	public Query createQuery(String queryStr) throws ParseException;

	public List<String> getProjectsName() throws IOException;

	public List<Fetcher> getFetchers();
	
	public Fetcher getFetcher(String name);

	public void setFetchers(List<Fetcher> fetchers);

	public void setIndexPath(String path);

	public void resetIndex();

}