package com.smartcr.core.index;

public class IndexException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1809564494074532854L;
	
	public IndexException(String msg) {
		super(msg);
	}
	
	public IndexException(String msg, Exception e) {
		super(msg, e);
	}

}
