package com.smartcr.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.commons.lang.StringUtils;

import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.model.History;
import com.smartcr.core.model.Rule;
import com.smartcr.core.model.persitence.AuthorDAO;
import com.smartcr.core.model.persitence.ChangeRequestDAO;
import com.smartcr.core.model.persitence.CommentDAO;
import com.smartcr.core.model.persitence.DAORegister;
import com.smartcr.core.model.persitence.DatabaseConfig;
import com.smartcr.core.model.persitence.HistoryDAO;
import com.smartcr.core.model.persitence.RuleDAO;

public class ModelFacade {

	static Logger logger = Logger.getLogger(ModelFacade.class.getName());

	private static ModelFacade coreFacadeInstance = null;

	private ChangeRequestDAO changeRequestDAO = ChangeRequestDAO.getInstance();

	private AuthorDAO authorDAO = AuthorDAO.getInstance();

	private ModelFacade() {

	}

	public static ModelFacade getInstance() {
		if (coreFacadeInstance == null) {
			coreFacadeInstance = new ModelFacade();
		}

		return coreFacadeInstance;
	}

	public Author getAuthor(Long id) {
		return authorDAO.get(id);
	}

	public Author getAuthorByEmail(String email) {
		return authorDAO.findByEmail(email);
	}

	public ChangeRequest getCR(Long id) {
		return changeRequestDAO.get(Long.valueOf(id));
	}

	public ChangeRequest getCRByOriginalId(Long originalId) {
		return changeRequestDAO.findByProperty("originalId", originalId)
				.getSingleResult();
	}

	public void deleteCR(ChangeRequest changeRequest) {
		changeRequestDAO.delete(changeRequest);
	}

	public void deleteCR(Long id) {
		changeRequestDAO.delete(id);
	}

	public Long countCRs() {
		return changeRequestDAO.count();
	}

	public Long countAuthors() {
		return authorDAO.count();
	}

	public List<Author> findAllAuthors() {
		return authorDAO.findAllOrderByAsc("name");
	}

	// public void insertOrUpdateCR(ChangeRequest changeRequest) {
	// changeRequestDAO.insertOrUpdate(changeRequest);
	// }

	public List<ChangeRequest> findUnassignedCRs() {
		return changeRequestDAO.findUnassigned();
	}

	public List<ChangeRequest> findAssignedCRs() {
		return changeRequestDAO.findAssigned();
	}
	
	public List<ChangeRequest> findAssignedCRsByProject(String project) {
		return changeRequestDAO.findAssignedByProject(project);
	}
	
	public List<ChangeRequest> findClosedCRs() {
		return changeRequestDAO.findClosed();
	}

	public List<ChangeRequest> findAllCRs() {
		return changeRequestDAO.findAllOrderByAsc("id");
	}

	public List<ChangeRequest> findCRsCreatedBy(Author author) {
		return changeRequestDAO.findCreatedBy(author);
	}
	
	public List<ChangeRequest> findCRsFixedBy(Author author) {
		return changeRequestDAO.findFixedBy(author);
	}
	
	public List<ChangeRequest> findCRsClosedBy(Author author) {
		return changeRequestDAO.findClosedBy(author);
	}
	
	public List<ChangeRequest> findCRByExample(ChangeRequest crExample) {
		return changeRequestDAO.findByExample(crExample);
	}

	public void configureDatabase(DatabaseConfig databaseConfig) {
		DAORegister.getInstance().configureDatabase(databaseConfig);
	}

	enum CountType {
		STATUS, RESOLUTION, CREATED_BY, FIXED_BY, COMPONENT, PROJECT;
	}

	/**
	 * Counts how many CRs there are for each CR status.
	 * 
	 * @return
	 */
	public SortedMap<String, Integer> getCRStatusCount() {
		return count(CountType.STATUS);
	}

	/**
	 * Counts how many CRs each developer has created.
	 * 
	 * @return
	 */
	public SortedMap<String, Integer> getCRCreatedByCount() {
		return count(CountType.CREATED_BY);
	}

	/**
	 * Counts how many CRs each developer has solved.
	 * 
	 * @return
	 */
	public SortedMap<String, Integer> getCRFixedByCount() {
		return count(CountType.FIXED_BY);
	}

	/**
	 * Counts how many CRs there are for each CR resolution.
	 * 
	 * @return
	 */
	public SortedMap<String, Integer> getCRResolutionCount() {
		return count(CountType.RESOLUTION);
	}

	/**
	 * Counts how many CRs there are for each component.
	 * 
	 * @return
	 */
	public SortedMap<String, Integer> getCRComponentCount() {
		return count(CountType.COMPONENT);
	}

	/**
	 * Counts how many CRs there are for each project.
	 * 
	 * @return
	 */
	public SortedMap<String, Integer> getCRProjectCount() {
		return count(CountType.PROJECT);
	}

	private SortedMap<String, Integer> count(CountType countType) {
		SortedMap<String, Integer> count = new TreeMap<String, Integer>();

		for (ChangeRequest cr : findAllCRs()) {
			String key = null;
			if (countType == CountType.STATUS) {
				key = cr.getStatus();
			} else if (countType == CountType.RESOLUTION) {
				key = cr.getResolution();
				if (StringUtils.isEmpty(key)) {
					key = "No resolution";
				}
			} else if (countType == CountType.CREATED_BY) {
				key = getAnyNotNullValue(cr.getCreatedBy());
			} else if (countType == CountType.COMPONENT) {
				key = cr.getOrigin();
				if (StringUtils.isEmpty(key)) {
					continue;
				}
			} else if (countType == CountType.PROJECT) {
				key = cr.getProject();
				if (StringUtils.isEmpty(key)) {
					continue;
				}
			} else if (countType == CountType.FIXED_BY) {
				if (cr.getFixedBy() != null) {
					key = getAnyNotNullValue(cr.getFixedBy());
				} else {
					continue;
				}
			}

			if (!count.containsKey(key)) {
				count.put(key, 1);
			} else {
				int newCount = count.get(key) + 1;
				count.put(key, newCount);
			}
		}
		return count;
	}

	private String getAnyNotNullValue(Author author) {
		String key;
		if (StringUtils.isNotEmpty(author.getEmail())) {
			key = author.getEmail();
		} else if (StringUtils.isNotEmpty(author.getEmail())) {
			key = author.getName();
		} else {
			key = String.valueOf(author.getId());
		}
		return key;
	}

	/**
	 * Counts the average time for each resolution. The time is given according
	 * to the timeUnit.
	 * 
	 * @param timeUnit
	 * @return
	 */
	public SortedMap<String, Long> getAverageTimeByResolution(TimeUnit timeUnit) {
		logger.info("Getting avegare time according to the resolutions.");
		SortedMap<String, Long> count = new TreeMap<String, Long>();
		SortedMap<String, ArrayList<Long>> countArray = new TreeMap<String, ArrayList<Long>>();

		/* Creates an array of all time delta for each resolution. */
		for (ChangeRequest cr : changeRequestDAO.findAll()) {
			if (StringUtils.isEmpty(cr.getResolution())
					|| cr.getDateClosed() == null) {
				continue;
			}
			if (!countArray.containsKey(cr.getResolution())) {
				logger.info("Considering resolution " + cr.getResolution());
				countArray.put(cr.getResolution(), new ArrayList<Long>());
			}
			long diff = cr.getDateClosed().getTime()
					- cr.getDateCreated().getTime();
			countArray.get(cr.getResolution()).add(new Long(diff));
		}

		/* Counts the average time for each resolution. */
		for (Entry<String, ArrayList<Long>> entry : countArray.entrySet()) {
			logger.info("Calculating time for " + entry.getKey());
			long total = 0;
			for (Long diff : entry.getValue()) {
				total += diff;
			}
			long avgTimeMill = total / entry.getValue().size();
			logger.info("Average time in milliseconds for " + entry.getKey()
					+ ": " + avgTimeMill);
			long avgTimeDays = timeUnit.convert(avgTimeMill,
					TimeUnit.MILLISECONDS);
			count.put(entry.getKey(), avgTimeDays);
			logger.info("Average time for " + entry.getKey() + ": "
					+ avgTimeDays);
		}

		return count;
	}

	public void insertOrUpdateRule(Rule rule) {
		if (rule.getId() != null) {
			RuleDAO.getInstance().update(rule);
		} else {
			RuleDAO.getInstance().insert(rule);
		}
	}

	public List<Rule> findAllRules() {
		return RuleDAO.getInstance().findAll();
	}

	public Rule getRule(Long id) {
		return RuleDAO.getInstance().get(id);
	}

	public void removeRule(Rule rule) {
		RuleDAO.getInstance().delete(rule);
	}

	public void updateCR(ChangeRequest cr) {
		ChangeRequestDAO.getInstance().update(cr);
	}

	public void insertCR(ChangeRequest cr) {
		ChangeRequestDAO.getInstance().insert(cr);
	}

	public void insertAuthor(Author author) {
		AuthorDAO.getInstance().insert(author);
	}

	public void insertComment(Comment comment) {
		CommentDAO.getInstance().insert(comment);
	}

	public List<Comment> findCommentsByAuthor(Author author) {
		return CommentDAO.getInstance().findByProperty("author", author).getResultList();
	}
	
	public List<History> findHistoryByAuthor(Author author) {
		return HistoryDAO.getInstance().findByProperty("author", author).getResultList();
	}
	
	public void deleteCommentByOriginalID(Long id) {
		Comment comment = CommentDAO.getInstance()
				.findByProperty("originalId", id).getSingleResult();
		CommentDAO.getInstance().delete(comment);
	}

	public Comment getCommentByOriginalID(Long id) {
		return CommentDAO.getInstance().findByProperty("originalId", id)
				.getSingleResult();
	}

	public void updateComment(Comment comment) {
		CommentDAO.getInstance().update(comment);
	}

	public void deleteAuthor(Author author) {
		AuthorDAO.getInstance().delete(author);
	}

	public void updateHistory(History history) {
		HistoryDAO.getInstance().update(history);
	}

	public List<History> findAllHistory() {
		return HistoryDAO.getInstance().findAll();
	}

	public void updateAuthor(Author author) {
		AuthorDAO.getInstance().update(author);
	}

	public Author getAuthorByName(String authorName) {
		return AuthorDAO.getInstance().findByName(authorName);
	}

	public List<String> findAllProjects() {
		return ChangeRequestDAO.getInstance().findProjects();
	}

	public void deleteRule(Rule rule) {
		RuleDAO.getInstance().delete(rule);
	}

	public List<ChangeRequest> findCRsByProject(String project) {
		ChangeRequest cr = new ChangeRequest();
		cr.setProject(project);
		return ChangeRequestDAO.getInstance().findByExample(cr);
	}

}
