package com.smartcr.core.index;

public class FetcherImplRequiredException extends Exception {

	private static final long serialVersionUID = 6490805819567811291L;

	public FetcherImplRequiredException(String msg) {
		super(msg);
	}
	
	public FetcherImplRequiredException() {
		super("The list of fetchers is empty. " +
				"At least, one object that implements " +
				"IChangeRequestFetcher must be provided.");
	}
	
}
