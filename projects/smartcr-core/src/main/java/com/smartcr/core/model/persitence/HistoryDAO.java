package com.smartcr.core.model.persitence;

import java.util.logging.Logger;

import com.smartcr.core.model.History;

public class HistoryDAO extends BaseDAO<History> {

	Logger logger = Logger.getLogger(getClass().getName());
	private static HistoryDAO instance = null;

	private HistoryDAO() {
	}

	public static HistoryDAO getInstance() {
		if (instance == null) {
			instance = new HistoryDAO();
			DAORegister.getInstance().register(instance);
		}
		return instance;
	}

	@Override
	public Class<History> getClazz() {
		return History.class;
	}

}
