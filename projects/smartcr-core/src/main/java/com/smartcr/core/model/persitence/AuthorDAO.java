package com.smartcr.core.model.persitence;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;

import com.smartcr.core.model.Author;

public class AuthorDAO extends BaseDAO<Author> {

	private static AuthorDAO instance = null;

	private AuthorDAO() {
	}

	public static AuthorDAO getInstance() {
		if (instance == null) {
			instance = new AuthorDAO();
			DAORegister.getInstance().register(instance);
		}
		return instance;
	}

	@Override
	public Class<Author> getClazz() {
		return Author.class;
	}

	public Author find(Author author) {
		Map<String, Object> props = new HashMap<String, Object>();

		if (author.getEmail() != null && !author.getEmail().isEmpty()) {
			props.put("email", author.getEmail());
		}

		if (author.getName() != null && !author.getName().isEmpty()) {
			props.put("name", author.getName());
		}

		TypedQuery<Author> typedQuery = findByProperties(props);
		try {
			return typedQuery.getSingleResult();
		} catch (NonUniqueResultException e) {
			return typedQuery.getResultList().get(0);
		} catch (NoResultException e) {
			return null;
		}
	}

	public Author findByEmail(String email) {
		TypedQuery<Author> query = findByProperty("email", email);
		if (query.getResultList().size() == 1) {
			return query.getResultList().get(0);
		} else {
			return null;
		}
	}
	
	public Author findByName(String name) {
		TypedQuery<Author> query = findByProperty("name", name);
		if (query.getResultList().size() == 1) {
			return query.getResultList().get(0);
		} else {
			return null;
		}
	}

}
