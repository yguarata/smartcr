package com.smartcr.core.model.persitence;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DAORegister {

	static Logger logger = Logger.getLogger(DAORegister.class.getName());
	private static EntityManagerFactory entityManagerFactory;
	private static EntityManager entityManager;
	private HashMap<String, String> props = new HashMap<String, String>();

	private static DAORegister instance = null;

	private static Set<BaseDAO<?>> daoRegister = new HashSet<BaseDAO<?>>();

	private DAORegister() {
	}

	public static DAORegister getInstance() {
		if (instance == null) {
			instance = new DAORegister();
		}
		return instance;
	}

	public void register(BaseDAO<?> dao) {
		if (!daoRegister.contains(dao)) {
			logger.info("Registering dao " + dao.getClass().getName());
			daoRegister.add(dao);
		} else {
			logger.info("DAO " + dao.getClass().getName() + " already registered.");
		}
	} 

	public void configureDatabase(DatabaseConfig databaseConfig) {
		assert databaseConfig != null; //fdsfsd

		if (entityManagerFactory == null) {
			props.put("javax.persistence.validation.mode", "none");
			props.put("hibernate.dialect", databaseConfig.getDatabaseDialect());
			props.put("hibernate.connection.driver_class",
					databaseConfig.getDatabaseDriver());
			props.put("hibernate.connection.username",
					databaseConfig.getDatabaseUser());
			props.put("hibernate.connection.password",
					databaseConfig.getDatabasePassword());
			props.put("hibernate.connection.url",
					databaseConfig.getDatabaseUrl());
			props.put("hibernate.hbm2ddl.auto",
					databaseConfig.getDatabaseAuto());
			props.put("hibernate.show_sql", databaseConfig.getDatabaseShowSQL());
			entityManagerFactory = Persistence.createEntityManagerFactory(
					"smartcr", props);
		}

		if (entityManager == null) {
			entityManager = entityManagerFactory.createEntityManager();
		}
	}
	
	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

}
