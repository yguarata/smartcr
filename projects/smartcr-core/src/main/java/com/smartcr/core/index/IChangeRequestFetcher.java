package com.smartcr.core.index;

import java.util.List;
import java.util.Set;

import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.util.RepositoryConfig;

public interface IChangeRequestFetcher {

	/**
	 * This method is only useful if it is desired to handle the configuration
	 * information of the fetcher, such as repository URL, username, password,
	 * change request IDs. If so, use this methods to keep a copy of that
	 * configuration.
	 * 
	 * @param config
	 *            The repository configuration. It is read only.
	 */
	public void configure(RepositoryConfig config);

	/**
	 * This method is called every time that it is made a call to the fetcher's
	 * implementation methods. Generelly it holds code for login or other
	 * configuration porpuses.
	 */
	public void startWorkSession();

	/**
	 * This method is called after calling any methods of the fetcher
	 * implementation. Generelly it holds code for logout or other configuration
	 * porpuses.
	 */
	public void stopWorkSession();

	/**
	 * Retrieve all change requests from the repository which the fetcher
	 * implementation are connected.
	 * 
	 * @return
	 */
	public List<ChangeRequest> findAllChangeRequests();

	/**
	 * Retrive a change request given its id.
	 * 
	 * @param id
	 *            The identifier of the change request.
	 * @return
	 */
	public ChangeRequest getChangeRequest(String id);

	/**
	 * Get the IDs of all change requests that can be retrieved from the
	 * repository which the fetcher implementation is connected.
	 * 
	 * @return
	 */
	public Set<String> findAvailableChangeRequestIDs();

}
