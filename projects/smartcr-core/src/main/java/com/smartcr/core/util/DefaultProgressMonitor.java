package com.smartcr.core.util;

import java.util.ArrayList;
import java.util.List;

public class DefaultProgressMonitor implements IProgressMonitor {

	private static IProgressMonitor instance;
	private List<Task> tasks = new ArrayList<Task>();

	private DefaultProgressMonitor() {
	}

	public static IProgressMonitor getInstance() {
		if (instance == null) {
			instance = new DefaultProgressMonitor();
		}
		return instance;
	}
	
	@Override
	public Task beginTask(String name, int totalWork) {
		Task newTask = new Task(name, totalWork, this);
		tasks.add(newTask);
		return newTask;
	}

	@Override
	public void subTask(String name) {

	}

	public List<Task> getTasks() {
		return tasks;
	}

	@Override
	public void removeTask(Task sysoutTask) {
		if (getTasks().contains(sysoutTask)) {
			getTasks().remove(sysoutTask);
		}
	}

	@Override
	public void update(Task task, String extraInfo) {
		System.out.println("Calling " + getClass().getName());
	}

}
