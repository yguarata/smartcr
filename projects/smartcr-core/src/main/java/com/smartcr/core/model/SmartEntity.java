package com.smartcr.core.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OrderBy;

@MappedSuperclass
public abstract class SmartEntity {

	/**
	 * This is the identifier of the entity in the database.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID", insertable = true, unique = true, nullable = false)
	@OrderBy(value = "ASC")
	private Long id;

	/**
	 * This id should be set only when the entity is being imported from some
	 * other other repository, such as Bugzilla, Mantis, Trac, etc.
	 */
	@Column(name = "ORIGINAL_ID", nullable = true, unique = true)
	private String originalId;

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (obj == null) {
			return false;
		}

		SmartEntity other = (SmartEntity) obj;

		if (getId() == null) {
			if (other.getId() != null) {
				return false;
			}
		} else if (!getId().equals(other.getId())) {
			return false;
		}

		return true;
	}

}
