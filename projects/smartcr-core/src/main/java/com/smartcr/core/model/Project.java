package com.smartcr.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.sun.istack.internal.NotNull;

@Entity
@Table(name = "PROJECT")
public class Project extends SmartEntity {

	@Column(name = "TITLE", length = 10000)
	private String title;

	@Column(name = "DESCRIPTION", length = 1000000)
	private String description;
	
	@NotNull
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
