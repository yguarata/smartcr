package com.smartcr.core.model.persitence;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.smartcr.core.model.SmartEntity;

public abstract class BaseDAO<T extends SmartEntity> {

	static Logger logger = Logger.getLogger(BaseDAO.class.getName());

	public void insert(final T obj) {
		try {
			getEntityManager().getTransaction().begin();
			getEntityManager().persist(obj);
			getEntityManager().getTransaction().commit();
			logger.info("Inserted new object " + obj.getClass().getSimpleName()
					+ " " + obj.getId() + " in the database.");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error inserting: "
					+ obj.getClass().getSimpleName(), e);
			getEntityManager().getTransaction().rollback();
		}
	}

	public Boolean contains(final Long id) {
		return get(id) != null;
	}

	public void delete(final Long id) {
		delete(get(id));
	}

	public void delete(final T obj) {
		try {
			getEntityManager().getTransaction().begin();
			getEntityManager().remove(obj);
			getEntityManager().getTransaction().commit();
			logger.info("Deleted object " + obj.getClass().getSimpleName()
					+ " " + obj.getId() + " from the database.");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error deleting: "
					+ obj.getClass().getSimpleName() + " " + obj.getId(), e);
			getEntityManager().getTransaction().rollback();
		}
	}

	public T get(final Long id) {
		return getEntityManager().find(getClazz(), id);
	}

	public abstract Class<T> getClazz();

	public void update(final T obj) {
		try {
			getEntityManager().getTransaction().begin();
			getEntityManager().merge(obj);
			getEntityManager().getTransaction().commit();
			logger.info("Updated object " + obj.getClass().getSimpleName()
					+ " " + obj.getId() + " in the database.");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error updating "
					+ obj.getClass().getSimpleName() + " " + obj.getId(), e);
			getEntityManager().getTransaction().rollback();
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		final String jpql = "select this from " + getClazz().getSimpleName()
				+ " this";
		final Query query = getEntityManager().createQuery(jpql);

		List<T> lista = query.getResultList();
		return lista;
	}

	public List<T> findAllOrderByAsc(String attribute) {
		return findAllOrderBy(attribute, "asc");
	}

	public List<T> findAllOrderByDesc(String attribute) {
		return findAllOrderBy(attribute, "desc");
	}

	@SuppressWarnings("unchecked")
	private List<T> findAllOrderBy(String attr, String direction) {
		final String jpql = "select this from " + getClazz().getSimpleName()
				+ " this order by " + attr + " " + direction;
		final Query query = getEntityManager().createQuery(jpql);
		List<T> lista = query.getResultList();
		return lista;
	}

	public Long count() {
		final Query query = getEntityManager().createQuery(
				"select count(this) from " + getClazz().getSimpleName()
						+ " this");
		return (Long) query.getSingleResult();
	}

	public EntityManagerFactory getEntityManagerFactory() {
		return DAORegister.getInstance().getEntityManagerFactory();
	}

	public EntityManager getEntityManager() {
		return DAORegister.getInstance().getEntityManager();
	}

	public TypedQuery<T> findByProperty(String property, Object value) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> criteria = builder.createQuery(getClazz());
		Root<T> root = criteria.from(getClazz());
		criteria.where(builder.equal(root.get(property), value));
		TypedQuery<T> typedQuery = getEntityManager().createQuery(criteria);
		return typedQuery;
	}

	public TypedQuery<T> findByProperties(Map<String, Object> props) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> criteria = builder.createQuery(getClazz());
		Root<T> root = criteria.from(getClazz());
		List<Predicate> predicates = new ArrayList<Predicate>();
		for (Entry<String, Object> prop : props.entrySet()) {
			predicates.add(builder.equal(root.get(prop.getKey()),
					prop.getValue()));
		}
		criteria.where(builder.and(predicates.toArray(new Predicate[0])));
		TypedQuery<T> typedQuery = getEntityManager().createQuery(criteria);
		return typedQuery;
	}

	public TypedQuery<T> findByPropertyNull(String property) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> criteria = builder.createQuery(getClazz());
		Root<T> root = criteria.from(getClazz());
		criteria.where(root.get(property).isNull());
		TypedQuery<T> typedQuery = getEntityManager().createQuery(criteria);
		return typedQuery;
	}

	public TypedQuery<T> findByPropertyNotNull(String property) {
		CriteriaBuilder builder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> criteria = builder.createQuery(getClazz());
		Root<T> root = criteria.from(getClazz());
		criteria.where(root.get(property).isNotNull());
		TypedQuery<T> typedQuery = getEntityManager().createQuery(criteria);
		return typedQuery;
	}

	public boolean exists(SmartEntity obj) {
		if (obj.getId() != null) {
			return get(obj.getId()) != null;
		} else {
			return false;
		}
	}

}
