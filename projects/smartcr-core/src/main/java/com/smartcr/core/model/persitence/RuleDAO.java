package com.smartcr.core.model.persitence;

import java.util.List;

import com.smartcr.core.model.Rule;
import com.smartcr.core.model.Rule.RuleType;

public class RuleDAO extends BaseDAO<Rule> {

	private static RuleDAO instance = null;
	
	private RuleDAO() {
	}

	public static RuleDAO getInstance() {
		if (instance == null) {
			instance = new RuleDAO();
			DAORegister.getInstance().register(instance);
		}
		return instance;
	}

	@Override
	public Class<Rule> getClazz() {
		return Rule.class;
	}

	public List<Rule> findComplexRules() {
		return findByProperty("ruleType", RuleType.COMPLEX).getResultList();
	}
	
	public List<Rule> findSimpleRules() {
		return findByProperty("ruleType", RuleType.SIMPLE).getResultList();
	}

}
