package com.smartcr.core.util;

import java.util.List;

public interface IProgressMonitor {

	public Task beginTask(String name, int totalWork);

	public void subTask(String name);

	public List<Task> getTasks();

	public void removeTask(Task sysoutTask);

	/**
	 * Received an update event from the task.
	 * 
	 * @param task
	 */
	public void update(Task task, String extraInfo);

}