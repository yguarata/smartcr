package com.smartcr.core.util;

public enum RepositoryType {

	CHANGE_REQUEST("change-request");
	
	private String repositoryType;

	private RepositoryType(String type) {
		this.setRepositoryType(type);
	}

	public String getRepositoryType() {
		return repositoryType;
	}

	public void setRepositoryType(String repositoryType) {
		this.repositoryType = repositoryType;
	}
	
}
