package com.smartcr.core.index;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.SearcherFactory;
import org.apache.lucene.search.SearcherManager;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.model.persitence.ChangeRequestDAO;
import com.smartcr.core.util.Task;

public class LuceneIndexer implements IIndex {

	static Logger logger = Logger.getLogger(LuceneIndexer.class.getName());
	private static final Version LUCENE_VERSION = Version.LUCENE_43;
	private List<Fetcher> fetchers = new ArrayList<Fetcher>();

	private static IIndex luceneIndexer;
	private Directory directory;
	private ChangeRequestDAO changeRequestDAO = ChangeRequestDAO.getInstance();
	private Analyzer analyzer;
	private IndexWriterConfig indexWriterConfig;
	private String indexPath;
	private IndexWriter indexWriter;

	private LuceneIndexer() {
		indexPath = System.getProperty("user.home");
		setIndexPath(indexPath);
		analyzer = new StandardAnalyzer(LUCENE_VERSION);
		indexWriterConfig = new IndexWriterConfig(LUCENE_VERSION, analyzer);
		indexWriterConfig.setOpenMode(OpenMode.CREATE_OR_APPEND);
		try {
			indexWriter = new IndexWriter(directory, indexWriterConfig);
			// IndexReader reader = DirectoryReader.open(indexWriter, false);
			// searcher = new IndexSearcher(reader);
		} catch (IOException e) {
			try {
				indexWriterConfig.setOpenMode(OpenMode.CREATE);
				indexWriter = new IndexWriter(directory, indexWriterConfig);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	@Override
	public void resetIndex() {
		try {
			logger.info("Reseting the index...");
			indexWriter.deleteAll();
			indexWriter.prepareCommit();
			indexWriter.commit();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setIndexPath(String path) {
		indexPath = path;
		try {
			directory = FSDirectory.open(new File(indexPath));
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Could not set the index path: "
					+ indexPath);
			e.printStackTrace();
			indexPath = System.getProperty("user.home");
			logger.info("Setting the path to " + indexPath);
			try {
				directory = FSDirectory.open(new File(indexPath));
			} catch (IOException e1) {
				logger.log(Level.SEVERE, "Could not set the index path: "
						+ indexPath + ". Exiting.");
				e1.printStackTrace();
				System.exit(1);
			}
		}
	}

	public static IIndex getInstance() {
		if (luceneIndexer == null) {
			luceneIndexer = new LuceneIndexer();
		}

		return luceneIndexer;
	}

	@Override
	public void addFetcher(Fetcher fetcher) {
		fetchers.add(fetcher);
	}

	@Override
	public void updateIndex() throws IOException, FetcherImplRequiredException,
			IndexException {
		indexAll();
	}

	@Override
	public void createIndex() throws IOException, FetcherImplRequiredException,
			IndexException {
		indexAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HashMap<Fetcher, List<String>> storeAllChangeRequestsFromFetchers() {
		HashMap<Fetcher, List<String>> notAdded = new HashMap<Fetcher, List<String>>();
		for (Fetcher crFetcher : getFetchers()) {
			List<String> na = storeChangeRequests(crFetcher);
			notAdded.put(crFetcher, na);
		}
		return notAdded;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> storeAllChangeRequests(String repositoryName) {
		Fetcher f = getFetcher(repositoryName);
		if (f != null) {
			return storeChangeRequests(f);
		}
		return null;
	}

	@Override
	public List<String> storeAllChangeRequests(Fetcher fetcher) {
		return storeChangeRequests(fetcher);
	}

	/**
	 * Store the CRs one by one.
	 * 
	 * @param crFetcher
	 *            The fetcher which will retrieve the CRs remotely.
	 * @return A list of Change Request IDs which could not be retrieved.
	 */
	private List<String> storeChangeRequests(Fetcher crFetcher) {
		List<String> notAdded = new ArrayList<String>();
		Set<String> ids = crFetcher.getActualChangeRequestIDs();
		int count = 0;
		String taskName = String.format("retrieval of %s CRs from %s",
				ids.size(), crFetcher.getRepositoryConfig().getName());
		Task task = crFetcher.monitor.beginTask(taskName, ids.size());
		for (String crID : ids) {
			task.worked(++count, "Getting CR orid=" + crID + "...");
			ChangeRequest cr = crFetcher.getChangeRequestSessioned(crID);
			if (cr != null) {
				changeRequestDAO.insert(cr);
				logger.info("Added cr " + cr.getId() + " with "
						+ cr.getComments().size() + " comments and "
						+ cr.getHistory().size() + " history.");
			} else {
				notAdded.add(crID);
			}
		}
		task.end();
		return notAdded;
	}

	@Override
	public Fetcher getFetcher(String repositoryName) {
		for (Fetcher f : getFetchers()) {
			if (f.getRepositoryConfig().getName().toLowerCase()
					.equals(repositoryName.toLowerCase())) {
				return f;
			}
		}
		return null;
	}

	private void indexAll() throws IOException, IndexException {
		indexChangeRequests(changeRequestDAO.findAll());
	}

	@Override
	public void indexChangeRequest(ChangeRequest cr) throws IOException,
			IndexException {
		index(cr);
	}

	@Override
	public void indexChangeRequests(List<ChangeRequest> crs)
			throws IOException, IndexException {
		if (CollectionUtils.isEmpty(crs)) {
			throw new IndexException("There is no CR to be indexed.");
		}
		for (ChangeRequest cr : crs) {
			index(cr);
		}
	}

	@Override
	public int count() throws IOException {
		SearcherManager mgr = new SearcherManager(indexWriter, false,
				new SearcherFactory());
		IndexSearcher searcher = mgr.acquire();
		int count = searcher.getIndexReader().maxDoc();
		mgr.release(searcher);
		return count;
	}

	private void index(ChangeRequest cr) throws IOException, IndexException {
		if (cr == null) {
			throw new IndexException("The CR objecto is null.");
		}

		SearcherManager mgr = new SearcherManager(indexWriter, false,
				new SearcherFactory());
		IndexSearcher searcher = mgr.acquire();

		if (searcher.getIndexReader().maxDoc() < cr.getId().intValue()) {
			logger.info("Indexing CR " + cr.getId() + "...");
			indexWriter.addDocument(changeRequestToDoc(cr));
		} else {
			logger.info("Updating the index of CR " + cr.getId() + "...");
			indexWriter.updateDocument(new Term("id", cr.getId().toString()),
					changeRequestToDoc(cr));
		}
		mgr.release(searcher);
		indexWriter.prepareCommit();
		indexWriter.commit();
	}

	@Override
	public List<Document> search(String queryStr) throws IOException,
			ParseException {
		logger.info("Searching for '" + queryStr + "'...");
		List<Document> result = new ArrayList<Document>();
		Query query = createQuery(queryStr);
		SearcherManager searchMgr = new SearcherManager(indexWriter, false,
				new SearcherFactory());
		IndexSearcher searcher = searchMgr.acquire();
		try {
			ScoreDoc[] hits = searcher.search(query, 1000).scoreDocs;
			logger.info("Found " + hits.length + " documents in the index.");
			for (int i = 0; i < hits.length; i++) {
				result.add(searcher.doc(hits[i].doc));
			}
		} finally {
			searchMgr.release(searcher);
			searcher = null;
		}
		return result;
	}

	@Override
	public Query createQuery(String queryStr) throws ParseException {
		String fields[] = { "id", "title", "description", "steps", "project",
				"createdby", "closedby", "fixedby", "comments" };
		QueryParser parser = new MultiFieldQueryParser(LUCENE_VERSION, fields,
				analyzer);
		Query query = parser.parse(queryStr);
		return query;
	}

	private List<String> getUniqueTerms(String field) throws IOException {
		List<String> terms = new ArrayList<String>();
		Directory directory = FSDirectory.open(new File(indexPath));
		IndexReader reader = DirectoryReader.open(directory);
		Fields fields = MultiFields.getFields(reader);
		TermsEnum iter = fields.terms("project").iterator(null);
		BytesRef byteRef = null;
		while ((byteRef = iter.next()) != null) {
			String term = new String(byteRef.bytes, byteRef.offset,
					byteRef.length);
			terms.add(term);
		}
		return terms;
	}

	@Override
	public List<String> getProjectsName() throws IOException {
		return getUniqueTerms("project");
	}

	private Document changeRequestToDoc(ChangeRequest cr) {
		Document doc = new Document();
		doc.add(new TextField("id", String.valueOf(cr.getId()), Field.Store.YES));
		doc.add(new TextField("project", StringUtils.defaultIfEmpty(
				cr.getProject(), ""), Field.Store.YES));
		doc.add(new TextField("resolution", StringUtils.defaultIfEmpty(
				cr.getResolution(), ""), Field.Store.YES));
		doc.add(new TextField("status", StringUtils.defaultIfEmpty(
				cr.getStatus(), ""), Field.Store.YES));
		doc.add(new TextField("title", StringUtils.defaultIfEmpty(
				cr.getTitle(), ""), Field.Store.YES));
		doc.add(new TextField("description", StringUtils.defaultIfEmpty(
				cr.getDescription(), ""), Field.Store.YES));
		doc.add(new TextField("steps", StringUtils.defaultIfEmpty(
				cr.getStepsToReproduce(), ""), Field.Store.NO));

		String fixedBy = "";
		if (cr.getFixedBy() != null && cr.getFixedBy().getEmail() != null) {
			fixedBy = cr.getFixedBy().getEmail();
		}

		String createdBy = "";
		if (cr.getCreatedBy() != null && cr.getCreatedBy().getEmail() != null) {
			createdBy = cr.getCreatedBy().getEmail();
		}

		String closedBy = "";
		if (cr.getClosedBy() != null && cr.getClosedBy().getEmail() != null) {
			closedBy = cr.getClosedBy().getEmail();
		}

		doc.add(new TextField("fixedby", fixedBy, Field.Store.YES));
		doc.add(new TextField("createdby", createdBy, Field.Store.YES));
		doc.add(new TextField("closedby", closedBy, Field.Store.YES));

		for (Comment comment : cr.getComments()) {
			doc.add(new TextField("comments", StringUtils.defaultIfEmpty(
					comment.toString(), ""), Field.Store.NO));
		}

		return doc;
	}

	@Override
	public List<Fetcher> getFetchers() {
		return fetchers;
	}

	@Override
	public void setFetchers(List<Fetcher> fetchers) {
		this.fetchers = fetchers;
	}

	public static void main(String[] args) throws IOException, ParseException,
			ClassNotFoundException, InstantiationException,
			IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException,
			SecurityException, FetcherImplRequiredException, IndexException {
		IIndex idx = LuceneIndexer.getInstance();
		idx.createIndex();
		idx.search("psp");
		System.out.println(idx.getProjectsName());
	}

}
