package com.smartcr.core.util;

public class SameRepositoryNameException extends Exception {

	/**
	 * Serial verion UID.
	 */
	private static final long serialVersionUID = 7493406381042406747L;

	public SameRepositoryNameException(String repoName) {
		super("The repository name must be unique: " + repoName);
	}
	
}
