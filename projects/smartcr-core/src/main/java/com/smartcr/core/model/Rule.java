package com.smartcr.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "DROOLS_RULE")
public class Rule extends SmartEntity {

	public enum RuleType {
		SIMPLE, // Rules that don't depend on intelligent features, such as
				// information retrieval. These rules are the first to be
				// applied.

		COMPLEX // Rules that depend on intelligent features, and are applied at
				// the end of the assignment strategy.
	};

	@Column(name = "NAME")
	private String name;

	@Column(name = "RULE_CONTENT", length = 1000000)
	private String ruleContent;

	@Column(name = "RULE_TYPE")
	@Enumerated(EnumType.STRING)
	private RuleType ruleType;

	public RuleType getRuleType() {
		return ruleType;
	}

	public void setRuleType(RuleType ruleType) {
		this.ruleType = ruleType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRuleContent() {
		return ruleContent;
	}

	public void setRuleContent(String rule) {
		this.ruleContent = rule;
	}
	
	public boolean isSimple() {
		return ruleType == RuleType.SIMPLE;
	}
	
	public boolean isComplex() {
		return ruleType == RuleType.COMPLEX;
	}
}
