package com.smartcr.core.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.smartcr.core.util.ConsolePrint;

@Entity
@Table(name = "CHANGE_REQUEST")
public class ChangeRequest extends Task {

	/**
	 * The step to reproduce the problem described in the CR.
	 */
	@Lob
	@Column(name = "STEPS_TO_REPRODUCE")
	private String stepsToReproduce;

	/**
	 * The project which the CR refers.
	 */
	@Column(name = "PROJECT", length = 1000)
	private String project;

	/**
	 * The life-cycle phase where the CR where identified: requirements,
	 * implementation, etc.
	 */
	@Column(name = "ORIGIN", length = 1000)
	private String origin;

	/**
	 * The severity of the CR.
	 */
	@Column(name = "SEVERITY", length = 1000)
	private String severity;

	/**
	 * The status of the CR.
	 */
	@Column(name = "STATUS", length = 1000)
	private String status;

	/**
	 * The resolution given for the CR.
	 */
	@Column(name = "RESOLUTION", length = 1000)
	private String resolution;

	/**
	 * The list of comments for the CR.
	 */
	@OneToMany(mappedBy = "changeRequest", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<Comment> comments = new HashSet<Comment>();

	/**
	 * The artifact affect by this CR.
	 */
	@Column(name = "TARGET", length = 1000)
	private String target;

	/**
	 * Date of the fixing in the origin repository.
	 */
	@Column(name = "DATE_FIXED")
	private Date dateFixed;

	/**
	 * The priority of the change request.
	 */
	@Column(name = "PRIORITY")
	private String priority;

	/**
	 * The version of the software where the change request has been identified.
	 */
	@Column(name = "VERSION")
	private String version;

	/**
	 * The platform where the software executes and the change request has
	 * been identified.
	 */
	@Column(name = "PLATFORM")
	private String platform;

	/**
	 * The operating system where the software executes and the change request
	 * has been identified.
	 */
	@Column(name = "OPERATING_SYSTEM")
	private String operatingSystem;

	/**
	 * The list of history events for the CR.
	 */
	@OneToMany(mappedBy = "changeRequest", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Set<History> history = new HashSet<History>();
	
	public Set<History> getHistory() {
		return history;
	}

	public void setHistory(Set<History> history) {
		this.history = history;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public String getStepsToReproduce() {
		return stepsToReproduce;
	}

	public void setStepsToReproduce(String stepsToRepreduce) {
		this.stepsToReproduce = stepsToRepreduce;
	}

	public Date getDateFixed() {
		return dateFixed;
	}

	public void setDateFixed(Date dateFixed) {
		this.dateFixed = dateFixed;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getPriority() {
		return priority;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(String os) {
		this.operatingSystem = os;
	}

	public boolean isPersisted() {
		return this.getId() != null;
	}
	
	@Override
	public String toString() {
		String comments = "";

		String fmtComment = "OrID: %s Author: %s\n%s\n-------------------------------------------\n";

		if (!getComments().isEmpty()) {
			comments = "-- " + getComments().size() + " COMMENTS --\n";
			for (Comment comment : getComments()) {
				comments += String.format(fmtComment, comment.getId(), comment.getAuthor()
						.getEmail(), ConsolePrint.addLinebreaks(
						comment.getComment(), 80));
			}
		}

		String fmt = String
				.format("+---------------------------------------------------------------------------------------+\n"
						+ "|CR ID: %-15s                                                                 |\n"
						+ "+---------------------------------------------------------------------------------------+\n"
						+ "ORIGINAL ID:..........%s \n"
						+ "TITILE:...............%s \n"
						+ "PROJECT:..............%s \n"
						+ "ORIGIN:...............%s \n"
						+ "STATUS:...............%s \n"
						+ "SEVERITY:.............%s \n"
						+ "PRIORITY:.............%s \n"
						+ "RESOLUTION:...........%s \n"
						+ "VERSION:..............%s \n"
						+ "PLATFORM:.............%s \n"
						+ "OPERATING SYSTEM:.....%s \n"
						+ "CREATED BY:...........%s \n"
						+ "FIXED BY:.............%s \n"
						+ "CLOSED BY:............%s \n"
						+ "REPORTED ON:..........%s \n"
						+ "DESCRIPTION:          \n%s \n\n"
						+ "STEPS TO REPRODUCE:   \n%s \n" + "%s",
						getId(),
						getOriginalId(),
						getTitle(),
						getProject(),
						getOrigin(),
						getStatus(),
						getSeverity(),
						getPriority(),
						((getResolution() == null) ? "" : getResolution()),
						getVersion(),
						getPlatform(),
						getOperatingSystem(),
						((getCreatedBy() == null) ? "" : getCreatedBy()),
						((getFixedBy() == null) ? "" : getFixedBy()),
						((getClosedBy() == null) ? "" : getClosedBy()),
						getDateCreated(),
						(getDescription() == null) ? "" : ConsolePrint
								.addLinebreaks(getDescription(), 80),
						(getStepsToReproduce() == null) ? "" : ConsolePrint
								.addLinebreaks(getStepsToReproduce(), 80),
						comments);

		return fmt;
	}

}
