package com.smartcr.core.model.stateMachine;

import java.util.List;

public interface IStateMachine {

	/**
	 * Must return a list of String with the available states for the new state
	 * machine.
	 * 
	 * @return
	 */
	public List<String> getAvailableStatus();

	/**
	 * Must return a list of String which is the possible resolutions for a
	 * given state.
	 * 
	 * @param state
	 * @return
	 */
	public List<String> getAvailableResolutionsForStatus(String state);

	/**
	 * Must return a list of String which is the possible states that can be
	 * targeted from a given current state.
	 * 
	 * @param currentStatus
	 * @return
	 */
	public List<String> getPossibleStatusTargets(String currentStatus);
}
