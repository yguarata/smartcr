package com.smartcr.core.index;

public class FetcherImplNotFound extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7287441571500495590L;

	public FetcherImplNotFound(String name) {
		super(String.format("Fetcher not found for the repository %s", name));
	}
	
}
