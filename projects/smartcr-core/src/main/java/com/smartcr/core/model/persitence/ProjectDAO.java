package com.smartcr.core.model.persitence;

import com.smartcr.core.model.Project;

public class ProjectDAO extends BaseDAO<Project> {

	private static ProjectDAO instance = null;

	private ProjectDAO() {
	}

	public static ProjectDAO getInstance() {
		if (instance == null) {
			instance = new ProjectDAO();
			DAORegister.getInstance().register(instance);
		}
		return instance;
	}

	@Override
	public Class<Project> getClazz() {
		return Project.class;
	}
	
}
