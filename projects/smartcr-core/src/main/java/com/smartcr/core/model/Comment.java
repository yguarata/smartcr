package com.smartcr.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents a comment for a CR.
 * 
 * @author 04966162424
 * 
 */
@Entity
@Table(name = "COMMENT")
public class Comment extends SmartEntity {
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANGE_REQUEST_ID", nullable = false)
	private ChangeRequest changeRequest;

	/**
	 * The author of the comment.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AUTHOR_ID", nullable = false)
	private Author author;

	/**
	 * The comment.
	 */
	@Lob
	@Column(name = "COMMENT")
	private String comment;

	/**
	 * Date of creation.
	 */
	@Column(name = "DATE_CREATED")
	private Date dateCreated;
	
	public Comment() {
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "    " + getAuthor() + "\n    " + getComment();
	}

	public ChangeRequest getChangeRequest() {
		return changeRequest;
	}

	public void setChangeRequest(ChangeRequest changeRequest) {
		this.changeRequest = changeRequest;
	}

	public Date getDateCreate() {
		return dateCreated;
	}
	
	public Date setDateCreate(Date dateCreated) {
		return this.dateCreated = dateCreated;
	}
}
