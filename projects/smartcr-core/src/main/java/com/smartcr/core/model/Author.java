package com.smartcr.core.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;

/**
 * Represents a person who have created, owned, fixed, or commented a CR.
 * 
 * @author 04966162424
 * 
 */
@Entity
@Table(name = "AUTHOR")
public class Author extends SmartEntity {

	@Column(name = "NAME", length = 1000)
	private String name;

	@Column(name = "EMAIL", length = 150)
	private String email;

	public Author() {
	}

	public Author(String name, String email) {
		super();
		this.name = name;
		this.email = email;
		if (StringUtils.isEmpty(this.email)
				&& StringUtils.isNotEmpty(this.name)) {
			this.email = this.name;
		} else if (StringUtils.isEmpty(this.name)
				&& StringUtils.isNotEmpty(this.email)) {
			this.name = this.email;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return getId() + " - " + getName() + " (" + getEmail() + ")";
	}

	@Override
	public boolean equals(Object obj) {
		Author other = (Author) obj;
		if (other == null) {
			return false;
		}
		if (getId() != null) {
			return getId().equals(other.getId());
		} else if (getEmail() != null) {
			return getEmail().equals(other.getEmail());
		} else {
			return false;
		}
	}

}
