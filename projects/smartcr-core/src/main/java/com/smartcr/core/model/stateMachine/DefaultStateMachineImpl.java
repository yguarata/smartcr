package com.smartcr.core.model.stateMachine;

import java.util.ArrayList;
import java.util.List;

/**
 * This default state machine is similar to the Bugzilla state machine.
 * @author yguarata
 *
 */
public class DefaultStateMachineImpl implements IStateMachine {

	@Override
	public List<String> getAvailableStatus() {
		List<String> states = new ArrayList<String>();
		states.add("unconfirmed");
		states.add("new");
		states.add("assigned");
		states.add("resolved");
		states.add("verified");
		states.add("reopen");
		states.add("close");
		return states;
	}

	@Override
	public List<String> getAvailableResolutionsForStatus(String status) {
		if (status == null) {
			return null;
		}
		
		List<String> resolutions = new ArrayList<String>();
		if (status.toLowerCase().equals("resolved")) {
			resolutions.add("fixed");
			resolutions.add("duplicate");
			resolutions.add("wontfix");
			resolutions.add("worksforme");
			resolutions.add("invalid");
			resolutions.add("remind");
			resolutions.add("later");
		}
		return resolutions;
	}

	@Override
	public List<String> getPossibleStatusTargets(String currentStatus) {
		return getAvailableStatus();
	}

}
