package com.smartcr.core.util;

import java.text.Normalizer;
import java.text.Normalizer.Form;

public class StringUtil {

	public static String removeAccents(String s) {
		return Normalizer.normalize(s, Form.NFD).replaceAll(
				"\\p{InCombiningDiacriticalMarks}+", "");
	}

	/**
	 * Remove accents, spaces and puts on lower case.
	 */
	public static String unify(String s) {
		return removeAccents(s.replace(" ", "").toLowerCase());
	}
	
}
