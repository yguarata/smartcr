package com.smartcr.core.model.persitence;

import java.util.logging.Logger;

import com.smartcr.core.model.Comment;

public class CommentDAO extends BaseDAO<Comment> {

	Logger logger = Logger.getLogger(getClass().getName());
	private static CommentDAO instance = null;

	private CommentDAO() {
	}

	public static CommentDAO getInstance() {
		if (instance == null) {
			instance = new CommentDAO();
			DAORegister.getInstance().register(instance);
		}
		return instance;
	}

	@Override
	public Class<Comment> getClazz() {
		return Comment.class;
	}

}
