package com.smartcr.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents the history of changes in a change request.
 * 
 * @author 04966162424
 * 
 */
@Entity
@Table(name = "HISTORY")
public class History extends SmartEntity implements Comparable<History> {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHANGE_REQUEST_ID", nullable = false)
	private ChangeRequest changeRequest;

	/**
	 * The author of the change.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AUTHOR_ID", nullable = false)
	private Author author;

	/**
	 * The change description.
	 */
	@Lob
	@Column(name = "CHANGE")
	private String change;

	@Column(name = "FIELD")
	private String field;
	
	/**
	 * Date of creation.
	 */
	@Column(name = "DATE_CREATED")
	private Date date;

	public ChangeRequest getChangeRequest() {
		return changeRequest;
	}

	public void setChangeRequest(ChangeRequest changeRequest) {
		this.changeRequest = changeRequest;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author user) {
		this.author = user;
	}

	public String getChange() {
		return change;
	}

	public void setChange(String change) {
		this.change = change;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		return String.format("%s | %s | %s | %s", date, author, field, change);
	}

	@Override
	public int compareTo(History history) {
		int i = date.compareTo(history.getDate());
		if ( i == 0) {
			i = getId().compareTo(history.getId());
		}
		return i;
	}
}
