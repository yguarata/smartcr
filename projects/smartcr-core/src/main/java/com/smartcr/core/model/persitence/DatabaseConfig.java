package com.smartcr.core.model.persitence;

public class DatabaseConfig {

	private String databaseDialect;
	
	private String databaseDriver;
	
	private String databaseUser;
	
	private String databasePassword;
	
	private String databaseUrl;
	
	private String databaseAuto;
	
	private String databaseShowSQL;
	
	public void setDatabaseDialect(String databaseDialect) {
		this.databaseDialect = databaseDialect;
	}

	public void setDatabaseDriver(String databaseDriver) {
		this.databaseDriver = databaseDriver;
	}

	public void setDatabaseUser(String databaseUser) {
		this.databaseUser = databaseUser;
	}

	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}

	public void setDatabaseUrl(String databaseUrl) {
		this.databaseUrl = databaseUrl;
	}

	public void setDatabaseAuto(String databaseAuto) {
		this.databaseAuto = databaseAuto;
	}

	public void setDatabaseShowSQL(String databaseShowSQL) {
		this.databaseShowSQL = databaseShowSQL;
	}

	public String getDatabaseDialect() {
		return databaseDialect;
	}

	public String getDatabaseDriver() {
		return databaseDriver;
	}

	public String getDatabaseUser() {
		return databaseUser;
	}

	public String getDatabasePassword() {
		return databasePassword;
	}

	public String getDatabaseUrl() {
		return databaseUrl;
	}

	public String getDatabaseAuto() {
		return databaseAuto;
	}

	public String getDatabaseShowSQL() {
		return databaseShowSQL;
	}

}
