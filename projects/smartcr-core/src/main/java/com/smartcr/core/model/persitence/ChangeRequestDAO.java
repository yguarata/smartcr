package com.smartcr.core.model.persitence;

import java.io.IOException;
import java.util.List;

import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.smartcr.core.index.IndexException;
import com.smartcr.core.index.LuceneIndexer;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.model.History;

public class ChangeRequestDAO extends BaseDAO<ChangeRequest> {

	private AuthorDAO authorDAO = AuthorDAO.getInstance();

	private static ChangeRequestDAO instance = null;

	private ChangeRequestDAO() {
	}

	public static ChangeRequestDAO getInstance() {
		if (instance == null) {
			instance = new ChangeRequestDAO();
			DAORegister.getInstance().register(instance);
		}
		return instance;
	}

	@Override
	public Class<ChangeRequest> getClazz() {
		return ChangeRequest.class;
	}

	@SuppressWarnings("unchecked")
	public List<ChangeRequest> findByExample(ChangeRequest changeRequest) {
		Example crExample = Example.create(changeRequest).ignoreCase()
				.enableLike(MatchMode.ANYWHERE).excludeZeroes()
				.excludeProperty("comments");
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(ChangeRequest.class).add(
				crExample);
		if (changeRequest.getFixedBy() != null
				&& StringUtils
						.isNotEmpty(changeRequest.getFixedBy().getEmail())) {
			Criteria newCriteria = criteria.createCriteria("fixedBy");
			newCriteria.add(Restrictions.like("email", changeRequest
					.getFixedBy().getEmail(), MatchMode.ANYWHERE));
		}

		if (changeRequest.getClosedBy() != null
				&& StringUtils.isNotEmpty(changeRequest.getClosedBy()
						.getEmail())) {
			Criteria newCriteria = criteria.createCriteria("closedBy");
			newCriteria.add(Restrictions.like("email", changeRequest
					.getClosedBy().getEmail(), MatchMode.ANYWHERE));
		}

		if (changeRequest.getCreatedBy() != null
				&& StringUtils.isNotEmpty(changeRequest.getCreatedBy()
						.getEmail())) {
			Criteria newCriteria = criteria.createCriteria("createdBy");
			newCriteria.add(Restrictions.like("email", changeRequest
					.getCreatedBy().getEmail(), MatchMode.ANYWHERE));
		}

		return criteria.list();
	}

	public List<ChangeRequest> findUnassigned() {
		return findByPropertyNull("fixedBy").getResultList();
	}

	public List<ChangeRequest> findAssigned() {
		logger.info("Finding assigned CRs for using in the classifier...");
		return findByPropertyNotNull("fixedBy").getResultList();
	}

	public List<ChangeRequest> findFixedBy(Author author) {
		return findByProperty("fixedBy", author).getResultList();
	}

	public List<ChangeRequest> findCreatedBy(Author author) {
		return findByProperty("createdBy", author).getResultList();
	}

	public List<ChangeRequest> findClosedBy(Author author) {
		return findByProperty("closedBy", author).getResultList();
	}

	public List<ChangeRequest> findClosed() {
		return findByPropertyNotNull("closedBy").getResultList();
	}

	public List<String> findResolutions(String repositoryName) {
		TypedQuery<String> query = getEntityManager().createQuery(
				"SELECT distinct cr.resolution FROM ChangeRequest cr "
						+ "WHERE repositoryName = '" + repositoryName + "'",
				String.class);
		return query.getResultList();
	}

	@Override
	public void insert(ChangeRequest obj) {
		if (obj.getCreatedBy() != null) {
			Author author = authorDAO.find(obj.getCreatedBy());
			if (author != null) {
				obj.setCreatedBy(author);
			} else {
				authorDAO.insert(obj.getCreatedBy());
				logger.info("Added author " + obj.getCreatedBy());
			}
		}

		if (obj.getFixedBy() != null) {
			Author author = authorDAO.find(obj.getFixedBy());
			if (author != null) {
				obj.setFixedBy(author);
			} else {
				authorDAO.insert(obj.getFixedBy());
				logger.info("Added author " + obj.getFixedBy());
			}
		}

		if (obj.getClosedBy() != null) {
			Author author = authorDAO.find(obj.getClosedBy());
			if (author != null) {
				obj.setClosedBy(author);
			} else {
				authorDAO.insert(obj.getClosedBy());
				logger.info("Added author " + obj.getClosedBy());
			}
		}

		for (Comment comment : obj.getComments()) {
			if (comment.getAuthor() != null) {
				Author author = authorDAO.find(comment.getAuthor());
				if (author != null) {
					comment.setAuthor(author);
				} else {
					authorDAO.insert(comment.getAuthor());
					logger.info("Added author " + comment.getAuthor());
				}
			}
		}

		for (History history : obj.getHistory()) {
			if (history.getAuthor() != null) {
				Author author = authorDAO.find(history.getAuthor());
				if (author != null) {
					history.setAuthor(author);
				} else {
					authorDAO.insert(history.getAuthor());
					logger.info("Added author " + history.getAuthor());
				}
			}
		}

		super.insert(obj);

		try {
			LuceneIndexer.getInstance().indexChangeRequest(obj);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IndexException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(ChangeRequest obj) {
		super.update(obj);
		try {
			LuceneIndexer.getInstance().indexChangeRequest(obj);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IndexException e) {
			e.printStackTrace();
		}
	}

	public List<String> findProjects() {
		TypedQuery<String> query = getEntityManager().createQuery(
				"SELECT distinct cr.project FROM ChangeRequest cr",
				String.class);
		return query.getResultList();
	}

	public List<ChangeRequest> findAssignedByProject(String project) {
		ChangeRequest changeRequest = new ChangeRequest();
		changeRequest.setProject(project);
		Example crExample = Example.create(changeRequest).ignoreCase()
				.enableLike(MatchMode.ANYWHERE).excludeZeroes()
				.excludeProperty("comments");
		Session session = (Session) getEntityManager().getDelegate();
		Criteria criteria = session.createCriteria(ChangeRequest.class).add(crExample);
		criteria.add(Restrictions.isNotNull("fixedBy"));
		return criteria.list();
	}

}
