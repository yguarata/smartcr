package com.smartcr.core;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;

import com.smartcr.core.index.Fetcher;
import com.smartcr.core.index.FetcherImplNotFound;
import com.smartcr.core.index.FetcherImplRequiredException;
import com.smartcr.core.index.IIndex;
import com.smartcr.core.index.IndexException;
import com.smartcr.core.index.LuceneIndexer;
import com.smartcr.core.util.IAskPassword;

public class IndexFacade {

	static Logger logger = Logger.getLogger(IndexFacade.class.getName());

	private static IndexFacade coreFacadeInstance = null;

	private IIndex index = LuceneIndexer.getInstance();

	private IndexFacade() {

	}

	public static IndexFacade getInstance() {
		if (coreFacadeInstance == null) {
			coreFacadeInstance = new IndexFacade();
		}

		return coreFacadeInstance;
	}

	public void insertCRsFromAllFetchers(Long limit, boolean continueFromLast,
			IAskPassword askPassword) {
		for (Fetcher fetcher : index.getFetchers()) {
			askPassword.verifyPassword(fetcher);
			if (limit != null) {
				fetcher.getRepositoryConfig().setLimit(limit);
			} else {
				fetcher.getRepositoryConfig().setLimit(null);
			}
			if (continueFromLast) {
				fetcher.setFetchFromLastID(true);
			} else {
				fetcher.setFetchFromLastID(false);
			}
			index.storeAllChangeRequests(fetcher.getName());
		}
	}

	public void insertCRsByRepositoryName(String repositoryName, Long limit,
			boolean continueFromLast, IAskPassword askPassword)
			throws FetcherImplNotFound {
		Fetcher fetcher = index.getFetcher(repositoryName);
		askPassword.verifyPassword(fetcher);
		fetcher.getRepositoryConfig().setLimit(limit);
		fetcher.setFetchFromLastID(continueFromLast);
		index.storeAllChangeRequests(repositoryName);
	}

	public List<String> getProjectsName() throws IOException {
		return index.getProjectsName();
	}

	public void createIndex() {
		try {
			index.createIndex();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FetcherImplRequiredException e) {
			e.printStackTrace();
		} catch (IndexException e) {
			e.printStackTrace();
		}
	}
	
	public void updateIndex() {
		try {
			index.updateIndex();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (FetcherImplRequiredException e) {
			e.printStackTrace();
		} catch (IndexException e) {
			e.printStackTrace();
		}
	}
	
	public List<Fetcher> getCRFetchers() {
		return index.getFetchers();
	}

	public List<Document> searchCRs(String query) throws IOException,
			ParseException {
		return index.search(query);
	}

	public void setIndexPath(String path) {
		LuceneIndexer.getInstance().setIndexPath(path);
	}

	public void addFetcher(Fetcher fetcher) {
		if (LuceneIndexer.getInstance().getFetcher(fetcher.getName()) != null) {
			return;
		} else {
			logger.info("Adding fetcher " + fetcher.getName() + "...");
			LuceneIndexer.getInstance().addFetcher(fetcher);
		}
	}

}
