package com.smartcr.core.util;

import com.smartcr.core.index.Fetcher;

public interface IAskPassword {
	public void verifyPassword(Fetcher fetcher);
}
