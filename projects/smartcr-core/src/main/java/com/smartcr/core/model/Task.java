package com.smartcr.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Task extends SmartEntity {

	/**
	 * The title of the CR.
	 */
	@Column(name = "TITLE", length = 10000)
	private String title;

	/**
	 * The description of the CR.
	 */
	@Lob
	@Column(name = "DESCRIPTION")
	private String description;
	
	/**
	 * Who created the CR.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CREATOR_ID")
	private Author createdBy;
	
	/**
	 * Who fixed the issue reported in the CR.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FIXER_ID")
	private Author fixedBy;
	
	/**
	 * Who closed the CR.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CLOSER_ID")
	private Author closedBy;
	
	/**
	 * Date of creation.
	 */
	@Column(name = "DATE_CREATED")
	private Date dateCreated;
	
	/**
	 * Date of the closing.
	 */
	@Column(name = "DATE_CLOSED")
	private Date dateClosed;
	
	/**
	 * Estimated date to the task.
	 */
	@Column(name = "DATE_DUE")
	private Date dateDue;
	
	/**
	 * Date of the update in the origin repository.
	 */
	@Column(name = "DATE_UPDATED")
	private Date dateUpdated;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Author getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Author creator) {
		this.createdBy = creator;
	}

	public Author getFixedBy() {
		return fixedBy;
	}

	public void setFixedBy(Author owner) {
		this.fixedBy = owner;
	}
	
	public Author getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(Author resolver) {
		this.closedBy = resolver;
	}
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public Date getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(Date dateClosed) {
		this.dateClosed = dateClosed;
	}
	
	public Date getDateDue() {
		return dateDue;
	}

	public void setDateDue(Date dateDue) {
		this.dateDue = dateDue;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
}
