package com.smartcr.core.util;

import java.util.StringTokenizer;

import com.smartcr.core.model.ChangeRequest;

public class ConsolePrint {

	public static void getCR(long id, String repository) {
		System.out.println(String.format("Retrieving CR %d from %s", id,
				repository));
	}

	public static void crDontExist(long id, String repository) {
		System.out.println(String.format("CR %d from %s do not exist!", id,
				repository));
	}

	public static void totalCRsIndexed(long total, String repository) {
		System.out.println(String.format("Total of CRs indexed for %s: %d",
				repository, total));
	}

	public static void crShort(ChangeRequest cr) {
		String title = cr.getTitle();
		if (title != null && title.length() > 50) {
			title = title.subSequence(0, 47) + "...";
		}
		System.out.println(String.format("ID: %5s | ORIID: %5s | %-50s | %-18s | %-20s | %-10s", cr.getId(), cr.getOriginalId(),
				title, cr.getStatus(), cr.getResolution(), cr.getSeverity()));
	}

	public static void crComplete(ChangeRequest cr) {
		System.out.println(cr);
	}
	
	public static String addLinebreaks(String input, int maxLineLength) {
	    StringTokenizer tok = new StringTokenizer(input, " ");
	    StringBuilder output = new StringBuilder(input.length());
	    int lineLen = 0;
	    while (tok.hasMoreTokens()) {
	        String word = tok.nextToken();

	        if (lineLen + word.length() > maxLineLength) {
	            output.append("\n");
	            lineLen = 0;
	        }
	        output.append(word + " ");
	        lineLen += word.length();
	    }
	    return output.toString();
	}
}
