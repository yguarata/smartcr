package com.smartcr.core.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.junit.Test;

import com.smartcr.core.index.IIndex;
import com.smartcr.core.index.IndexException;
import com.smartcr.core.index.LuceneIndexer;
import com.smartcr.core.model.ChangeRequest;

public class LuceneIndexerTest extends TestBase {

	@Test
	public void testIndexCR() throws IOException, ParseException, IndexException {
		IIndex idx = LuceneIndexer.getInstance();
		idx.resetIndex();
		ChangeRequest cr = TestUtil.createCR(1l);
		idx.indexChangeRequest(cr);
		List<Document> result = idx.search(cr.getTitle());
		Assert.assertEquals(1, result.size());
		Assert.assertEquals(cr.getId(), Long.valueOf(result.get(0).get("id")));
	}

	@Test
	public void testIndexCRs() throws IOException, ParseException, IndexException {
		IIndex idx = LuceneIndexer.getInstance();
		idx.resetIndex();
		List<ChangeRequest> crs = new ArrayList<ChangeRequest>();
		crs.add(TestUtil.createCR(2l));
		crs.add(TestUtil.createCR(3l));
		crs.add(TestUtil.createCR(4l));
		crs.add(TestUtil.createCR(5l));
		crs.add(TestUtil.createCR(6l));
		idx.indexChangeRequests(crs);
		List<Document> result = idx.search(crs.get(0).getTitle());
		Assert.assertEquals(result.size(), crs.size());
		Assert.assertEquals(result.size(), idx.count());
	}
}
