package com.smartcr.core.test.dao;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.smartcr.core.model.Author;
import com.smartcr.core.model.persitence.AuthorDAO;
import com.smartcr.core.test.TestBase;

public class AuthorDAOTest extends TestBase {

	AuthorDAO dao = AuthorDAO.getInstance();

	@Before
	@After
	public void clean() {
		// for (Author author : dao.findAll()) {
		// dao.delete(author);
		// }
	}

	@Test
	public void testCRUD() {
		Author author = new Author("Yguarata", "yguarata@gmail.com");
		dao.insert(author);
		Assert.assertNotNull(author.getId());
		Assert.assertEquals(author, dao.findByEmail(author.getEmail()));
		Assert.assertEquals(author, dao.findByEmail(author.getEmail()));

		author.setEmail("yguarata@gmail.com");
		Assert.assertEquals(author, dao.find(author));

		Assert.assertEquals(null,
				dao.find(new Author(null, "yguarata@gmail.com")));
		Assert.assertEquals(null, dao.find(new Author("Yguarata", null)));
	}

}
