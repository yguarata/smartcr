package com.smartcr.core.test;

import org.junit.BeforeClass;

import com.smartcr.core.index.LuceneIndexer;
import com.smartcr.core.model.persitence.DAORegister;
import com.smartcr.core.model.persitence.DatabaseConfig;

public class TestBase {

	protected static DatabaseConfig databaseConfig = new DatabaseConfig();

	@BeforeClass
	public static void setUp() throws Exception {
		databaseConfig.setDatabaseAuto("update");
		databaseConfig.setDatabaseDialect("org.hibernate.dialect.HSQLDialect");
		databaseConfig.setDatabaseDriver("org.hsqldb.jdbcDriver");
		databaseConfig.setDatabasePassword("");
		databaseConfig.setDatabaseShowSQL("true");
		databaseConfig.setDatabaseUrl("jdbc:hsqldb:mem:jamsession");
		databaseConfig.setDatabaseUser("sa");
		DAORegister.getInstance().configureDatabase(databaseConfig);
		LuceneIndexer.getInstance().resetIndex();
	}

}
