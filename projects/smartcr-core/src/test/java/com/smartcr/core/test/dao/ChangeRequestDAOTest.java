package com.smartcr.core.test.dao;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.persitence.AuthorDAO;
import com.smartcr.core.model.persitence.ChangeRequestDAO;
import com.smartcr.core.test.TestBase;
import com.smartcr.core.test.TestUtil;

public class ChangeRequestDAOTest extends TestBase {

	ChangeRequestDAO dao = ChangeRequestDAO.getInstance();
	AuthorDAO authorDAO = AuthorDAO.getInstance();

	@Before
	@After
	public void clean() {
		for (ChangeRequest cr : dao.findAll()) {
			dao.delete(cr);
		}
		for (Author author : authorDAO.findAll()) {
			authorDAO.delete(author);
		}
	}

	@Test
	public void testCRUD() {
		ChangeRequest cr = TestUtil.createCR();
		cr.setOriginalId("5000");

		dao.insert(cr);
		Assert.assertNotNull(cr.getId());
		Assert.assertEquals(dao.get(cr.getId()).getId(), cr.getId());
		Assert.assertEquals(dao
				.findByProperty("originalId", cr.getOriginalId())
				.getSingleResult().getOriginalId(), cr.getOriginalId());
		Assert.assertEquals(dao.get(cr.getId()).getComments().size(), 1);
		Assert.assertEquals(dao.get(cr.getId()).getHistory().size(), 1);
		
		cr.setTitle("New title");
		dao.update(cr);
		Assert.assertEquals(dao.get(cr.getId()).getTitle(), "New title");

		Assert.assertEquals(1, dao.findAll().size());

		dao.delete(cr);
		Assert.assertFalse(dao.contains(cr.getId()));
	}

	@Test
	public void testFindAll() {
		dao.insert(TestUtil.createCR());
		dao.insert(TestUtil.createCR());
		dao.insert(TestUtil.createCR());
		dao.insert(TestUtil.createCR());
		dao.insert(TestUtil.createCR());
		// List<ChangeRequest> closed = dao.findClosed();
		// Assert.assertTrue(closed != null && closed.size() != 0);
		Assert.assertEquals(5, dao.findAll().size());
	}

	@Test
	public void testFindByExample() {
		dao.insert(TestUtil.createCR());
		ChangeRequest crExample = TestUtil.createCR();
		Assert.assertEquals(1, dao.findByExample(crExample).size());

		ChangeRequest crExample2 = new ChangeRequest();
		Author author = TestUtil.createCR(30l, "teste", "teste@gmail.com")
				.getFixedBy();
		crExample2.setFixedBy(author);
		Assert.assertEquals(0, dao.findByExample(crExample2).size());

		crExample2.getFixedBy().setEmail(crExample.getFixedBy().getEmail());
		Assert.assertEquals(1, dao.findByExample(crExample2).size());

		crExample2.setTitle("something differente");
		Assert.assertEquals(0, dao.findByExample(crExample2).size());
	}

}
