package com.smartcr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.NoResultException;
import javax.xml.ws.Endpoint;

import com.smartcr.classification.ClassificationFacade;
import com.smartcr.classification.assignment.impl.LearningException;
import com.smartcr.core.ModelFacade;
import com.smartcr.core.model.Author;
import com.smartcr.core.model.ChangeRequest;
import com.smartcr.core.model.Comment;
import com.smartcr.core.model.persitence.DAORegister;
import com.smartcr.core.model.persitence.DatabaseConfig;
import com.smartcr.core.util.DefaultProgressMonitor;

@WebService
public class WebServices {

	Logger logger = Logger.getLogger(getClass().getName());
	private static final ModelFacade MODEL_FACADE = ModelFacade.getInstance();
	private static final ClassificationFacade ASSIGN_FACADE = ClassificationFacade
			.getInstance();
	private TrainingThread trainingThread;

	/**
	 * Gets the author specified byt the String email.
	 * 
	 * @param email
	 * @return
	 */
	@WebMethod
	public Author getAuthor(String email) {
		return MODEL_FACADE.getAuthorByEmail(email);
	}

	/**
	 * Returns all authors stored in the database.
	 * 
	 * @return
	 */
	@WebMethod
	public List<Author> findAllAuthors() {
		return MODEL_FACADE.findAllAuthors();
	}

	/**
	 * Returns a list of author (developers) that are suitable for handling the
	 * specified change request.
	 * 
	 * @param changeRequest
	 * @return
	 * @throws LearningException
	 */
	@WebMethod
	public Set<Author> suggestDevelopers(ChangeRequest changeRequest)
			throws LearningException {
		return ASSIGN_FACADE.recommendAuthorsForCR(changeRequest).keySet();
	}

	/**
	 * Returns a list with all IDs of change requests stored in the database.
	 * 
	 * @return
	 */
	@WebMethod
	public List<String> findAllChangeRequestIDs() {
		List<String> ids = new ArrayList<String>();
		for (ChangeRequest cr : MODEL_FACADE.findAllCRs()) {
			ids.add(cr.getOriginalId());
		}
		return ids;
	}

	/**
	 * Get the amount of change requests stored.
	 * 
	 * @return
	 */
	@WebMethod
	public Long countChangeRequests() {
		Long count = MODEL_FACADE.countCRs();
		logger.info("Counting change request total=" + count);
		return count;
	}

	/**
	 * Get the amount of authors stored.
	 * 
	 * @return
	 */
	@WebMethod
	public Long countAuthors() {
		return MODEL_FACADE.countAuthors();
	}

	/**
	 * Gets the change request specified by the changeRequestId. This id is the
	 * identifier of the change request in the repository which is accessing
	 * this web service.
	 * 
	 * @param id
	 * @return
	 */
	@WebMethod
	public ChangeRequest getChangeRequest(@WebParam(name = "id") Long id) {
		return MODEL_FACADE.getCRByOriginalId(id);
	}

	/**
	 * Creates a change request in the database of the web service. In other
	 * words, this web service is used to export a change request from the
	 * repository which is accessing this service, to the database of the web
	 * service.
	 * 
	 * @param changeRequest
	 */
	@WebMethod
	public void createChangeRequest(@WebParam(name = "id") Long id,
			@WebParam(name = "title") String title,
			@WebParam(name = "description") String description,
			@WebParam(name = "steps") String steps,
			@WebParam(name = "project") String project,
			@WebParam(name = "origin") String origin,
			@WebParam(name = "severity") String severity,
			@WebParam(name = "status") String status,
			@WebParam(name = "resolution") String resolution,
			@WebParam(name = "target") String target,
			@WebParam(name = "priority") String priority,
			@WebParam(name = "version") String version,
			@WebParam(name = "platform") String platform,
			@WebParam(name = "os") String os,
			@WebParam(name = "dtFixed") Date dtFixed,
			@WebParam(name = "dtCreated") Date dtCreated,
			@WebParam(name = "dtDue") Date dtDue,
			@WebParam(name = "dtClosed") Date dtClosed,
			@WebParam(name = "dtUpdated") Date dtUpdated,
			@WebParam(name = "creatorEmail") String creatorEmail,
			@WebParam(name = "fixerEmail") String fixerEmail,
			@WebParam(name = "closerEmail") String closerEmail) {
		logger.info("Creating change request originalId=" + id);
		ChangeRequest cr = new ChangeRequest();
		cr.setOriginalId(String.valueOf(id));
		cr.setId(null);
		cr.setTitle(title);
		cr.setDescription(description);
		cr.setStepsToReproduce(steps);
		cr.setProject(project);
		cr.setOrigin(origin);
		cr.setSeverity(severity);
		cr.setStatus(status);
		cr.setResolution(resolution);
		cr.setTarget(target);
		cr.setPriority(priority);
		cr.setVersion(version);
		cr.setPlatform(platform);
		cr.setOperatingSystem(os);
		Author fixer = new Author(fixerEmail, fixerEmail);
		Author creator = new Author(creatorEmail, creatorEmail);
		cr.setFixedBy(fixer);
		cr.setCreatedBy(creator);
		MODEL_FACADE.insertCR(cr);
	}

	@WebMethod
	public void updateChangeRequest(@WebParam(name = "id") Long id,
			@WebParam(name = "title") String title,
			@WebParam(name = "description") String description,
			@WebParam(name = "steps") String steps,
			@WebParam(name = "project") String project,
			@WebParam(name = "origin") String origin,
			@WebParam(name = "severity") String severity,
			@WebParam(name = "status") String status,
			@WebParam(name = "resolution") String resolution,
			@WebParam(name = "target") String target,
			@WebParam(name = "priority") String priority,
			@WebParam(name = "version") String version,
			@WebParam(name = "platform") String platform,
			@WebParam(name = "os") String os,
			@WebParam(name = "dtFixed") Date dtFixed,
			@WebParam(name = "dtCreated") Date dtCreated,
			@WebParam(name = "dtDue") Date dtDue,
			@WebParam(name = "dtClosed") Date dtClosed,
			@WebParam(name = "dtUpdated") Date dtUpdated,
			@WebParam(name = "creatorEmail") String creatorEmail,
			@WebParam(name = "fixerEmail") String fixerEmail,
			@WebParam(name = "closerEmail") String closerEmail) {
		logger.info("Updating change request originalId=" + id);
		ChangeRequest cr = MODEL_FACADE.getCRByOriginalId(id);
		cr.setTitle(title);
		cr.setDescription(description);
		cr.setStepsToReproduce(steps);
		cr.setProject(project);
		cr.setOrigin(origin);
		cr.setSeverity(severity);
		cr.setStatus(status);
		cr.setResolution(resolution);
		cr.setTarget(target);
		cr.setPriority(priority);
		cr.setVersion(version);
		cr.setPlatform(platform);
		cr.setOperatingSystem(os);
		if (cr.getFixedBy() != null
				&& !cr.getFixedBy().getEmail().equals(fixerEmail)) {
			Author fixer = new Author(fixerEmail, fixerEmail);
			cr.setFixedBy(fixer);
		}
		MODEL_FACADE.updateCR(cr);
	}

	@WebMethod
	public void addChangeRequestComment(@WebParam(name = "crId") Long id,
			@WebParam(name = "commentId") Long commentId,
			@WebParam(name = "comment") String comment,
			@WebParam(name = "authorEmail") String authorEmail) {
		logger.info("Adding comment to change request originalId=" + id);
		Author author = MODEL_FACADE.getAuthorByEmail(authorEmail);
		if (author == null) {
			author = new Author(authorEmail, authorEmail);
			MODEL_FACADE.insertAuthor(author);
		}
		ChangeRequest cr = MODEL_FACADE.getCRByOriginalId(id);
		Comment commentObj = new Comment();
		commentObj.setChangeRequest(cr);
		commentObj.setComment(comment);
		commentObj.setAuthor(author);
		commentObj.setOriginalId(String.valueOf(commentId));
		MODEL_FACADE.insertComment(commentObj);
	}

	@WebMethod
	public void deleteChangeRequestComment(@WebParam(name = "id") Long id) {
		logger.info("Deleting comment originalId=" + id);
		MODEL_FACADE.deleteCommentByOriginalID(id);
	}

	@WebMethod
	public void editChangeRequestComment(@WebParam(name = "id") Long id,
			@WebParam(name = "comment") String comment) {
		logger.info("Updating comment originalId=" + id);
		Comment commentObj = MODEL_FACADE.getCommentByOriginalID(id);
		commentObj.setComment(comment);
		MODEL_FACADE.updateComment(commentObj);
	}

	@WebMethod
	public int changeRequestExists(@WebParam(name = "id") Long id) {
		try {
			getChangeRequest(id);
			return 1;
		} catch (NoResultException exception) {
			return 0;
		}
	}

	/**
	 * This web service assigns a suitable author (developer) for the given
	 * change request.
	 * 
	 * @param changeRequestId
	 * @return The author assigned to the change request, or null if the change
	 *         could not be assigned.
	 * @throws LearningException
	 */
	@WebMethod
	public Author assignChangeRequest(Long changeRequestId)
			throws LearningException {
		ChangeRequest cr = MODEL_FACADE.getCRByOriginalId(changeRequestId);
		if (ASSIGN_FACADE.isClassifierTrained()) {
			if (ASSIGN_FACADE.assignCR_(cr)) {
				MODEL_FACADE.updateCR(cr);
				return cr.getFixedBy();
			} else {
				return null;
			}
		} else {
			trainingThread = new TrainingThread();
			trainingThread.run();
			throw new LearningException(
					"The classifier responsible for assigning change "
							+ "request is being trained. Please, tray again in "
							+ "a few minutes.");
		}
	}

	@WebMethod
	public void trainClassifier() {
		if (ASSIGN_FACADE.isClassifierTrained()
				|| ASSIGN_FACADE.isClassifierTraining()) {
			return;
		}

		if (trainingThread == null) {
			trainingThread = new TrainingThread();
		}

		trainingThread.run();
	}

	@WebMethod
	public void stopTraining() {
		if (ASSIGN_FACADE.isClassifierTraining() && trainingThread != null) {
			// TODO: create mecanism to stop thread.
		}
	}

	class TrainingThread extends Thread {
		@Override
		public void run() {
			try {
				ASSIGN_FACADE.trainCRAssigner(DefaultProgressMonitor
						.getInstance());
			} catch (LearningException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		Properties props = new Properties();
		try {
			props.load(WebServices.class.getClassLoader().getResourceAsStream(
					"config.properties"));
			DatabaseConfig dbConfig = new DatabaseConfig();
			dbConfig.setDatabaseAuto(props.getProperty("dbAuto"));
			dbConfig.setDatabaseDialect(props.getProperty("dbDialect"));
			dbConfig.setDatabaseDriver(props.getProperty("dbDriver"));
			dbConfig.setDatabasePassword(props.getProperty("dbPassword"));
			dbConfig.setDatabaseShowSQL(props.getProperty("dbShowSql"));
			dbConfig.setDatabaseUrl(props.getProperty("dbUrl"));
			dbConfig.setDatabaseUser(props.getProperty("dbUser"));
			DAORegister.getInstance().configureDatabase(dbConfig);
			Endpoint.publish("http://localhost:8081/smartcr/service",
					new WebServices());
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
